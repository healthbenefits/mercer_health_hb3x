package pages.HB43x;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class CongratsPage {

	ExtentTest test =null;

	@FindBy(id = "ctl00_MainBody_Confirm1_ConfirmCtrl__pageTitle")

	public WebElement congrats_Msg;//Congratulations, Your Elections Have Been Saved!

	@FindBy(xpath = "//a[@id='ctl00_MainBody_Confirm1_ConfirmCtrl_NavLink']")
	WebElement congrats_Continue; 

	public CongratsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	public void congratulationsMessage() throws InterruptedException {
		clickElement(congrats_Continue);
	}
}
