package pages.GMD;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class ProxyGroupsPage {
	
	ExtentTest test =null;
	

	@FindBy(xpath = "//div[@class='mul-box-bdy']//h2")
	public WebElement pageTitle;
	

	
	public ProxyGroupsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}

}