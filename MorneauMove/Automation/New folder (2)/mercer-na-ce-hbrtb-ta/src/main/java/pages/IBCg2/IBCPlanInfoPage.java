package pages.IBCg2;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBCPlanInfoPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h1[text()=' Plan Information ']")
	 public WebElement pageH1;
	
	
	public IBCPlanInfoPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
		
}
	
	
}
