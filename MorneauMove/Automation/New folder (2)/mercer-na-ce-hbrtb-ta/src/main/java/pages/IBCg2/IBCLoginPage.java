package pages.IBCg2;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBCLoginPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h2[text()='Returning Users']")
	 public WebElement ret_user;
	
	@FindBy(xpath = "//p[text()='Log in to your existing account.']")
	public WebElement login_Txt;
	
	@FindBy(xpath = "//label[text()='Username']")
	 public WebElement un_Txt;
	
	@FindBy(xpath = "//label[text()='Password']")
	 public WebElement pw_Txt;
	
	@FindBy(xpath = "//input[@id='username']")
	 public WebElement un_Txtbx;
	
	@FindBy(xpath = "//input[@id='password']")
	 public WebElement pw_Txtbx;
	
	@FindBy(xpath = "//button[@class='mas-btn mas-btn-primary']")
	 public WebElement Login_button;
	
	@FindBy(xpath = "//a[text()='Privacy Policy']")
	 public WebElement footlinkpp;
	
	@FindBy(xpath = "//a[text()='Terms of Use']")
	 public WebElement footlinktou;
		
	
	public IBCLoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void login(String USERNAME,String PASSWORD) throws InterruptedException {
		waitForElementToDisplay(ret_user);
		setInput(un_Txtbx, USERNAME);
		setInput(pw_Txtbx, PASSWORD);;
		clickElement(Login_button);
	}
	
	
	
	}
