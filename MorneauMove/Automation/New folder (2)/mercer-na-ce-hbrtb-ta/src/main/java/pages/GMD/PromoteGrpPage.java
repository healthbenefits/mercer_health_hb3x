package pages.GMD;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class PromoteGrpPage {
	
	ExtentTest test =null;
	
	
	@FindBy(xpath = "//div[@class='mul-box-bdy']//h2")
	public WebElement pageTitle;
	
	@FindBy(xpath = "//button[@title='Promote DEV to TEST']")
	WebElement promoteDT;
	
	@FindBy(xpath = "//button[@class='mul-btn mul-btn-medium mul-btn-primary ui-dialog-titlebar-close']")
	WebElement promDTbutton;
	
	@FindBy(xpath = "//button[@title='Promote TEST to CSO']")
	WebElement promoteTC;
	
	@FindBy(xpath = "//button[@title='Promote CSO to PROD']")
	WebElement promoteCP;
	
	@FindBy(xpath = "//input[@class='col-12 isupport_ticket2']")
	WebElement iSuptckt;
	
	@FindBy(xpath = "//input[@class='group_lob_db']")
	WebElement runCB;
	
	@FindBy(xpath = "//button[@title='Run Now']")
	WebElement runNow;
	
	@FindBy(xpath = "//h6[text()='Confirmation']")
	public WebElement prm_Confirmmsg;
	
	@FindBy(xpath = "//span[@title='Close']")
	public WebElement msg_Close;
	
	public PromoteGrpPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void promotionDT() throws InterruptedException {
		
		clickElement(promoteDT);
		waitForElementToDisplay(promDTbutton);
		clickElement(promDTbutton);
				
		
}
	public void promotionTC() throws InterruptedException {
		
		clickElement(msg_Close);
		clickElement(promoteTC);
		waitForElementToDisplay(iSuptckt);
		setInput(iSuptckt, "1523");
		clickElement(runCB);
		clickElement(runNow);
				
		
}
	public void promotionCP() throws InterruptedException {
		
		clickElement(msg_Close);
		clickElement(promoteCP);
		waitForElementToDisplay(iSuptckt);
		setInput(iSuptckt, "1523");
		clickElement(runCB);
		clickElement(runNow);
				
		
	}
}