package pages.IBCg2;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBC_formsPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h1[text()=' Forms ']")
	 public WebElement pageH1;
	
	@FindBy(xpath = "//span[text()='Wealth Forms']")
	 public WebElement wlth_frms;
	
	@FindBy(xpath = "//span[text()='Health Forms']")
	 public WebElement hlth_frms;
	
	
	public IBC_formsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
		
}
	
	public void multipleform() throws InterruptedException {
		waitForElementToDisplay(hlth_frms);
		clickElement(hlth_frms);
		clickElement(wlth_frms);
	}
	
	
}
