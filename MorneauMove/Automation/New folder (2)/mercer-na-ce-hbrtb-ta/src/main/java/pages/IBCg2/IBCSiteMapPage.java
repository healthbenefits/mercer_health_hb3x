package pages.IBCg2;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBCSiteMapPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h1[text()=' Site Map ']")
	 public WebElement pageH1;
	
	@FindBy(xpath = "//span[text()='Health Overview']")
	 public WebElement hlth_OVLink;
	
	@FindBy(xpath = "//div[@class='SupportOverviewTitleClass']")
	 public WebElement supportLink;
	
	@FindBy(xpath = "//div[@class='FormsOverviewClass']")
	 public WebElement formsLink;
	
	
	
	public IBCSiteMapPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
		
}
	
	
}
