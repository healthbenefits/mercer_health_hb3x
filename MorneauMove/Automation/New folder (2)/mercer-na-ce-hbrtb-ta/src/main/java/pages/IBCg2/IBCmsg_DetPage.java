package pages.IBCg2;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBCmsg_DetPage {
	
	ExtentTest test =null;
	

	@FindBy(xpath = "//h1[@class='page_title']")
	 public WebElement page_Head;
	
	
	@FindBy(xpath = "//a[text()=' Reply ']")
	 public WebElement rep_Msg;
	
	public IBCmsg_DetPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void repmsg() throws InterruptedException {
		waitForElementToDisplay(rep_Msg);
		clickElement(rep_Msg);
	}
	}