package pages.VIPAdm;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class CarrierDBPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h2[text()='Carrier Dashboard']")
	 public WebElement pagehead;
	
	@FindBy(xpath = "//a[text()='Carriers']")
	 public WebElement carriersTab;
	
	@FindBy(xpath = "//a[text()='Schedules']")
	 public WebElement schedulesTab;
	
	@FindBy(xpath = "//a[text()='Thresholds']")
	 public WebElement thresholdsTab;
	
	@FindBy(xpath = "//a[text()='Premiums']")
	 public WebElement premiumsTab;
	
	@FindBy(xpath = "//a[text()='Current Ongoing Cycle']")
	 public WebElement curOGcycTab;
	
	@FindBy(xpath = "//a[text()='Current Open Enrollment Cycle']")
	 public WebElement curOEcycTab;
	
	 @FindBy(xpath = "//a[text()='Time Impersonate OG Cycle']")
	 public WebElement tiOGcycTab;
	 
	 @FindBy(xpath = "(//a[@class='app-go-to-breakdown'])[1]")
	 public WebElement carrier1;
	  
		
	public CarrierDBPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
		
}
	public void carrierslct() throws InterruptedException {
		
		waitForElementToDisplay(carrier1);
		clickElement(carrier1);
		}
	
	public void currOEcycle() throws InterruptedException {
		
		waitForElementToDisplay(curOEcycTab);
		clickElement(curOEcycTab);
		
		}  
	
}
