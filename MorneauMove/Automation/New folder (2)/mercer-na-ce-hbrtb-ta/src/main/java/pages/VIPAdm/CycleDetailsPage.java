package pages.VIPAdm;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class CycleDetailsPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//small[text()='Cycle Details']")
	 public WebElement cycle_dtl;
	
	@FindBy(xpath = "//a[text()='Export']")
	 public WebElement export_excel;
	
	@FindBy(xpath = "//a[text()='Next Carrier']")
	 public WebElement nxt_carrier;
	
	@FindBy(xpath = "(//span[text()='Allstars'])[1]")
	 public WebElement carr2name;
	
	@FindBy(xpath = "//a[text()='Back to Cycle Breakdown']")
	 public WebElement back_cb;
	
		
	public CycleDetailsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
		
}
	public void nxtcrrclick() throws InterruptedException {
		
		waitForElementToDisplay(nxt_carrier);
		clickElement(nxt_carrier);
		}
	
public void backtocb() throws InterruptedException {
		
		waitForElementToDisplay(back_cb);
		clickElement(back_cb);
		}
	
}
