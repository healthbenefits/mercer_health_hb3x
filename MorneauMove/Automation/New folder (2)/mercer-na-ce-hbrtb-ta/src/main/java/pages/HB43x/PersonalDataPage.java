package pages.HB43x;

import static driverfactory.Driver.setInput;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeNoException;
import static verify.SoftAssertions.verifyElementTextContains;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.Executor;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.gargoylesoftware.htmlunit.javascript.host.dom.Document;

import driverfactory.Driver;
import verify.SoftAssertions;

import static driverfactory.Driver.*;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;

public class PersonalDataPage {

	ExtentTest test =null;

	@FindBy(id = "ctl00_MainBody_HBWizardNavBar1_NavBar_ctl01_SpanText")
	public WebElement my_Personal_Data;//  Review Personal Data

	@FindBy(id = "ctl00_MainBody_ProcessLifeWizard___CustomNav0_StartNextButton1")
	WebElement next1;

	public PersonalDataPage(WebDriver driver ) {
		PageFactory.initElements(driver, this);
	}

	public void personalData() throws InterruptedException {
		clickElement(next1);

	}
}
