package pages.HRI_EP;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;


public class MB_SubmitPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//a[@title=\"Submit\"]")
	 public WebElement manualBatchSubmit;
	
	
	public MB_SubmitPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void submit_MB( ) throws InterruptedException {
		
		clickElement(manualBatchSubmit);
		
		
	}
}