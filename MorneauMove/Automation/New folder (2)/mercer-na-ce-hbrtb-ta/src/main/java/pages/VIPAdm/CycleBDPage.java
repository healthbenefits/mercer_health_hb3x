package pages.VIPAdm;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class CycleBDPage {
	
	ExtentTest test =null;
		 
	 @FindBy(xpath = "(//span[text()='Anthem'])[1]")
	 public WebElement carr1name;
	 
	 @FindBy(xpath = "//small[text()='Cycle Breakdown']")
	 public WebElement cycle_bd;
	 
	@FindBy(xpath = "//span[text()='Total']")
	 public WebElement total_tab;
	
	@FindBy(xpath = "//a[text()='Back to Dashboard']")
	 public WebElement back_db;
		
	public CycleBDPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
		
}
	public void total_ppt() throws InterruptedException {
		
		waitForElementToDisplay(total_tab);
		clickElement(total_tab);
		}
	
public void backtodb() throws InterruptedException {
		
		waitForElementToDisplay(back_db);
		clickElement(back_db);
		}
	
}
