package pages.IBCg2;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBCrep_MsgPage {
	
	ExtentTest test =null;
	

	@FindBy(xpath = "//textarea[@name='ctl00$TopazWebPartManager1$twp346218008$ctl11']")
	 public WebElement rep_Tb;
	
	@FindBy(xpath = "//h1[@class='page_title']")
	 public WebElement page_head;
	
	@FindBy(xpath = "//input[@value='Save »']")
	 public WebElement Save_rep;
	
	public IBCrep_MsgPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void repMessg() throws InterruptedException {
		waitForElementToDisplay(rep_Tb);
		setInput(rep_Tb, "testing");
		//clickElement(Save_rep);
	}
	}