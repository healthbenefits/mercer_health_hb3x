package pages.HB43x;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class MyPrsnlDataPage {


	ExtentTest test =null;

	@FindBy(id = "//span[text()='My Personal Data']")
	WebElement myPDTitle;
	
	@FindBy(xpath = "//td[text()='Personal Information']")
	public WebElement perinf_Heading;

	@FindBy(xpath = "//td[text()='Employee Information']")
	public WebElement empinf_Heading;
	
	@FindBy(xpath = "//input[@id='MainBody_EnrollmentMultiView_ContinueBtn']")
	public WebElement contButton;
	
	public MyPrsnlDataPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void currentCoverageNext() throws InterruptedException {
		
		
		
	}
}
