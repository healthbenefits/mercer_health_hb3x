package pages.HB43x;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HB43xSubmitPage {



	@FindBy(id = "ctl00_MainBody_ProcessLifeWizard_ctl15_ReviewAndSubmitBtn")
	WebElement review_Submit;


	public HB43xSubmitPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	public void reviewSubmit() throws InterruptedException {
		clickElement(review_Submit);
	}
}
