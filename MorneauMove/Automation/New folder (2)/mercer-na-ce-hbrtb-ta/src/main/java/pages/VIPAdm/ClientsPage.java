package pages.VIPAdm;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class ClientsPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h2[text()='Clients']")
	 public WebElement pagehead;
	
	@FindBy(xpath = "//input[@type='text']")
	 public WebElement filter_Tb;
	
	
	@FindBy(xpath = "//*[@id=\"applicationHost\"]/div/section/div/table/tbody/tr[2]/td[1]/a")
	 public WebElement selClient;
	
	
	public ClientsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
		
}
	public void clientClick() throws InterruptedException {
	
	waitForElementToDisplay(filter_Tb);
	setInput(filter_Tb, "QAACME");
	clickElement(selClient);
	}
	
}
