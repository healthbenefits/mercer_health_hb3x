package pages.HB43x;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class MyFamDataPage {


	ExtentTest test =null;

	@FindBy(id = "//span[@class='txtPageTitle']")
	WebElement fampageTitle;
	
	@FindBy(xpath = "//span[@id='MainBody_ctl00_RevDeps_TableHeader']")
	public WebElement verify_Famtext;
	
	@FindBy(xpath = "//input[@title='Click here to Add a New Dependent']")
	public WebElement add_Fam;
	
	@FindBy(xpath = "//input[@name='ctl00$MainBody$EnrollmentMultiView$DependentMultiview$EditDep$DependentDetail$DependentDetailsView$txtFirstName']")
	public WebElement firstNameTB;
	
	@FindBy(xpath = "//input[@name='ctl00$MainBody$EnrollmentMultiView$DependentMultiview$EditDep$DependentDetail$DependentDetailsView$txtLastName']")
	public WebElement lastNameTB;
	
	@FindBy(xpath = "//select[@id='MainBody_EnrollmentMultiView_DependentMultiview_EditDep_DependentDetail_DependentDetailsView_relationList']")
	public WebElement relationDD;
	
	@FindBy(xpath = "//input[@id='MainBody_EnrollmentMultiView_DependentMultiview_EditDep_DependentDetail_DependentDetailsView_DepDateEntry_DateBox1']")
	public WebElement dobTB;
	
	@FindBy(xpath = "//input[@id='MainBody_EnrollmentMultiView_DependentMultiview_EditDep_DependentDetail_DependentDetailsView_txtSSN']")
	public WebElement ssnTB;
	
	@FindBy(xpath = "//input[@id='MainBody_EnrollmentMultiView_DependentMultiview_EditDep_DependentDetail_DependentDetailsView_genderList_1']")
	public WebElement sel_gender;
	
	@FindBy(xpath = "//select[@id='MainBody_EnrollmentMultiView_DependentMultiview_EditDep_DependentDetail_DependentDetailsView_medicareEligibilityStatusList']")
	public WebElement med_eleg;
	
	@FindBy(xpath = "//select[@id='MainBody_EnrollmentMultiView_DependentMultiview_EditDep_DependentDetail_DependentDetailsView_stateTaxExclusionList']")
	public WebElement tax_exc;
	
	@FindBy(xpath = "//select[@id='MainBody_EnrollmentMultiView_DependentMultiview_EditDep_DependentDetail_DependentDetailsView_chkEmpAddress']")
	public WebElement add_cb;
	
	@FindBy(xpath = "//input[@id='MainBody_EnrollmentMultiView_ContinueBtn']")
	public WebElement cont_Button;
	
	
	public MyFamDataPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void currentCoverageNext() throws InterruptedException {
		
		
		
	}
}
