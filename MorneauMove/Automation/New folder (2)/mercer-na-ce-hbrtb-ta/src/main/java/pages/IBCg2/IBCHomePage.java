package pages.IBCg2;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBCHomePage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//span[text()='Contact Us']")
	 public WebElement msg_Con;
	
	@FindBy(xpath = "//span[text()='Health']")
	 public WebElement health_tb;
	
	@FindBy(xpath = "//span[text()='Wealth']")
	 public WebElement wealth_tb;
	
	@FindBy(xpath = "//span[text()='Total Rewards']")
	 public WebElement rewards_tb;
	
	@FindBy(xpath = "//span[text()='Forms']")
	 public WebElement forms_tb;
	
	@FindBy(xpath = "//span[text()='Resource Center']")
	 public WebElement ressrc_tb;
	
	@FindBy(xpath = "//a[text()=' Privacy Policy ']")
	 public WebElement footlinkpp;
	
	@FindBy(xpath = "//a[text()=' Terms of Use ']")
	 public WebElement footlinktou;
	
	@FindBy(xpath = "//a[text()='Contact Us']")
	 public WebElement footlinkconus;
		
	@FindBy(xpath = "//span[text()='Resource Center']")
	 public WebElement rsccentre;
	
	@FindBy(xpath = "//span[text()='Site Map']")
	 public WebElement site_mp;
	
	@FindBy(xpath = "//span[@title=' Take Action ']")
	 public WebElement action_rt;
	
	@FindBy(xpath = "//a[text()=' View all action items ']")
	 public WebElement all_act;
	
	@FindBy(xpath = "//span[text()='Total Rewards']")
	 public WebElement tot_rwrds;
	
	@FindBy(xpath = "//img[@src='https://qai.ibenefitcenter.com//Images/QAWORK/WEB/en/icon_print_blue.gif']")
	 public WebElement print_icon;
	
	
	
	
	public IBCHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	
	
	public void contactUs() throws InterruptedException {
		waitForElementToDisplay(msg_Con);
		clickElement(msg_Con);
	}
	
	public void health() throws InterruptedException {
		waitForElementToDisplay(health_tb);
		clickElement(health_tb);
	}
	
	public void rewards() throws InterruptedException {
		waitForElementToDisplay(rewards_tb);
		clickElement(rewards_tb);
		}
	
	public void forms() throws InterruptedException {
		waitForElementToDisplay(forms_tb);
		clickElement(forms_tb);
		
	}	
	
	public void rescr() throws InterruptedException {
		waitForElementToDisplay(ressrc_tb);
		clickElement(ressrc_tb);
		
	}	
	
	public void sitemp() throws InterruptedException {
		waitForElementToDisplay(site_mp);
		clickElement(site_mp);
		
	}	
	
	public void alert() throws InterruptedException {
		waitForElementToDisplay(all_act);
		clickElement(all_act);
		
	}
	
	public void wealth() throws InterruptedException {
		waitForElementToDisplay(wealth_tb);
		clickElement(wealth_tb);
		
	}
	
	

}
