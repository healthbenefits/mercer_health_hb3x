package pages.HB43x;

import static driverfactory.Driver.setInput;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeNoException;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.Executor;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.gargoylesoftware.htmlunit.javascript.host.dom.Document;

import driverfactory.Driver;
import verify.SoftAssertions;


import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;

public class DepVerificationPage {

	ExtentTest test =null;

	@FindBy(css = "input[name*='FileUpload']")
	WebElement fileUL;

	@FindBy(id = "MainBody_DepVerificationCtrl_btnUploadAll")
	WebElement upload_Documents;
	//ctl00_MainBody_Confirm1_DepVerificationCtrl_btnUploadAll

	@FindBy(xpath = "//div[@id='MainBody_DepVerificationCtrl_pnlUploadConfirm']//b[text()='You have successfully uploaded your document(s).'] ")
	public WebElement ud_Confirmation;

	@FindBy(id = "ctl00_MainBody_Confirm1_ContinueBtn")
	WebElement dep_Continue;

	public DepVerificationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	public void depDocumentUpload() throws InterruptedException {

		waitForElementToDisplay(upload_Documents);
		File uploadedFile = new File("./src/main/resources/testdata/Upload.png"); 
		fileUL.sendKeys(uploadedFile.getAbsolutePath()); 
		clickElement(upload_Documents);

	}
	public void depContinue() throws InterruptedException{
		clickElement(dep_Continue);
	}


}


