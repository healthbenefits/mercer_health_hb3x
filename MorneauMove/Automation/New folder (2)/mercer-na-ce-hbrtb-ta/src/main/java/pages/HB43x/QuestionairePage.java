package pages.HB43x;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class QuestionairePage {

	
	@FindBy(id = "ctl00_MainBody_RecalcOnTheFlyProcessControl1_RecalcQuestionStep__pptCollectedData2__yesAnswer")
	WebElement rotf1;
	
	
	@FindBy(id = "ctl00_MainBody_RecalcOnTheFlyProcessControl1_RecalcQuestionStep__pptCollectedData2__noAnswer")
	WebElement rotf2;
	
	@FindBy(id = "ctl00_MainBody_RecalcOnTheFlyProcessControl1_RecalcQuestionStep__continueButton")
	WebElement rotf_Continue;//(4)
	
	
	public QuestionairePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	
	public void questionaire() throws InterruptedException {
		
		waitForElementToDisplay(rotf1);
		if(rotf1.isSelected()){
			clickElement(rotf2);
		}
		else {
			clickElement(rotf1);
		}
		for(int i=0; i<=3; i++) {
		clickElement(rotf_Continue);

	}
}
}
