package pages.IBCg2;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBC_alertsPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h1[text()=' Your Action Items and Alerts Test ']")
	 public WebElement pageH1;
	
	
	public IBC_alertsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
		
}
	
	
}
