package pages.IBCg2;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBCcon_UsPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h1[@class='page_title']")
	 public WebElement pageH1;
	

	@FindBy(xpath = "//a[text()=' Visit the Message Center ']")
	 public WebElement msg_Ctrlnk;
	
	@FindBy(xpath = "//span[text()=' Chat Online  ']")
	 public WebElement chat_1;
	
	@FindBy(xpath = "//span[text()=' Email Us ']")
	 public WebElement email_2;
	
	@FindBy(xpath = "//span[text()='  Call Us ']")
	 public WebElement call_3;
	
	
	
	public IBCcon_UsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
		
}
	
	public void contactUs() throws InterruptedException {
		waitForElementToDisplay(msg_Ctrlnk);
		clickElement(msg_Ctrlnk);
	
	}
	
}
