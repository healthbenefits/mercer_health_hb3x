package pages.IBCg2;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBCmsg_CtrPage {
	
	ExtentTest test =null;
	

	@FindBy(xpath = "//a[text()='  Create a Message ']")
	 public WebElement crt_Msg;
	
	
	@FindBy(xpath = "(//a[@class='LinkCssClass'])[1]")
	 public WebElement msg_Rep;
	
	public IBCmsg_CtrPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void crtMesg() throws InterruptedException {
		waitForElementToDisplay(crt_Msg);
		clickElement(crt_Msg);
		
	}
	
	public void msgopen() throws InterruptedException {
		waitForElementToDisplay(msg_Rep);
		clickElement(msg_Rep);
		
	}
	}