package pages.HB43x;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.*;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class BeneficiariesPage {

	ExtentTest test =null;

	@FindBy(id = "ctl00_MainBody_HBWizardNavBar1_NavBar_ctl04_SpanText")
	public WebElement reviewBenTxt;

	@FindBy(id = "ctl00_MainBody_ProcessLifeWizard___CustomNav3_StepNextButton4")
	WebElement next4;

	public BeneficiariesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	public void beneficiaryNext( ) throws InterruptedException {
		clickElement(next4);
	}
}
