package pages.IBCg2;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBCcrt_MsgPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h1[@class='page_title']")
	 public WebElement page_head;
	

	@FindBy(xpath = "(//select[@class='form_text_MsgCreate'])[1]")
	 public WebElement dd1;
	

	@FindBy(xpath = "(//select[@class='form_text_MsgCreate'])[2]")
	 public WebElement dd2;
	

	@FindBy(xpath = "(//input[@class='form_text_MsgCreate'])[1]")
	 public WebElement sub_Tb;
	

	@FindBy(xpath = "//textarea[@class='form_text_MsgCreate']")
	 public WebElement comm_Tb;
	
	@FindBy(xpath = "(//input[@class='form_text_MsgCreate'])[2]")
	 public WebElement email_Tb;
	
	@FindBy(xpath = "//input[@value=' Send ']")
	 public WebElement send_Btn;
	
	public IBCcrt_MsgPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}

	public void msgDets() throws InterruptedException {
		waitForElementToDisplay(dd1);
		selEleByVisbleText(dd1, "Make a suggestion");
		selEleByVisbleText(dd2, "Health");
		setInput(sub_Tb, "test Subject");
		setInput(comm_Tb, "test comments");
		setInput(email_Tb, "kishan.kashyap@mercer.com");
		clickElement(send_Btn);
	}
	}