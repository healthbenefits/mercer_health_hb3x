package pages.IBCg2;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class IBCrsc_CenterPage {
	
	ExtentTest test =null;
	

	@FindBy(xpath = "//span[text()='Health Education']")
	 public WebElement health_edu;
	
	@FindBy(xpath = "//span[text()='Wealth Education']")
	 public WebElement wealth_edu;
	
	@FindBy(xpath = "//span[text()='Changes in Your Life']")
	 public WebElement ciyl_link;
	
	@FindBy(xpath = "(//span[text()='Tools'])[2]")
	 public WebElement tools_link;
	
	@FindBy(xpath = "//span[text()='Newsletters']")
	 public WebElement newsltr_link;
	
	@FindBy(xpath = "//span[text()='Plan Information']")
	 public WebElement plan_info;
	
	@FindBy(xpath = "//span[text()='News']")
	 public WebElement news_link;
		
	
	public IBCrsc_CenterPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void lyfchgs() throws InterruptedException {
		waitForElementToDisplay(ciyl_link);
		clickElement(ciyl_link);
		}
	
	public void tools() throws InterruptedException {
		waitForElementToDisplay(tools_link);
		clickElement(tools_link);
	}
	
	public void nwsltr() throws InterruptedException {
		waitForElementToDisplay(newsltr_link);
		clickElement(newsltr_link);
	}
	
	public void planinfo() throws InterruptedException {
		waitForElementToDisplay(plan_info);
		clickElement(plan_info);
	}
	
	public void health_edu() throws InterruptedException {
		waitForElementToDisplay(health_edu);
		clickElement(health_edu);
	}//https://qai.ibenefitcenter.com/QAWORK/WealthEducation.tpz;;https://qai.ibenefitcenter.com/QAWORK/HealthEducation.tpz
	
	public void wealth_edu() throws InterruptedException {
		waitForElementToDisplay(wealth_edu);
		clickElement(wealth_edu);
	}
	
	public void newsclick() throws InterruptedException {
		waitForElementToDisplay(news_link);
		clickElement(news_link);
	}
	
	}

//Plan Information 