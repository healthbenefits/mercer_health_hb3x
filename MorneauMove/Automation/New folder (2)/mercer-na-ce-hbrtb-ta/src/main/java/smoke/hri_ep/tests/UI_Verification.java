package smoke.hri_ep.tests;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HRI_EP.Adm_BEPage;
import pages.HRI_EP.Adm_HRIPage;
import pages.HRI_EP.ClientHomePage;
import pages.HRI_EP.Client_Page;
import pages.HRI_EP.Con_BEPage;
import pages.HRI_EP.Con_HRIPage;
import pages.HRI_EP.EventTriggerPage;
import pages.HRI_EP.FileConfigPage;
import pages.HRI_EP.HomeButtonPage;
import pages.HRI_EP.MB_SubmitPage;
import pages.HRI_EP.ManualBatchPage;
import pages.HRI_EP.OnHoldLEPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class UI_Verification extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  UI_Verification(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		UI_Verification Tc01 = new UI_Verification("HRI");
		
	}
	@Parameters({"Execute","BASEURL","Client","Verify1","Verify2","File","Verify3","StreamConID","Verify4", "Verify5", "Verify6", "Verify7", "Verify8"})
	@Test(priority = 1, enabled = true)
	public void HRI_EP(String Execute, String BASEURL, String Client, String Verify1, String Verify2, String File , String Verify3, String StreamConID, String Verify4, String Verify5, String Verify6, String Verify7, String Verify8) throws Exception {
		try {
			
			test = reports.createTest("HRI/EP--verify Login To HRI/EP--" + BROWSER_TYPE);
			test.assignCategory("smoke");
			if (Execute.contentEquals("Y")) {
			webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			Client_Page clientSelect = new Client_Page(driver); 
			clientSelect.clientClick(Client);
			
			ClientHomePage clientHome= new ClientHomePage(driver);
			waitForElementToDisplay(clientHome.client_HomePage_Verify);
			SoftAssertions.verifyElementTextContains(clientHome.client_HomePage_Verify,Client,test);
			clientHome.con_BE_Click();
			
			Con_BEPage conUI= new Con_BEPage(driver);
			waitForElementToDisplay(conUI.be_Title);
			SoftAssertions.verifyElementTextContains(conUI.be_Title,Verify1,test);
			
			HomeButtonPage homeclick= new HomeButtonPage(driver);
			homeclick.homePage();
			waitForElementToDisplay(clientHome.client_HomePage_Verify);
			clientHome.con_HRI_Click();
			
			Con_HRIPage conHRI= new Con_HRIPage(driver);
			waitForElementToDisplay(conHRI.hri_Title);
			SoftAssertions.verifyElementTextContains(conHRI.hri_Title,Verify2,test);
			conHRI.searchFLConfig(File);
			
			FileConfigPage filecon= new FileConfigPage(driver);
			waitForElementToDisplay(filecon.flCon_Title);
			SoftAssertions.verifyElementTextContains(filecon.flCon_Title,Verify3,test);
			String strConID=String.valueOf(StreamConID.replace(".0", ""));
			SoftAssertions.verifyElementTextContains(filecon.strm_ConID,strConID,test);
			filecon.et_Tab();
			
			EventTriggerPage etverify= new EventTriggerPage(driver);
			waitForElementToDisplay(etverify.et_Title);
			SoftAssertions.verifyElementTextContains(etverify.et_Title,Verify4,test);
			
			homeclick.homePage();
			waitForElementToDisplay(clientHome.client_HomePage_Verify);
			clientHome.adm_BE_Click();
			
			Adm_BEPage admBE= new Adm_BEPage(driver);
			waitForElementToDisplay(admBE.be_title);
			SoftAssertions.verifyElementTextContains(admBE.be_title,Verify5,test);
			admBE.steb_Click();
			waitForElementToDisplay(admBE.adm_STEB);
			SoftAssertions.verifyElementTextContains(admBE.adm_STEB,Verify6,test);
			Thread.sleep(2000);
			admBE.onHold_Click();
			waitForElementToDisplay(admBE.adm_OHLE);
			SoftAssertions.verifyElementTextContains(admBE.adm_OHLE,Verify7,test);
			
			homeclick.homePage();
			waitForElementToDisplay(clientHome.client_HomePage_Verify);
			clientHome.adm_HRI_Click();
			
			Adm_HRIPage admHRI= new Adm_HRIPage(driver);
			waitForElementToDisplay(admHRI.hri_Title);
			SoftAssertions.verifyElementTextContains(admHRI.hri_Title,Verify8,test);
			
			}
		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
