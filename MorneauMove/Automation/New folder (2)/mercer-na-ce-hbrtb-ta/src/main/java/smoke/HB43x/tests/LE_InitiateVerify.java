package smoke.HB43x.tests;
import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.apache.poi.hssf.record.PageBreakRecord.Break;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HB43x.BeneficiariesPage;
import pages.HB43x.CoveragePage;
import pages.HB43x.DepVerificationPage;
import pages.HB43x.FamilyDataPage;
import pages.HB43x.HealthPage;
import pages.HB43x.LEChangeVerificationPage;
import pages.HB43x.LEInitiatePage;
import pages.HB43x.LandingPage;
import pages.HB43x.LoginPage;
import pages.HB43x.QuestionairePage;
import pages.HB43x.RecalcPage;
import pages.HB43x.HB43xSubmitPage;
import pages.HB43x.CongratsPage;
import pages.HB43x.PersonalDataPage;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.*;
import static driverfactory.Driver.waitForElementToDisplay;

public class LE_InitiateVerify    extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public LE_InitiateVerify  (String appName) {
		super(appName);
	}

	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		LE_InitiateVerify Tc01 = new LE_InitiateVerify("HB43x");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
	}

	@Test(enabled = true)
	public void login() throws Exception{
		try {

			test = reports.createTest("HB43x--verify Login To HB43x--" + BROWSER_TYPE);
			test.assignCategory("smoke");

			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);


			LoginPage loginPage = new LoginPage(driver); 
			loginPage.login(USERNAME, PASSWORD);

			LandingPage landingPage= new LandingPage(driver);
			landingPage.landingHealth();

			HealthPage lsChange= new HealthPage(driver);
			lsChange.lifeStatuschange();

			LEInitiatePage leInitiate= new LEInitiatePage(driver);
			waitForElementToDisplay(leInitiate.verify_LEI_Page);
			verifyElementText(leInitiate.verify_LEI_Page, "Process a Life Status Change",test);
			leInitiate.leInitiate();

			LEChangeVerificationPage leChangeVerify= new LEChangeVerificationPage(driver);
			waitForElementToDisplay(leChangeVerify.lecv_Title);
			verifyElementTextContains(leChangeVerify.lecv_Title, "Life Event Change Verification",test);
			leChangeVerify.leVerify();

			PersonalDataPage personalData= new PersonalDataPage(driver );
			waitForElementToDisplay(personalData.my_Personal_Data);
			verifyElementTextContains(personalData.my_Personal_Data, "  Review Personal Data",test);
			personalData.personalData();

			FamilyDataPage familyData= new FamilyDataPage(driver);
			waitForElementToDisplay(familyData.my_Family_Data);
			verifyElementTextContains(familyData.my_Family_Data, "  Review My Family Data",test);
			familyData.familyDataNext();

			String region= System.getProperty("env");
			System.out.println(region);
			if(region.equalsIgnoreCase("QA")) {
				QuestionairePage questionaire= new QuestionairePage(driver);
				questionaire.questionaire();
			}
			RecalcPage recalc= new RecalcPage(driver);
			waitForElementToDisplay(recalc.recalc_Text);
			verifyElementTextContains(recalc.recalc_Text, "Processing your request. Please wait….",test);

			CoveragePage cCoverage= new CoveragePage(driver);
			waitForElementToDisplay(cCoverage.verify_CCpage);
			verifyElementTextContains(cCoverage.verify_CCpage, "My Benefits",test);
			cCoverage.currentCoverageNext();

			
			if(!region.equalsIgnoreCase("QA")) {
			BeneficiariesPage beneficiaries= new BeneficiariesPage(driver);
			waitForElementToDisplay(beneficiaries.reviewBenTxt);
			verifyElementText(beneficiaries.reviewBenTxt, "  Review Beneficiaries",test);
			beneficiaries.beneficiaryNext();

			}


			HB43xSubmitPage submit= new HB43xSubmitPage(driver);
			submit.reviewSubmit();

			RecalcPage recalc2= new RecalcPage(driver);
			waitForElementToDisplay(recalc2.recalc_Text);
			verifyElementTextContains(recalc2.recalc_Text, "Processing your request. Please wait….",test);

			DepVerificationPage depVerify= new DepVerificationPage(driver);
			depVerify.depContinue();

			
			CongratsPage congratulations= new CongratsPage(driver);
			waitForElementToDisplay(congratulations.congrats_Msg);
			verifyElementTextContains(congratulations.congrats_Msg, "Congratulations, Your Elections Have Been Saved!",test);
			congratulations.congratulationsMessage();


		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("LE Verification failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("LE Verification failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		finally {
			reports.flush();
			driver.quit();
		}
	}
}


