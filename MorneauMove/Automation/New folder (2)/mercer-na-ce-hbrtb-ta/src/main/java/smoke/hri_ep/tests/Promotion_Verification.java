package smoke.hri_ep.tests;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HRI_EP.Adm_BEPage;
import pages.HRI_EP.Adm_HRIPage;
import pages.HRI_EP.ClientHomePage;
import pages.HRI_EP.Client_Page;
import pages.HRI_EP.Con_BEPage;
import pages.HRI_EP.Con_HRIPage;
import pages.HRI_EP.FileConfigPage;
import pages.HRI_EP.HomeButtonPage;
import pages.HRI_EP.MB_SubmitPage;
import pages.HRI_EP.ManualBatchPage;
import pages.HRI_EP.OnHoldLEPage;
import pages.HRI_EP.Promote_FileConfig;
import pages.HRI_EP.Promote_TEBPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class Promotion_Verification extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  Promotion_Verification(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		Promotion_Verification Tc01 = new Promotion_Verification("HRI");
		
	}
	@Parameters({"Execute", "Region","BASEURL","Client","File"})
	@Test(priority = 1, enabled = true)
	public void HRI_EP(String Execute, String Region, String BASEURL, String Client,String File) throws Exception {
		try {
			
			test = reports.createTest("HRI/EP--verify Login To HRI/EP--" + BROWSER_TYPE);
			test.assignCategory("smoke");
			if (Execute.contentEquals("Y")) {
			webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			Client_Page clientSelect = new Client_Page(driver); 
			clientSelect.clientClick(Client);
			
			ClientHomePage clientHome= new ClientHomePage(driver);
			waitForElementToDisplay(clientHome.client_HomePage_Verify);
			SoftAssertions.verifyElementTextContains(clientHome.client_HomePage_Verify,Client,test);
			clientHome.con_BE_Click();
			
			Con_BEPage promoteicon = new Con_BEPage(driver);
			promoteicon.promote_Batch();
			
			Promote_TEBPage promoteTEB = new Promote_TEBPage(driver);
			waitForElementToDisplay(promoteTEB.promote_beTitle);
			verifyElementTextContains(promoteTEB.promote_beTitle, "Batch Events - Promote Time Elapsed Batches", test);
			if (Region.contentEquals("CIDEV") || Region.contentEquals("QA"))
			{
			promoteTEB.promote_tebDT();
			}
			promoteTEB.promote_tebTC();
			promoteTEB.promote_tebCP();
			
			HomeButtonPage home = new HomeButtonPage(driver);
			home.homePage();
		
			waitForElementToDisplay(clientHome.con_HRI);
			clientHome.con_HRI_Click();
			
			Con_HRIPage conhri= new Con_HRIPage(driver);
			waitForElementToDisplay(conhri.hri_Title);
			conhri.searchFLConfig(File);
			
			FileConfigPage promoteclick= new FileConfigPage(driver);
			waitForElementToDisplay(promoteclick.file_PromoteIcon);
			promoteclick.promote_File();
			
			Promote_FileConfig filepromote = new Promote_FileConfig(driver);
			waitForElementToDisplay(filepromote.promote_fcTitle);
			verifyElementTextContains(filepromote.promote_fcTitle, "HR Import - Promote - Ongoing Census", test);
			filepromote.promote_fileDT();
			filepromote.promote_fileTC();
			filepromote.promote_fileCP();
			
			}
		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
