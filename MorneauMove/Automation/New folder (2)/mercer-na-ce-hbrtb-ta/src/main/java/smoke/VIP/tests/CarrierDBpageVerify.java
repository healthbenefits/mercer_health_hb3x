package smoke.VIP.tests;
import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.GMD.Client_Page;
import pages.HB43x.HealthPage;
import pages.HB43x.LandingPage;
import pages.HB43x.LoginPage;
import pages.VIPAdm.CarrierDBPage;
import pages.VIPAdm.ClientsPage;
import pages.VIPAdm.CycleBDPage;
import pages.VIPAdm.CycleDetailsPage;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.*;

public class CarrierDBpageVerify    extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public CarrierDBpageVerify  (String appName) {
		super(appName);
	}

	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		CarrierDBpageVerify Tc01 = new CarrierDBpageVerify("VIP");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
	}

	@Test(enabled = true)
	public void login() throws Exception{
		try {
			test = reports.createTest("VIP admin--verify Carrier Dashboard page look and feel --" + BROWSER_TYPE);
			test.assignCategory("smoke");

			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);

			LoginPage loginPage = new LoginPage(driver); 
			loginPage.login(USERNAME, PASSWORD);

			ClientsPage clientslt= new ClientsPage(driver);
			waitForElementToDisplay(clientslt.pagehead);
			verifyElementTextContains(clientslt.pagehead, "Clients", test);
			clientslt.clientClick();
			
			CarrierDBPage dashboard= new CarrierDBPage(driver);
			waitForElementToDisplay(dashboard.pagehead);
			verifyElementTextContains(dashboard.pagehead, "Carrier Dashboard", test);
			verifyElementTextContains(dashboard.carriersTab, "Carriers", test);
			verifyElementTextContains(dashboard.schedulesTab, "Schedules", test);
			verifyElementTextContains(dashboard.thresholdsTab, "Thresholds", test);
			verifyElementTextContains(dashboard.premiumsTab, "Premiums", test);
			verifyElementTextContains(dashboard.curOGcycTab, "Current Ongoing Cycle", test);
			verifyElementTextContains(dashboard.curOEcycTab, "Current Open Enrollment Cycle", test);
			verifyElementTextContains(dashboard.tiOGcycTab, "Time Impersonate OG Cycle", test);
			dashboard.carrierslct();
			
			CycleBDPage cycbd = new CycleBDPage(driver);
			waitForElementToDisplay(cycbd.carr1name);
			verifyElementTextContains(cycbd.carr1name, "Anthem", test);
			verifyElementTextContains(cycbd.cycle_bd, "Cycle Breakdown", test);
			cycbd.total_ppt();
			
			CycleDetailsPage cycdtls= new CycleDetailsPage(driver);
			waitForElementToDisplay(cycdtls.cycle_dtl);
			verifyElementTextContains(cycdtls.cycle_dtl, "Cycle Details", test);
			verifyElementTextContains(cycdtls.export_excel, "Export", test);
			cycdtls.nxtcrrclick();
			waitForElementToDisplay(cycdtls.carr2name);
			verifyElementTextContains(cycdtls.carr2name, "Allstars", test);
			cycdtls.backtocb();
			cycbd.backtodb();
			
			dashboard.currOEcycle();
			dashboard.carrierslct();
			waitForElementToDisplay(cycbd.carr1name);
			verifyElementTextContains(cycbd.carr1name, "Anthem", test);
			verifyElementTextContains(cycbd.cycle_bd, "Cycle Breakdown", test);
			cycbd.total_ppt();
			waitForElementToDisplay(cycdtls.cycle_dtl);
			verifyElementTextContains(cycdtls.cycle_dtl, "Cycle Details", test);
			verifyElementTextContains(cycdtls.export_excel, "Export", test);
			cycdtls.nxtcrrclick();
			waitForElementToDisplay(cycdtls.carr2name);
			verifyElementTextContains(cycdtls.carr2name, "Allstars", test);
			cycdtls.backtocb();
			cycbd.backtodb();


		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login Verification failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login Verification failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}

		finally {
			reports.flush();
			driver.quit();
		}
	}
}





