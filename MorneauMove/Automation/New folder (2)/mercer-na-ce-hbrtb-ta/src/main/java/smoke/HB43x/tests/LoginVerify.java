package smoke.HB43x.tests;
import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HB43x.HealthPage;
import pages.HB43x.LandingPage;
import pages.HB43x.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.*;

public class LoginVerify    extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;

	public LoginVerify  (String appName) {
		super(appName);
	}

	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		LoginVerify Tc01 = new LoginVerify("HB43x");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
	}

	@Test(enabled = true)
	public void login() throws Exception{
		try {
			test = reports.createTest("HB43x--verify Login To HB43x--" + BROWSER_TYPE);
			test.assignCategory("smoke");

			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);

			LoginPage loginPage = new LoginPage(driver); 
			loginPage.login(USERNAME, PASSWORD);

			LandingPage landingPage= new LandingPage(driver);
			landingPage.landingHealth();

			HealthPage healthVerify= new HealthPage(driver);
			waitForElementToDisplay(healthVerify.action_Tb);
			verifyElementTextContains(healthVerify.action_Tb," Take Action ",test);
			healthVerify.healthVerify(driver, urlVerify,test);


		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login Verification failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login Verification failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}

		finally {
			reports.flush();
			driver.quit();
		}
	}
}





