package regression.IBCG2.tests;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.IBCg2.IBCHomePage;
import pages.IBCg2.IBCLoginPage;
import pages.IBCg2.IBCcon_UsPage;
import pages.IBCg2.IBCcrt_MsgPage;
import pages.IBCg2.IBCmsg_CtrPage;
import pages.IBCg2.IBCmsg_DetPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class ContactusVerify extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  ContactusVerify(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		ContactusVerify Tc01 = new ContactusVerify("IBC");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	@Test(priority = 1, enabled = true)
	public void contactus() throws Exception {
		try {
			test = reports.createTest("IBC--verify Contact Us Page--" + BROWSER_TYPE);
			test.assignCategory("regression");
			
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			IBCLoginPage login = new IBCLoginPage(driver); 
			login.login(USERNAME, PASSWORD);
			
			IBCHomePage conus = new IBCHomePage(driver);
			conus.contactUs();
			switchToWindowWithURL("https://qai.ibenefitcenter.com/QAWORK/ContactUsHome.tpz", driver);
			
			IBCcon_UsPage contus= new IBCcon_UsPage(driver);
			waitForElementToDisplay(contus.pageH1);
			verifyElementTextContains(contus.pageH1, " Contact Us ", test);
			verifyElementTextContains(contus.chat_1, " Chat Online  ", test);
			verifyElementTextContains(contus.email_2,"  Email Us ", test);
			verifyElementTextContains(contus.call_3, "  Call Us ", test);
			verifyElementTextContains(contus.msg_Ctrlnk, " Visit the Message Center ", test);
					
		
						
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("UI Verification failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("UI Verification failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
		
