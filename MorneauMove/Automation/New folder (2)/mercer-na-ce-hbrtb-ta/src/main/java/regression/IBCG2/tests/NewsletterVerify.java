package regression.IBCG2.tests;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.IBCg2.IBC_changesInLYFPage;
import pages.IBCg2.IBCHomePage;
import pages.IBCg2.IBCLoginPage;
import pages.IBCg2.IBCToolsPage;
import pages.IBCg2.IBC_newsletterPage;
import pages.IBCg2.IBCcon_UsPage;
import pages.IBCg2.IBCcrt_MsgPage;
import pages.IBCg2.IBCmsg_CtrPage;
import pages.IBCg2.IBCmsg_DetPage;
import pages.IBCg2.IBCrsc_CenterPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class NewsletterVerify extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  NewsletterVerify(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		NewsletterVerify Tc01 = new NewsletterVerify("IBC");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	@Test(priority = 1, enabled = true)
	public void newsltr() throws Exception {
		try {
			test = reports.createTest("IBC--verify NewsLetters page--" + BROWSER_TYPE);
			test.assignCategory("regression");
			
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			IBCLoginPage login = new IBCLoginPage(driver); 
			login.login(USERNAME, PASSWORD);
			
			IBCHomePage resctr= new IBCHomePage(driver);
			resctr.rescr();
			
			IBCrsc_CenterPage nwsltr= new IBCrsc_CenterPage(driver);
			nwsltr.nwsltr();
			
			IBC_newsletterPage news= new IBC_newsletterPage(driver);
			waitForElementToDisplay(news.pageH1);
			verifyElementTextContains(news.pageH1, " Newsletters ", test);    
					
		
						
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("UI Verification failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("UI Verification failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
		
