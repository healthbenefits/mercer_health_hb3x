package regression.IBCG2.tests;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.IBCg2.IBCHomePage;
import pages.IBCg2.IBCLoginPage;
import pages.IBCg2.IBCcon_UsPage;
import pages.IBCg2.IBCcrt_MsgPage;
import pages.IBCg2.IBCmsg_CtrPage;
import pages.IBCg2.IBCmsg_DetPage;
import pages.IBCg2.IBCrep_MsgPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class ReplyMessageVerify extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  ReplyMessageVerify(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		ReplyMessageVerify Tc01 = new ReplyMessageVerify("IBC");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	@Test(priority = 1, enabled = true)
	public void replyMsg() throws Exception {
		try {
			test = reports.createTest("IBC--verify Relpying a Message--" + BROWSER_TYPE);
			test.assignCategory("regression");
			
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			IBCLoginPage login = new IBCLoginPage(driver); 
			login.login(USERNAME, PASSWORD);
			
			IBCHomePage conus = new IBCHomePage(driver);
			conus.contactUs();
			switchToWindowWithURL("https://qai.ibenefitcenter.com/QAWORK/ContactUsHome.tpz", driver);
			
			IBCcon_UsPage contus= new IBCcon_UsPage(driver);
			waitForElementToDisplay(contus.pageH1);
			verifyElementTextContains(contus.pageH1, " Contact Us ", test);
			contus.contactUs();
			
			IBCmsg_CtrPage msgopen= new IBCmsg_CtrPage(driver);
			msgopen.msgopen();
			
			
			IBCmsg_DetPage msgdet= new IBCmsg_DetPage(driver);
			msgdet.repmsg();
			waitForElementToDisplay(msgdet.page_Head);
			verifyElementTextContains(msgdet.page_Head, " Reply To a Message ", test);
			
			IBCrep_MsgPage reply= new IBCrep_MsgPage(driver);
			reply.repMessg();
			
			
			
			
						
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("UI Verification failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("UI Verification failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
		
