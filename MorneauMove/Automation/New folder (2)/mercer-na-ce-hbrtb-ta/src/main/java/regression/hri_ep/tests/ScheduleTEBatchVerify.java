package regression.hri_ep.tests;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HRI_EP.Adm_BEPage;
import pages.HRI_EP.ClientHomePage;
import pages.HRI_EP.Client_Page;
import pages.HRI_EP.Con_BEPage;
import pages.HRI_EP.MB_SubmitPage;
import pages.HRI_EP.ManualBatchPage;
import pages.HRI_EP.OnHoldLEPage;
import pages.HRI_EP.ScheduleTEBatchPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class ScheduleTEBatchVerify extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  ScheduleTEBatchVerify(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		ScheduleTEBatchVerify Tc01 = new ScheduleTEBatchVerify("HRI");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	@Test(priority = 1, enabled = true)
	public void ohLE() throws Exception {
		try {
			test = reports.createTest("HRI/EP--verify Time Elapsed Batches-" + BROWSER_TYPE);
			test.assignCategory("regression");
			
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			Client_Page clientSelect = new Client_Page(driver); 
			clientSelect.clientClick(Client2);
			
			ClientHomePage clientHome= new ClientHomePage(driver);
			waitForElementToDisplay(clientHome.client_HomePage_Verify);
			SoftAssertions.verifyElementTextContains(clientHome.client_HomePage_Verify,Client2,test);
			clientHome.con_BE_Click();
			
			Con_BEPage scheduleTEBatch= new Con_BEPage(driver);
			waitForElementToDisplay(scheduleTEBatch.be_Title);
			SoftAssertions.verifyElementTextContains(scheduleTEBatch.be_Title,"Batch Events - Time Elapsed Batches",test);
			System.out.println(Con_BEPage.s);
			scheduleTEBatch.STEB();
			scheduleTEBatch.saveSTEB();
			waitForElementToDisplay(scheduleTEBatch.teb_Confirmation);
			SoftAssertions.verifyElementTextContains(scheduleTEBatch.teb_Confirmation, "You have successfully added the LIFE EVENT rule ", test);
			scheduleTEBatch.home_Click();
			
			
			waitForElementToDisplay(clientHome.adm_BE);
			clientHome.adm_BE_Click();
			
			Adm_BEPage STEBclick= new Adm_BEPage(driver);
			waitForElementToDisplay(STEBclick.sb_TimeElapsed);
			STEBclick.steb_Click();
			
			ScheduleTEBatchPage runNowSTEB= new ScheduleTEBatchPage(driver);
			waitForElementToDisplay(runNowSTEB.steb_Title);
			verifyElementTextContains(runNowSTEB.steb_Title, "Batch Events - Schedule Time Elapsed Batches", test);
			runNowSTEB.runSTEB();
			
			Adm_BEPage tebComplete= new Adm_BEPage(driver);
			tebComplete.teb_Progress(driver);
			waitForElementToDisplay(tebComplete.batch_Complete);
			
			
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Batch Verification failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Batch Verification failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
