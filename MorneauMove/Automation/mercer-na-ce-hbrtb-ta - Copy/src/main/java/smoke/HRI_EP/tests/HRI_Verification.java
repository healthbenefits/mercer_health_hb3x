package smoke.HRI_EP.tests;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HRI_EP.Adm_BEPage;
import pages.HRI_EP.Adm_HRIPage;
import pages.HRI_EP.ClientHomePage;
import pages.HRI_EP.Client_Page;
import pages.HRI_EP.Con_BEPage;
import pages.HRI_EP.MB_SubmitPage;
import pages.HRI_EP.ManualBatchPage;
import pages.HRI_EP.OnHoldLEPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class HRI_Verification extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  HRI_Verification(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		HRI_Verification Tc01 = new HRI_Verification("HRI");
		
	}
	@Parameters({"Execute", "BASEURL","Client","File_id","Soap_endpointURL","Soap_resourcePath"})
	@Test(priority = 1, enabled = true)
	public void HRI_EP(String Execute, String BASEURL, String Client,String File_id,String Soap_endpointURL, String Soap_resourcePath) throws Exception {
		try {
			
			test = reports.createTest("HRI/EP--verify SOAP-UI File Process--" + BROWSER_TYPE);
			test.assignCategory("regression");
			if (Execute.contentEquals("Y")) {
			webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			Client_Page clientSelect = new Client_Page(driver); 
			clientSelect.clientClick(Client);
			
			ClientHomePage clientHome= new ClientHomePage(driver);
			waitForElementToDisplay(clientHome.client_HomePage_Verify);
			SoftAssertions.verifyElementTextContains(clientHome.client_HomePage_Verify,Client,test);
			clientHome.adm_HRI_Click();
			
			Adm_HRIPage triggerFile = new Adm_HRIPage(driver);
			waitForElementToDisplay(triggerFile.hri_Title);
			SoftAssertions.verifyElementTextContains(triggerFile.hri_Title,"HR Import - File Dashboard",test);
			triggerFile.search_File(File_id);
			Thread.sleep(2000);
			triggerFile.fileTrigger( Soap_endpointURL, Soap_resourcePath);
			waitForElementToDisplay(triggerFile.fileFilter);
			triggerFile.search_File(File_id);
			Thread.sleep(800000);
			refreshpage();
			waitForElementToDisplay(triggerFile.hri_Title);
		 	triggerFile.search_File(File_id);
			
			
			
			}
		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
