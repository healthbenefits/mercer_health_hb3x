package patchPage;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class MasterPatchPage {
	
	ExtentTest test =null;
	
	
	@FindBy(xpath = "//span[contains(.,'new item')]")
	public WebElement newItem;
		
	@FindBy(xpath = "//input[@title='Patch Name from MGTI Required Field']")
	WebElement patchName;
	
	@FindBy(xpath = "//select[@title='Patch Type Required Field']")
	WebElement patchType;
	
	@FindBy(xpath = "//input[@title='Target Date Required Field']")
	WebElement tarDate;
		
	@FindBy(xpath = "//input[@title='Due Date']")
	WebElement dueDate;
	
	@FindBy(xpath = "//div[@id='Comment_33f72b09-2f4c-43c8-a2ff-cf74ed080fa1_$TextField_inplacerte']")
	WebElement comment_box;

	@FindBy(xpath = "//li[@id=\"Ribbon.ListForm.Edit.Actions\"]")
	WebElement attach_Email;
	
	@FindBy(xpath = "//input[@id='onetidIOFile']")
	WebElement email_Upload;
	
	@FindBy(xpath = "//input[@id='attachOKbutton']")
	WebElement OkButton;
	
	@FindBy(xpath = "//input[@id='ctl00_SPWebPartManager1_g_f5fe4456_0c5a_4ef7_88c9_bc5934851db2_ctl00_toolBarTbl_RightRptControls_ctl00_ctl00_diidIOSaveItem']")
	WebElement SaveButton;
	
	
	public MasterPatchPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void Masterpatch() throws InterruptedException {
		
		waitForElementToDisplay(newItem);
		clickElement(newItem);
		waitForElementToDisplay(patchName);
		setInput(patchName, "Kishan");
		selEleByVisbleText(patchType, "Desktop P1 Patch Verification");
		setInput(tarDate, "8/8/2019");
		setInput(dueDate, "8/8/2019");
		setInput(comment_box, "testing");
		clickElement(attach_Email);
		waitForElementToDisplay(email_Upload);
		setInput(email_Upload, "Upload");
		clickElement(OkButton);
		waitForElementToDisplay(SaveButton);
		/*clickElement(SaveButton);
		*/
			
	}
}