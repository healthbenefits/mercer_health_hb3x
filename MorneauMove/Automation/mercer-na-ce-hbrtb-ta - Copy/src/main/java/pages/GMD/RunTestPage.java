package pages.GMD;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class RunTestPage {
	
	ExtentTest test =null;
	
	
	@FindBy(xpath = "//select[@id='run-test-modal-select-environment']")
	public WebElement test_Env;
	
	@FindBy(xpath = "//input[@id='run-test-modal-participant-ssn']")
	WebElement test_SSN;
	
	@FindBy(xpath = "//input[@id='js-run-test-checkbox-select-all']")
	WebElement grpsTest;
	
	@FindBy(xpath = "//button[@title='Run Test']")
	WebElement runTest;
	
	@FindBy(xpath = "//h4[text()='Test Results']")
	public WebElement testResult;
	
	@FindBy(xpath = "//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close']")
	public WebElement cancelBtn;
	
	
	public RunTestPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void runTest() throws InterruptedException {
		
		waitForElementToDisplay(test_Env);
		selEleByVisbleText(test_Env, "TEST");
		setInput(test_SSN, "100613111");
		clickElement(grpsTest);
		clickElement(runTest);
	}
		public void closeRun() throws InterruptedException {
			clickElement(cancelBtn);
	}
}