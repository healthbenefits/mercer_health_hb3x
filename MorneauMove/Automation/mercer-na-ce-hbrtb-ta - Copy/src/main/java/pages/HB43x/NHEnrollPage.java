package pages.HB43x;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class NHEnrollPage {


	ExtentTest test =null;

	@FindBy(id = "//span[@class='h1']")
	WebElement nhEnrTitle;
	
	@FindBy(xpath = "Enrollment Checklist")
	public WebElement chcklstText;
	
	@FindBy(xpath = "//input[@class='Input0Margin']")
	public WebElement choose;
	
	@FindBy(xpath = "//img[@id='MainBody_EnrollmentMultiView_Chklist_ChooseEnrollItem_EnrollItemImg_0']")
	public WebElement tck1;

	public NHEnrollPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void currentCoverageNext() throws InterruptedException {
		
		
		
	}
}
