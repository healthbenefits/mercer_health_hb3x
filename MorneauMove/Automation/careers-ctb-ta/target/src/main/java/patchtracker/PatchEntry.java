package patchtracker;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;

import patchPage.Patchpage;
import utilities.InitTests;
import verify.SoftAssertions;



public class PatchEntry extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  PatchEntry(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		PatchEntry Tc01 = new PatchEntry("Patch");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	@Parameters({ "m","Patch_name",  "Application", "Event_date", "Time_EST", "Tester", "Test_type","Impact", "Verf_type", "Efforts", "Test_status", "Coordinator", "Comments"})
	@Test(priority = 1, enabled = true)
	public void Patch(String m,String Patch_name, String Application, String Event_date,String Time_EST,String Tester,String Test_type,String Impact, String Verf_type,String Efforts, String Test_status, String Coordinator,String Comments) throws Exception {
		try {
			
			test = reports.createTest("Patch--verify Login To Patch--" + BROWSER_TYPE);
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
			Patchpage patchenter= new Patchpage(driver);
			patchenter.patchtrack(driver,m,  Patch_name, Application, Event_date, Time_EST, Tester, Test_type, Impact, Verf_type, Efforts, Test_status, Coordinator, Comments);
			
	} catch (Exception e) {
		e.printStackTrace();

		fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	}
	
	finally {
		reports.flush();
		driver.quit();
	}
}
}
