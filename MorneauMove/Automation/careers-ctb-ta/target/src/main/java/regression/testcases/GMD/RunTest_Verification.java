package regression.testcases.GMD;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.GMD.AddGroupPage;
import pages.GMD.ClientHomePage;
import pages.GMD.Client_Page;
import pages.GMD.FeaturesARAPage;
import pages.GMD.GroupsLink;
import pages.GMD.ProxyGroupsPage;
import pages.GMD.RunTestPage;
import pages.GMD.VisibilityDatePage;
import utilities.InitTests;
import verify.SoftAssertions;



public class RunTest_Verification extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  RunTest_Verification(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		RunTest_Verification Tc01 = new RunTest_Verification("GMD");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	@Test(priority = 1, enabled = true)
	public void dcBanner() throws Exception {
		try {
			test = reports.createTest("GMD--verify Login To GMD--" + BROWSER_TYPE);
			test.assignCategory("regression");
			
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			Client_Page clientSelect = new Client_Page(driver); 
			clientSelect.clientClick(Client);
			
			ClientHomePage clientUI= new ClientHomePage(driver);
			waitForElementToDisplay(clientUI.clientTitle);
			verifyElementTextContains(clientUI.clientTitle, "QAWORK TESTDROPHB", test);
			clientUI.runTest_Click();
			
			RunTestPage runtst= new RunTestPage(driver);
			runtst.runTest();
						
		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
		
