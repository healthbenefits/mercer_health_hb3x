package smoke.careerview.tests;

import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.fail;
import static verify.SoftAssertions.verifyElementTextContains;
import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.careerview.AdminAnalyticsPage;
import pages.careerview.AdminHeaderPage;
import pages.careerview.AdminImportPage;
import pages.careerview.AdminPage;
import pages.careerview.AdminTaskPage;
import pages.careerview.ComparePopUpPage;
import pages.careerview.HeaderPage;
import pages.careerview.HomePage;
import pages.careerview.LocateMePage;
import pages.careerview.LoginPage;
import pages.careerview.ManagerAssessmentPage;
import pages.careerview.MyProfileSideMenu;
import pages.careerview.NavigatePopUpPage;
import pages.careerview.OverviewPage;
import utilities.InitTests;
import static verify.SoftAssertions.assertTrue;


public class TC1_LoginToCareerView extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public static String dirPath = dir_path;

	public TC1_LoginToCareerView(String appName) {
		super(appName);
	}

	@Test(enabled = true, priority = 1)
	public void loginToCareerView() throws Exception {
		try {
			test = reports.createTest("Logging in to career view application");
			test.assignCategory("smoke");

			TC1_LoginToCareerView careerViewTest = new TC1_LoginToCareerView("careerView");

			System.out.println("Logging in to application");
			

			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);

			LoginPage loginPage = new LoginPage(driver);
			loginPage.loginToCareerView(USERNAME, PASSWORD);

			OverviewPage myProfilePopUp = new OverviewPage(driver);

			assertTrue(isElementExisting(driver, myProfilePopUp.myProfile_button),
					"User logged in to application", test);

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 2)
	public void checkMyProfilePopup() throws IOException {

		try {
			
			test = reports.createTest("check my Profile Popup");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);
			
			OverviewPage myProfilePopUp = new OverviewPage(driver);
			

			myProfilePopUp.goTomyProfile();
			
			verifyElementTextContains(myProfilePopUp.myProfile_header, "My Profile", test);

		

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 3)
	public void updateProfileInfo() throws IOException {
		try {
			test = reports.createTest("check Profile info");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);
			
			OverviewPage myProfilePopUp = new OverviewPage(driver);
	
			myProfilePopUp.editInfo("QA11","About: Checking test11");
			assertTrue(isElementExisting(driver,myProfilePopUp.savedInfoStyle_button),"Information saved successfully", test);

//			 String oldProfilePhoto = (headerPage.profilePhoto).getAttribute("style");
//				System.out.println("old uploaded photo id   ----   " + oldProfilePhoto);
//
//			myProfilePopUp.uploadPhoto();
//			
//			
//			String newProfilePhoto = (headerPage.profilePhoto).getAttribute("style");
//			
//			System.out.println("New uploaded photo id ---- " + newProfilePhoto);
//			
//			SoftAssertions.assertFalse(newProfilePhoto.contentEquals(oldProfilePhoto),
//					"Profile photo addedd successfully", test);
			
	
			MyProfileSideMenu myProfileSideMenu = new MyProfileSideMenu(driver);
			myProfileSideMenu.closePopup();
			
			
		
		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = false, priority = 4)
	public void testForRequestAssessment() throws IOException {
		try {
			
			test = reports.createTest("Checking for request assessment");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);

			HeaderPage headerPage = new HeaderPage(driver);
			headerPage.selectMyProfileOption();

			OverviewPage myProfilePopUp = new OverviewPage(driver);
			
			verifyElementTextContains(myProfilePopUp.myProfile_header, "My Profile", test);
			
			MyProfileSideMenu myprofileSidemenu = new MyProfileSideMenu(driver);
			myprofileSidemenu.navigateToManagerAssessmentPage();

			ManagerAssessmentPage managerAssessmentPage = new ManagerAssessmentPage(driver);
			assertTrue(isElementExisting(driver, managerAssessmentPage.edit_button),
					"Navigated to manager assessment scren", test);

			managerAssessmentPage.editManagerInfo("Test123","abc@mercer.com");

			managerAssessmentPage.requestAssesment();
			assertTrue(isElementExisting(driver, managerAssessmentPage.requestSent_button),
					"Request is sent to manager's email", test);

			myprofileSidemenu.closePopup();
			// email verification can not automate thus not clicking on the "Request
			// Assessment" button to trigger an email to manager.

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 5)
	public void verifyLocateMe() throws Exception {
		try {
			test = reports.createTest("Verifying Locate me functionality");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);

			HomePage homePage = new HomePage(driver);
			homePage.navigateToLocateMe(driver);

			LocateMePage locateMePage = new LocateMePage(driver);
			assertTrue(isElementExisting(driver, locateMePage.ownRole_hightlighted),
					"the role of user is highlighted", test);

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 6)
	public void navigateFromHeader() throws Exception {
		try {
			test = reports.createTest("Verifying navigate to next functionality");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);

			LocateMePage locateMePage = new LocateMePage(driver);
			assertTrue(isElementExisting(driver, locateMePage.ownRole_hightlighted),
					"User is on locate me page", test);

			HeaderPage headerPage = new HeaderPage(driver);
			headerPage.navigateViaHeader(headerPage.navigate_button);

			NavigatePopUpPage navigatePopUp = new NavigatePopUpPage(driver);
			assertTrue(isElementExisting(driver, navigatePopUp.navigatePopUp_modal),
					"Navigate pop up is visible", test);
			navigatePopUp.closePopup();
			assertTrue(isElementExisting(driver, locateMePage.ownRole_hightlighted),
					"User is on locate me page", test);

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 7)
	public void verifyCompareFunctionality() throws Exception {
		try {
			test = reports.createTest("Verifying compare functionality");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);

			LocateMePage locateMePage = new LocateMePage(driver);
			assertTrue(isElementExisting(driver, locateMePage.ownRole_hightlighted),
					"User is on locate me page", test);

			HeaderPage headerPage = new HeaderPage(driver);
			headerPage.navigateViaHeader(headerPage.compare_button);

			ComparePopUpPage comparePopup = new ComparePopUpPage(driver);
			waitForElementToDisplay(comparePopup.startcompare_button);
			assertTrue(isElementExisting(driver, comparePopup.startcompare_button),
					"compare pop up is visible", test);
			comparePopup.performCompare(driver);
			assertTrue(isElementExisting(driver, comparePopup.roleCompare_modal),
					"Roles are compared successfully", test);
			comparePopup.closeModal();

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 8)
	public void navigateToHome() throws Exception {
		try {
			test = reports.createTest("Verifying if application returns home");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);

			LocateMePage locateMePage = new LocateMePage(driver);
			assertTrue(isElementExisting(driver, locateMePage.ownRole_hightlighted),
					"User is on locate me page", test);

			HeaderPage headerPage = new HeaderPage(driver);
			headerPage.navigateViaHeader(headerPage.home_button);

			HomePage homePage = new HomePage(driver);
			assertTrue(isElementExisting(driver, homePage.locateMe_menu),
					"User navigated to home page successfully", test);

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 9)
	public void testForRequestAssessment1() throws IOException {
		try {
			test = reports.createTest("Logging out from carrer view");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);

			HeaderPage headerPage = new HeaderPage(driver);
			headerPage.logout();

			LoginPage loginPage = new LoginPage(driver);
			assertTrue(isElementExisting(driver, loginPage.username_input),
					"User logged out successfully", test);
		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 10)
	public void verifyAnalyticsTabInAdmin() throws Exception {
		try {
			test = reports.createTest("Verifying Analytics tab in admin page");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);

			AdminPage adminPage = new AdminPage(driver);
			adminPage.navigateToAdmin(USERNAME, PASSWORD);
			
			waitForElementToDisplay(adminPage.adminPage_header);
			verifyElementTextContains(adminPage.adminPage_header, "Company Administration", test);
			
			AdminHeaderPage adminHeaderPage = new AdminHeaderPage(driver);
			adminHeaderPage.navigateViaHeader(adminHeaderPage.analytics_button);

			AdminAnalyticsPage analyticsPage = new AdminAnalyticsPage(driver);
			assertTrue(isElementExisting(driver, analyticsPage.rangeSelection_dropdown),
					"User is navigated to analytics page", test);

			analyticsPage.selectFromRangeDropdown();
			

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 11)
	public void verifyingImportTab() throws Exception {
		try {
			test = reports.createTest("Verifying Import tab in admin page");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);

			AdminHeaderPage adminHeaderPage = new AdminHeaderPage(driver);
			adminHeaderPage.navigateViaHeader(adminHeaderPage.import_button);

			AdminImportPage importPage = new AdminImportPage(driver);
			verifyElementTextContains(importPage.importPage_header, "Upload the career view data for the company", test);

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = false, priority = 12)
	public void verifyingAdminTaskTab() throws Exception {
		try {
			test = reports.createTest("Verifying Admin Task tab in admin page");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);

			AdminHeaderPage adminHeaderPage = new AdminHeaderPage(driver);
			adminHeaderPage.navigateViaHeader(adminHeaderPage.adminTask_button);

			AdminTaskPage adminTaskPage = new AdminTaskPage(driver);
			waitForElementToDisplay(adminTaskPage.adminTask_header);
			
			verifyElementTextContains( adminTaskPage.adminTask_header, "Reset manager assessment request for an employee", test);

			assertTrue(adminTaskPage.performResetAction(USERNAME),
					"Assessment request cleared", test);
		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@Test(enabled = true, priority = 13)
	public void logoutfromAdminSite() throws Exception {
		try {
			test = reports.createTest("Logging out from career view admin site");
			test.assignCategory("smoke");
			
			
			driver = driverFact.getEventDriver(webdriver, test);

			AdminPage adminPage = new AdminPage(driver);
			adminPage.performLogout();

			LoginPage loginPage = new LoginPage(driver);
			assertTrue(isElementExisting(driver, loginPage.username_input),
					"User logged out successfully from admin career view application", test);

		} catch (Error e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{
			reports.flush();

		}
	}

	@AfterSuite
	public void killDriver() {
		reports.flush();
		driver.close();
		killBrowserExe(BROWSER_TYPE);

	}
}
