package pages.ics;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CompensationTablePage {
	
	@FindBy(id = "subscriptionSelect")
	public static WebElement currentsubscription;
	

	@FindBy(xpath = "(//input[@type='radio'])[2]")
	public static WebElement oneTimeOrder;
	
	@FindBy(xpath = "//*[@id='hostLocation']")
	public static WebElement hostLocation;
	
	@FindBy(xpath = "//label[contains(text(),'Host Location')]")
	public static WebElement hostLocationLabel;
	

	@FindBy(xpath = "//label[contains(text(),'Report Type')]")
	public static WebElement reportTypeLabel;
	
	@FindBy(xpath="//*[@id='reportType']")
	public static WebElement reportType;
	
	@FindBy(xpath = "(//input[@type='button'])[1]")
	public static WebElement confirmButton;
	
	@FindBy(xpath = "//button[contains(text(),'Purchase')]")
	public static WebElement purchasebtn;
	
	@FindBy(xpath = "//a[contains(text(),'View Table')]")
	public static WebElement viewtable;
	
	@FindBy(xpath = "//button[text()='Go to Current Tables']")
	public static WebElement gotocurrentbtn;
	
	@FindBy(id = "u_nav_logout")
	public static WebElement logoutButton;
	
	@FindBy(xpath = "//h2[text()='Login']")
	public static WebElement loginText;

	public HeaderPage headerpage;
	
	public CompensationTablePage(WebDriver driver) {
		headerpage=new HeaderPage(driver);
		PageFactory.initElements(driver, this);
	}
	
	public void reportType() throws InterruptedException {
		clickElement(reportType);
		
		}
		
		public void selectConfirm() throws InterruptedException {
			
			clickElement(confirmButton);
			
			
		}
		public void selectRadioBtn() throws InterruptedException {
			clickElement(oneTimeOrder);
			
		}
		
		public void purchase() throws InterruptedException{
			
			clickElement(purchasebtn);
		}
		public void selectGotoCurrentBtn() throws InterruptedException {
			clickElement(gotocurrentbtn);
			
		}
			
		public void logout() throws InterruptedException {
			clickElement(logoutButton);
		}
	
}
