package pages.ics;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;
import java.io.File;

public class CurrentTablesShowPage {
	public static WebElement lastupdated_pdf;

	@FindBy(xpath = "(//div[@class='sprite sprite-info'])[1]")
	public static WebElement infoicon;

	@FindBy(xpath = "//img[@title='PDF']")
	public static WebElement pdf;

	@FindBy(id = "excelButton")
	public static WebElement export_button;

	public CurrentTablesShowPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public  void getpdf(WebDriver driver) throws InterruptedException {
		clickElement(infoicon);
		clickElement(pdf);

	}

	public static void deleteFilesInPath(String downloadPath) {
		File dir = new File(downloadPath);
		File[] dirContents = dir.listFiles();

		for (int i = 0; i < dirContents.length; i++) {

			dirContents[i].delete();

		}

	}
	
	public static boolean isFileDownloaded(String Path, String fileName) {
		  File dir = new File(Path);
		  File[] dirContents = dir.listFiles();

		  for (int i = 0; i < dirContents.length; i++) {
			  
		      if (dirContents[i].getName().contains(fileName)) {
		          // File has been found, it can now be deleted:
		    	  System.out.println("File found "+dirContents[i].getName());
		    	System.out.println("Downloaded File name--- >> "+dirContents[i].getName());
		    	
		          return true;
		      }
		          }
		      return false;
		  }	
	
	public static void exportfile() throws InterruptedException {
		clickElement(export_button);
	}
	
}
	
	


