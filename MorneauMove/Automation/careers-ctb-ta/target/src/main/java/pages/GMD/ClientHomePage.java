package pages.GMD;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class ClientHomePage {
	
	ExtentTest test =null;
	
	
	@FindBy(xpath = "//h1[@class='mul-hdr-rule']")
	public WebElement clientTitle;
	
	@FindBy(xpath = "//a[text()='Demographics']")
	public WebElement demographics;
	
	@FindBy(xpath = "//a[@title='Visibility Date Client Configuration']//i")
	public WebElement vdccIcon;
	
	@FindBy(xpath = "//a[@title='Features and Access Rights Administration']//i")
	public WebElement featuresARAIcon;
	
	@FindBy(xpath = "//a[@title='Proxy Groups']//i")
	public WebElement proxyGrpIcon;
	
	@FindBy(xpath = "//a[@title='Add a New Group']//i")
	public WebElement addGrpIcon;
	
	@FindBy(xpath = "//a/i[@title='Add Client Specific Group']")
	public WebElement add_CSG;
	
	@FindBy(xpath = "//h6[text()='Confirmation']")
	public WebElement confirm_Msg;
	
	@FindBy(xpath = "//a[@class='js-run-test-btn']//i")
	public WebElement runTstIcon;
	
	@FindBy(xpath = "//input[@id='filter']")
	public WebElement filterGrp;
	
	@FindBy(xpath = "//a[@title='Remove Rule']//i")
	public WebElement removeGrp;
	
	@FindBy(xpath = "//button[@title='Remove']")
	public WebElement removeCfm;
	
	@FindBy(xpath = "//a[@title='Promote Groups']//i")
	public WebElement promoteIcon;
	
	public ClientHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void demoGroups() throws InterruptedException {
		
		clickElement(demographics);
			}
	
	public void vdcc_Click() throws InterruptedException {
		
		clickElement(vdccIcon);
			}
	
	public void fara_Click() throws InterruptedException {
		
		clickElement(featuresARAIcon);
	}
	
	public void proxyGroup_Click() throws InterruptedException {
		
		clickElement(proxyGrpIcon);
	}
	
	public void addGroup_Click() throws InterruptedException {
		
		clickElement(addGrpIcon);
		clickElement(add_CSG);
		
		}
	
	public void runTest_Click() throws InterruptedException {
		
		clickElement(runTstIcon);
		
		}
	
	public void promoteGrp_Click() throws InterruptedException {
		
		clickElement(promoteIcon);
		
		}
	
	public void removeGrp_Click(String Group) throws InterruptedException {
		
		setInput(filterGrp, Group);
		clickElement(removeGrp);
		clickElement(removeCfm);
		
		}
}