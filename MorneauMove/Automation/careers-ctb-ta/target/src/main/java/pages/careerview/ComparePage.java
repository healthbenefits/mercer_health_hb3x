package pages.careerview;

import static driverfactory.Driver.*;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ComparePage {

	WebDriver driver;
	LogisticsPage logistics;
	ITPage ItPage;
	LocateMePage locateMe;

	@FindBy(xpath = "//p[contains(@class,'begin')]")
	public  WebElement comparePageMsg;

	@FindBy(xpath= "//div[@id='modal-compare-roles']/a")
	public WebElement close_modal;

	@FindBy(xpath= "//div[@id='compare_isolation']/dl/dt[2]")
	public WebElement selectedRoleMsg;

	@FindBy(xpath= "//a[contains(@class,'remove-role')]/span[contains(text(),'Programmer Analyst')]")
	public WebElement selectedRole;
	
	public String selRole_xpath = "//a[contains(@class,'remove-role')]/span[contains(text(),'Programmer Analyst')]";

	@FindBy(xpath= "//div[@id='compare_isolation']/a[1]")
	public WebElement Xmark;
	
	public String Xmark_xpath = "//div[@id='compare_isolation']/a[1]";

	@FindBy(xpath= "//a[@class='active blue-button modal-button-compare right']")
	public WebElement compareBtn;
	
	public String compareBtn_xpath = "//a[@class='active blue-button modal-button-compare right']";

	/*@FindBy(xpath= "//li[@id='nav-role_6_AD0007'][contains(@class,'tbox c-roles-3 professional compare-node')]")
	public WebElement thirdElementToCompare;*/

	public String thirdElementToCompare = "//li[@id='nav-role_6_AD0007'][contains(@class,'tbox c-roles-3 professional compare-node')]";

	public ComparePage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
		logistics = new LogisticsPage(driver);
		ItPage = new ITPage(driver);
		locateMe = new LocateMePage(driver);

	}

	public void close_modal() throws InterruptedException {
		waitForElementToDisplay(close_modal);
		clickElement(close_modal);
	}

	public void selectSingleRoleToCompare() throws InterruptedException {
		delay(1000);
		waitForElementToEnable(ItPage.progAnalyst);
		clickElement(ItPage.progAnalyst);
		
	}
	
	public String getRoleText(WebElement element) {
		
		String text = element.getText();
		return text;
	}

public String getRoleText(WebElement selElement,WebElement selElement2 , WebElement defSelected) {
		
		String text = selElement.getText();
		String text2 = selElement2.getText();
		return (" "+text+" "+text2+" "+defSelected.getText());
	}

public String getRoleText(String text,WebElement selElement2 , String defSelected) {
	
	String text2 = selElement2.getText();
	return (" "+text+" "+text2+" "+defSelected);
}

	public void compareWithinSameFamily() throws InterruptedException {
		waitForElementToEnable(ItPage.progAnalyst);
		clickElement(ItPage.progAnalyst);
		clickElementUsingJavaScript(driver,ItPage.AppDev);
		clickElementUsingJavaScript(driver,ItPage.AppDev);
		waitForElementToEnable(ItPage.SwAppEnggIII);
		clickElement(ItPage.SwAppEnggIII);
		
	}

	public void startCompare() throws InterruptedException {
		waitForElementToDisplay(compareBtn);
		clickElement(compareBtn);
	}

	public void clickXmark() throws InterruptedException {
		waitForElementToDisplay(Xmark);
		clickElement(Xmark);
	}

	public void compareWithinDifferentFamily() throws InterruptedException {

		locateMe.clickUpArrow();
		clickElement(locateMe.ArrowDropdown_Logistics);
		clickElement(logistics.AnalystMW);

		logistics.scrollToElement();
		clickElement(logistics.CordsMW);
	}
	
	public boolean checkPresence(List<WebElement> element) {
		return(element.size() >0);
		
	}
	
	public void clickRole(WebElement element) throws InterruptedException {
		clickElement(element);
	}
}
