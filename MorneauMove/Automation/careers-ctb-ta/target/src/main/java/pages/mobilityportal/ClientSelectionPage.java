package pages.mobilityportal;

import static driverfactory.Driver.clickElement;
//import static driverfactory.Driver.getEleByXpathTxt;
import static driverfactory.Driver.getEleByXpathContains;

import static driverfactory.Driver.refreshpage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClientSelectionPage {
WebDriver driver;
	

public static  WebElement customer;

public static  WebElement account;

@FindBy(xpath = "//h3[contains(text(),'Select a customer:')]")
public static WebElement selectcustomer;


@FindBy(xpath="//button[contains(text(),'Continue')]")
public static  WebElement continueBtn;
 
	public ClientSelectionPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectCustomer(String customerTxt) throws InterruptedException {
		refreshpage();
		customer=getEleByXpathContains("a",customerTxt,driver);
		clickElement(customer);
		
	}
	

	public void selectAccount(String accountTXt) throws InterruptedException {
		account=getEleByXpathContains("a",accountTXt,driver);
		clickElement(account);
		
	}
	public void clickOnContinue() throws InterruptedException
	{
		clickElement(continueBtn);
		
	}
	
}

