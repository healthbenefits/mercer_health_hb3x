package pages.HB43x;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class CoveragePage {
	
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//span[contains(text(),'My Benefits')]")
	public WebElement verify_CCpage;
	
	@FindBy(id = "ctl00_MainBody_ProcessLifeWizard___CustomNav2_StepNextButton3")
	WebElement next3;
	
	public CoveragePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void currentCoverageNext() throws InterruptedException {
		
		clickElement(next3);
		
		
	}
}
