package pages.mobilityportal;

import static driverfactory.Driver.waitForElementToDisplay;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

public class LoginPage {


		@FindBy(id = "ctl00_MainSectionContent_Email")
		public WebElement email;

		@FindBy(xpath = "//span[contains(text(),'Logout')]")
		public WebElement logoutLnk;

		@FindBy(id = "ctl00_MainSectionContent_Password")
		public WebElement password;

		// a[@class = 'btn action my-account-lnk dropdown-toggle']
		@FindBy(xpath = "//span[text()='My Account ']")
		public WebElement myAccounts;

		@FindBy(xpath = "//span[text()='Login']")
		public WebElement login;

		@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
		public WebElement signIn;

		@FindBy(css = "h1.title-my-tools")
		public WebElement myToolsTxt;
		WebDriver driver;
		public LoginPage(WebDriver driver) {
			this.driver=driver;
			PageFactory.initElements(driver, this);
		}

		public void login(String username, String passwordTxt) throws Exception {
			/*
			 * try catch logic to handle IE issue
			 * redirection to Home page instead of login page when previous scripts fails before logout
			 */
			try
			{
				clickElement(myAccounts);
				waitForElementToDisplay(logoutLnk,driver,5);
				clickElement(logoutLnk);
				clickElement(myAccounts);
				clickElement(login);
				waitForElementToDisplay(email);
				email.sendKeys(username);
				waitForElementToDisplay(password);
				password.sendKeys(passwordTxt);
				clickElement(signIn);
			}
			catch(Exception e)
			{
				clickElement(login);
				waitForElementToDisplay(email);
				email.sendKeys(username);
				waitForElementToDisplay(password);
				password.sendKeys(passwordTxt);
				clickElement(signIn);
			}
			

		}
	}

