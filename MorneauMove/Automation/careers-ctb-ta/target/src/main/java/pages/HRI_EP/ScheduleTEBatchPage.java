package pages.HRI_EP;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class ScheduleTEBatchPage {
	
	ExtentTest test =null;
	WebDriver driver;
	
	Con_BEPage a = new Con_BEPage(driver);
	
	@FindBy(xpath = "//h2[text()='Batch Events - Schedule Time Elapsed Batches']")
	public WebElement steb_Title;
	
	@FindBy(xpath = "//input[@id='data-derivation-rules-filter']")
	WebElement filter_TB;
	
	@FindBy(xpath = "//i[@class='mul-image-icon mul-image-icon-size-small mul-icon-hide-text run-batch-icon']")
	WebElement runNow_TEB;
	
	@FindBy(xpath = "//button[@title='OK']")
	WebElement run_OK;
	
	@FindBy(xpath = "//span[text()='Running']")
	WebElement running_Verify;
	
	public ScheduleTEBatchPage(WebDriver driver) {
		PageFactory.initElements(driver, this);

}
	
	public void runSTEB() throws InterruptedException {
		
	waitForElementToDisplay(filter_TB);
	System.out.println(a.s);
	filter_TB.sendKeys(a.s);
	clickElement(runNow_TEB);
	waitForElementToDisplay(run_OK);
	clickElement(run_OK);
	
	}
}