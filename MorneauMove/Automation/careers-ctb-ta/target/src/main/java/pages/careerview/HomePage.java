package pages.careerview;


import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

	WebDriver driver;
	@FindBy(xpath = "//span[@class = 'indicator']/parent::a[@href = '/organization/explore/6_IT']")
	public  WebElement locateMe_menu;
	
	@FindBy(xpath = "//li[@id='nav-home_6_IT']/a/span[2]//span[contains(@class,'pulse')]")
	public  WebElement blinkPulse;
	
	@FindBy(xpath = "//a[text()='My Profile'][@class = 'blue-button modal-button-profile']")
	public  WebElement myProfile_button;
	
	@FindBy(xpath = "//a[text()='Start Exploring'][@class = 'blue-button modal-button-explore']")
	public  WebElement startExploring_button;
	
	@FindBy(xpath = "//li[@id='nav-home_6_Mark']/a")
	public  WebElement marketing;
	
	@FindBy(xpath = "//li[@id='nav-home_6_RandD']/a")
	public  WebElement RnD;
	
	@FindBy(xpath = "//li[@id='nav-home_6_Log']/a")
	public  WebElement Logistics;
	
	@FindBy(xpath = "//li[@id='nav-home_6_IT']/a")
	public  WebElement IT;
	
	@FindBy(xpath = "//a[text()='Explore Wide'][@class = 'blue-button modal-button-wide']")
	public  WebElement exploreWide;
	
	@FindBy(xpath = "//a[text()='Explore Narrow'][@class = 'blue-button modal-button-narrow']")
	public  WebElement exploreNarrow;
	
	
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	public void clickExploreNarrow() throws InterruptedException {
		clickElement(exploreNarrow);
	}
	
	public void navigateToLocateMe(WebDriver driver) throws InterruptedException {
		
		clickElement(locateMe_menu);
		HeaderPage headerPage = new HeaderPage(driver);
		clickElement(headerPage.locateMe_button);
	}
	
	public boolean checkPresence() {
		return(driver.findElements(By.xpath("//li[@id='nav-home_6_IT']/a/span[2]//span[contains(@class,'pulse')]")).size() >0);
	}
	
	public void clickExploreWide() throws InterruptedException {
		waitForElementToDisplay(exploreWide);
		clickElement(exploreWide);
	}
	
	public void clickStartExploring() throws InterruptedException {
		clickElement(startExploring_button);
	}
	public void clickMarketing() throws InterruptedException {
		waitForElementToDisplay(marketing);
		clickElement(marketing);	
	}
	
	public void clickIT() throws InterruptedException {
		waitForElementToEnable(IT);
		clickElement(IT);
	}

	public void goTomyProfile() throws InterruptedException {
		waitForElementToDisplay(myProfile_button);
		clickElement(myProfile_button);
	}
}
