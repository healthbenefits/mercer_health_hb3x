package pages.careerview;

import static driverfactory.Driver.*;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogisticsPage {

	WebDriver driver;
	@FindBy(id = "nav-subfunc_6_Log_MW")
	public WebElement macro_Materials_Warehousing;
	
	@FindBy(id = "nav-subfunc_6_Log_TD")
	public WebElement macro_Transportation_Distribution;
	
	@FindBy(id = "nav-subfunc_6_Log_Plan")
	public WebElement macroLog_Planning;
	
	@FindBy(id = "nav-subfunc_6_Log_Oper")
	public WebElement macroLog_Oper;
	
	@FindBy(id = "nav-subfuncM_6_Log_MW")
	public WebElement mini_Materials_Warehousing;
	
	@FindBy(id = "nav-subfuncM_6_Log_TD")
	public WebElement mini_Transportation_Distribution;
	
	@FindBy(id = "nav-subfuncM_6_Log_Plan")
	public WebElement miniLog_plan;
	
	@FindBy(id = "nav-subfuncM_6_Log_Oper")
	public WebElement miniLog_Oper;
	
	@FindBy(xpath = "//li[@class='tbox c-roles-1 professional']/a[@id='modal-6_MW0009']")
	public  WebElement CordsMW;

	@FindBy(xpath = "//li[@class='tbox c-roles-1 professional nav-node']/a[@id='modal-6_MW0009']/span")
	public  WebElement CordsMWText;
	
	@FindBy(xpath = "//li[@class='tbox c-roles-1 professional nav-node']/a[@id='modal-6_MW0009']/span[2]")
	public  WebElement CordsMWNumber;
	
	@FindBy(xpath = "//li[@class='tbox c-roles-1 professional']/a[@id='modal-6_MW0006']")
	public  WebElement AnalystMW;
	
	@FindBy(xpath = "//a[@id='modal-6_MW0006']/span[contains(text(),'Analyst Materials / Warehousing')]")
	public  WebElement AnalystMWText;

	
	public String thirdElementToCompare =  "//li[@class='tbox c-roles-1 professional compare-node']/a[@id='modal-6_MW0009']";
	
	public LogisticsPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	public void scrollToElement() {

		Actions action = new Actions(driver);
		action.moveToElement(CordsMW).clickAndHold().moveByOffset(0,-1000).release().build().perform();
	}
	
	public void selectRoles() throws InterruptedException {
		HeaderPage header = new HeaderPage(driver);
		header.zoomIn();
		scrollToElement();
		clickElement(CordsMW);
	}
	
	public boolean checkPresence(List<WebElement> element) {
		return(element.size() >0);
		
	}
	
}
