package pages.HRI_EP;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class Con_HRIPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h2[text()='HR Import - File Configuration List']")
	 public WebElement hri_Title;
	
	@FindBy(xpath = "//input[@title='Type to filter results.']")
	public WebElement flFilter;
	
	@FindBy(xpath = "//*[@id=\"js-file-config-list-table\"]/tbody/tr/td[1]/a")
	 public WebElement fl_Config;
	
		
	public Con_HRIPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void searchFLConfig(String File) throws InterruptedException {
		
		flFilter.click();
		setInput(flFilter, File);
		clickElement(fl_Config);
		
	}
	
	}