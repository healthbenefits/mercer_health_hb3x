package pages.ics;

import static driverfactory.Driver.*;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HeaderPage {
	
	@FindBy(xpath = "//a[@title='Compensation Tables']")
	public static WebElement compensationTableLink;
	
	@FindBy(xpath = "//a[text()='Add Table']")
	public static WebElement addtableLink;
	
	@FindBy(xpath = "(//a[text()='Home'])[1]")
	public static WebElement homelink;
	
	
	@FindBy(xpath = "//a[@href=\"/icsweb/MyAccountShow.do\"]")
	public  WebElement welcomeimatest;
	
	@FindBy(xpath = "//div[contains(text(),'Knowledge Center')]")
	public  WebElement knowledgeCenter;
	
	@FindBy(xpath = "//div[contains(text(),'Knowledge Center')]/../..//div[@class='body-pad']")
	public  WebElement knowledgeCenterpdf_List;
	
	@FindBy(xpath="//a[contains(text(),'Balance Sheet Booklet')]")
	public  List<WebElement> productpdf_List;

	WebDriver driver;
	public HeaderPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	 public void clickHomeLink(WebElement headerLink) throws InterruptedException {
		 clickElement(headerLink);
	 }

	
	public void navigateToCompLnk(String lnkTxt) throws InterruptedException {
		waitForElementToDisplay(compensationTableLink);
		hoverOverElement(compensationTableLink, driver);
		clickElement(getEleByXpathContains("a",lnkTxt,driver));
	 }

	public void navigateToHome() throws InterruptedException {
		// TODO Auto-generated method stub
		clickElement(homelink);
	}
	
}
