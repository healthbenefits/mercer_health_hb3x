package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TechnicalCompetencyPage {
	WebDriver driver;
	@FindBy(xpath = "//h1[contains(text(),'Technical Competencies')]")
	public  WebElement TechnicalCompetencyHeader;
	
	@FindBy(xpath = "//*[@id='user-competency_user_technical-competencies_0']/label[1]/input")
	public  WebElement technicalCompetencyRadioButton1;
	
	@FindBy(xpath = "//div[@id='user-competency_user_technical-competencies_0']/label[2]/input")
	public  WebElement technicalCompetencyRadioButton2;
	
	@FindBy(xpath = "//*[@id='panel_2']/div/button[1]")
	public  WebElement saveBtn;
	
	public TechnicalCompetencyPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickLevel1RadioButton() throws InterruptedException {
		waitForElementToEnable(technicalCompetencyRadioButton1);
		clickElement(technicalCompetencyRadioButton1);
	}
	
	public void clickLevel2RadioButton() throws InterruptedException {
		waitForElementToEnable(technicalCompetencyRadioButton2);
		clickElement(technicalCompetencyRadioButton2);
	}
	
	public void clickSave() throws InterruptedException {
		waitForElementToEnable(saveBtn);
		clickElement(saveBtn);
	}
}
