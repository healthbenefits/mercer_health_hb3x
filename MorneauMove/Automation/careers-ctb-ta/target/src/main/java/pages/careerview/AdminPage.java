package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminPage {
	
WebDriver driver;
	@FindBy(xpath = "//h4[contains(text(), 'Company Administration')]")
	public  WebElement adminPage_header;
	
	@FindBy(xpath = "//a[contains(text(), 'Sign out')]")
	public  WebElement logout_button;
	
	public  String adminUrl = "https://careerview-stage.mercer.com/Demo2016/Admin";
	
	public AdminPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		}
	
	

	public void navigateToAdmin(String username, String password) throws InterruptedException {
		
		driver.get(adminUrl);
		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginToCareerView(username, password);
		
	}



	public void performLogout() throws InterruptedException {
		// TODO Auto-generated method stub
		waitForElementToDisplay(logout_button);
		clickElement(logout_button);
	}


}
