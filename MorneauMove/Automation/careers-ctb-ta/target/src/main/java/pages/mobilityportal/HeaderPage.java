package pages.mobilityportal;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.getEleByXpathContains;
import static driverfactory.Driver.switchToWindow;

public class HeaderPage {
WebDriver driver;
@FindBy(css= "a[class*='my-account-lnk']")
public WebElement myAccounts;

@FindBy(xpath = "//span[contains(text(),'Logout')]")
public WebElement logoutLnk;
	
	public HeaderPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	/*
	 * menuLinkTxt can be MyDashBoard/BalanceSheetCost/PlatformAdministrationTool/CompensationLocalizer
	 */
	public void navigateToMyAccMenu(String menuLinkTxt) throws InterruptedException {
		clickElement(myAccounts);
		clickElement(getEleByXpathContains("a",menuLinkTxt,driver));
	}
	public void logout() throws InterruptedException
	{
		clickElement(logoutLnk);	
		}
	
	
}
