package pages.HRI_EP;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.UUID;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class Con_BEPage {
	
	
	ExtentTest test =null;
	
		
	@FindBy(xpath = "//h2['@class=mul-hdr-rule groups-hdr-icons mul-pop-out-menu-action mul-hdr-icons']")
	public WebElement be_Title;
		
	@FindBy(xpath = "//a[text()='(Click here to open the MMC Information Classification Policy)']")
	WebElement ic_Banner;
	
	@FindBy(xpath = "//a[text()='(Click here to open the Mercer Data Handling Guidelines)']")
	WebElement dh_Banner;
			
	@FindBy(xpath = "//i[@class='mul-image-icon mul-image-icon-size-medium mul-image-icon-position-right add-group-icon']")
	public WebElement teb_NewIcon;
	
	@FindBy(xpath = "//a[@title='Promote Time Elapsed Batches']")
	public WebElement promote_Icon;
	
	@FindBy(xpath = "//a[text()='Life Event Rule']")
	public WebElement teb_Submit;
	
	@FindBy(xpath = "//textarea[@id='eval-point-expression-textarea']")
	public WebElement sql_TB;
	
	@FindBy(xpath = "//input[@id='batch-description']")
	public WebElement bd_TB;
	
	@FindBy(xpath = "//select[@id='life-event']")
	public WebElement le_Drop;
	//3 - Birth/Adoption
	
	@FindBy(xpath = "//select[@id='event-state']")
	public WebElement letState_Drop;
	//
	
	@FindBy(xpath = "//textarea[@class='ace_text-input']")
	public WebElement le_Date;
	
	@FindBy(xpath = "//button[@title='Validate Syntax']")
	public WebElement validateSyn_Button;
	
	
	@FindBy(xpath = "//h6[@class='mul-message-hdr']")
	public WebElement rule_Confirmation;
	
	
	@FindBy(xpath = "//a[text()='Save']")
	public WebElement save_Button;
	
	@FindBy(xpath = "//p[text()='You have successfully added the LIFE EVENT rule ']")
	public WebElement teb_Confirmation;
	
	@FindBy(xpath = "//a[text()='Home']")
	public WebElement be_Home;
	
	
	
	
	public Con_BEPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void data_ICPBanner( ) throws InterruptedException {
		
		clickElement(ic_Banner);
		
		
	}
	public void data_DHGBanner( ) throws InterruptedException {
		
		clickElement(dh_Banner);
		
		
	}
	public static String s = "";
	public String STEB( ) throws InterruptedException {
		
		clickElement(teb_NewIcon);
		clickElement(teb_Submit);
		setInput(sql_TB, "select ssn from comp_ssn where ssn='600021598'");
		String uuid = UUID.randomUUID().toString();
		s= uuid;
		System.out.println(uuid);
		setInput(bd_TB, uuid);
		selEleByVisbleText(le_Drop, "3 - Birth/Adoption");
		selEleByVisbleText(letState_Drop, "Pending");
		Thread.sleep(3000);
		setInput(le_Date, "#01/05/2019#");
		//le_Date.sendKeys("#01/01/2019#");
		clickElement(validateSyn_Button);
		
		return s;
		
	}
	
	public void saveSTEB( ) throws InterruptedException {
		
		clickElement(save_Button);
		
	}
	
	public void home_Click( ) throws InterruptedException {
		
		clickElement(be_Home);
		
	}
	
public void promote_Batch( ) throws InterruptedException {
		
		clickElement(promote_Icon);
		
	}
}