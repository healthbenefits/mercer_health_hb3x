package pages.CompensationLocalizer_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;

//import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import static ui.smoke.testcases.CompensationLocalizer.CompensationLocalizer_smoke.MFAChoice;
import static ui.smoke.testcases.CompensationLocalizer.CompensationLocalizer_smoke.MFAEmailId;
import static ui.smoke.testcases.CompensationLocalizer.CompensationLocalizer_smoke.MFAEmailPassword;

import java.util.ArrayList;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.getPageSource;
//import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.switchToWindow;

public class MFA {
	static String Code;
	@FindBy(id = "ctl00_MainSectionContent_ButtonSubmit")
	public static WebElement SendCodeButton;
	@FindBy(id = "ctl00_MainSectionContent_ChallengeCode")
	public static WebElement SubmitCode;
	@FindBy(id = "username")
	public static WebElement OutlookEmailID;
	@FindBy(id = "password")
	public static WebElement OutlookPassword;
	@FindBy(xpath = "//span[text()='MFA']")
	public static WebElement MFAtab;
	@FindBy(xpath="//div[@autoid='_lv_i']//following::div[1]")
	public static WebElement Message;
	@FindBy(xpath = "//div[@id='Item.MessageUniqueBody']//div[@class='PlainText']")
	public static WebElement MFAmessage;
	@FindBy(xpath = "//span[text()='Mark as read']")
	public static WebElement MarkAsRead;

	public MFA(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public static void authenticate(WebDriver driver) throws InterruptedException {
		if(driver.getPageSource().contains("Verify your identity")){
		clickElement(driver.findElement(By.id("ctl00_MainSectionContent_Credentials_"+MFAChoice)));
		Driver.clickElementUsingJavaScript(driver,SendCodeButton);
		FetchMFACodeFromOutlookEmail(driver);
		setInput(SubmitCode,Code+Keys.ENTER);
//		delay(6000);
//		waitForElementToEnable(SubmitCode);
	}}
	public static void FetchMFACodeFromOutlookEmail(WebDriver driver) throws InterruptedException {
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get("https://apac1mail.mmc.com/OWA/");
		waitForElementToDisplay(OutlookEmailID);
		setInput(OutlookEmailID,MFAEmailId);
		setInput(OutlookPassword,MFAEmailPassword+Keys.ENTER);
		clickElement(MFAtab);
		clickElement(Message);
		Code = MFAmessage.getText();
		Code = Code.substring(27, 33);
		clickElement(MarkAsRead);
		switchToWindow("Welcome - Mercer",driver);
		}
}

