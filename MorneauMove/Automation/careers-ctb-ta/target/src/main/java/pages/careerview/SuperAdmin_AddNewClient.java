package pages.careerview;

import static driverfactory.Driver.*;

import static utilities.InitTests.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;

import static utilities.SikuliActions.*;

public class SuperAdmin_AddNewClient {

	WebDriver driver;
	String appName = "careerView";
	String photoPath = "C:\\Users\\vidya-dandinashivara\\uiframework\\src\\main\\resources\\picturesToBeUploaded\\Tulips.jpg";
	
	@FindBy(xpath = "//h4[contains(text(),'Configure new company into CareerView')]")
	public WebElement addNewClient_Header;
	
	@FindBy(xpath = "//input[@name='name']")
	public  WebElement name_input;
	
	/*@FindBy(xpath = "//form[@id='form']/div[1]/div/label")
	public  WebElement name_inputError;*/
	public String name_inputErrorXpath = "//form[@id='form']/div[1]/div/label";
	
	@FindBy(xpath = "//input[@name='urlFragment']")
	public  WebElement urlFrag_input;
	
	@FindBy(xpath = "//a[@href='http://careerview-stage.mercer.com/Demo2016']")
	public  WebElement urlFrag;
	
	@FindBy(xpath = "//label[contains(text(),'Special character not allowed')]")
	public  WebElement urlFrag_inputError;
	
	@FindBy(xpath = "//input[@name='adminName']")
	public  WebElement adminName_input;
	
	/*@FindBy(xpath = "//form[@id='form']/div[3]//label")
	public WebElement adminName_inputError;*/
	public String adminName_inputErrorXpath = "//form[@id='form']/div[3]//label";
	
	@FindBy(xpath = "//input[@name='adminEmail']")
	public  WebElement adminEmail_input;
	
	@FindBy(xpath = "//label[contains(text(),'Please enter a valid email address.')]")
	public  WebElement adminEmail_inputError;
	
	@FindBy(xpath = "//button[contains(text(),'Save')]")
	public  WebElement save_Button;
	
	@FindBy(xpath = "//button[contains(text(),'Cancel')]")
	public  WebElement cancel_Button;
	
	@FindBy(xpath = "//label[@for='languageCheck'][contains(text(),'This field is required.')]")
	public  WebElement checkbox_error;
	
	@FindBy(xpath = "//input[@placeholder='Please provide valid IP Address comma seperated list']")
	public  WebElement ipAddr_input;
	
	@FindAll(@FindBy(how = How.XPATH, using = "//div[@class='display-only']"))    
	public List<WebElement> readOnly_fields;
	
	@FindBy(xpath = "//button[text()='Reset']")
	public  WebElement reset_button;
	
	@FindBy(xpath = "//button[text()='Revert to Default Logo']")
	public  WebElement revertToDefaultLogo_button;
	
	@FindBy(xpath = "//span[text()='(Please provide images with resolution of 139X19 size)']")
	public  WebElement Logo_Text;
	
	@FindBy(xpath = "//span[contains(@class,'logo')]")
	public  WebElement Logo_img;
	
	@FindBy(xpath = "//div[@id='forms']//span[@class='logo']")
	public  WebElement loginLogo_img;
	
	@FindBy(xpath = "//a[text()='Exit Company']")
	public  WebElement exitCompany_button;
	
	
	@FindBy(xpath = "//input[@name='logoImage']")
	public  WebElement chooseFile_button;
	
	
	public SuperAdmin_AddNewClient(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void enterAllFields(String name, String urlFrag , String adminName, String email) throws InterruptedException {
		name_input.clear();
		setInput(name_input, name);
		urlFrag_input.clear();
		setInput(urlFrag_input, urlFrag);
		adminName_input.clear();
		setInput(adminName_input, adminName );
		adminEmail_input.clear();
		setInput(adminEmail_input, email);
		clickElement(save_Button);
	}
	
	public void clickCancel() throws InterruptedException {
		clickElement(cancel_Button);
	}
	
	public void reset() throws InterruptedException {
		clickElement(reset_button);
	}
	
	public void clickSave() throws InterruptedException {
		waitForElementToDisplay(save_Button);
		clickElementUsingJavaScript(driver,save_Button);
	}
	
	public String getAlertText() {
		Alert alert = null;
		String alertText;
		// Get a handle to the open alert, prompt or confirmation
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.alertIsPresent());
			alert = driver.switchTo().alert();
		}
		catch(Exception e){
			
		}
		finally {
			// Get the text of the alert or prompt
			alertText = alert.getText();
			System.out.println(alertText);
			
			// And acknowledge the alert (equivalent to clicking "OK")
			alert.accept();
		}
		return alertText;

	}
	
	public void exitCompany() throws InterruptedException {
		clickElement(exitCompany_button);
	}
	
	public void uploadPhoto() throws Exception {
		waitForElementToDisplay(chooseFile_button);
		
		System.out.println("Path is :" + photoPath);
		chooseFile_button.sendKeys(photoPath);
	}
	
	public void revertTodefaultImage() throws InterruptedException {
		waitForElementToDisplay(revertToDefaultLogo_button);
		clickElement(revertToDefaultLogo_button);
		delay(1000);
	}
}
