package smoke.pat.tests;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.pat.AppStatsPage;
import pages.pat.ContentManagementPage;
import pages.pat.LicenceAgrMgmtPage;
import pages.pat.PlatformAdminPage;
import pages.pat.ReleaseManagerPage;
import pages.pat.RoleManagementPage;
import pages.pat.SpendIncPage;
import pages.pat.UserManagemntPage;
import pages.pat.UserTypeMgmtPage;
import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.LoginPage;
import pages.mobilityportal.MytoolsPage;
import utilities.InitTests;



public class TC01_verifyLogin extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public TC01_verifyLogin(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		TC01_verifyLogin Tc01 = new TC01_verifyLogin("PAT");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	@Test(priority = 1, enabled = true)
	public void verifyLogin() throws Exception {
		try {
			test = reports.createTest("PAT--verify Login To PAT--" + BROWSER_TYPE);
			test.assignCategory("smoke");
			
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
		
			LoginPage mobilityLogin = new LoginPage(driver);
			mobilityLogin.login(USERNAME, PASSWORD);
			ClientSelectionPage clientSelectionPage=new ClientSelectionPage(driver);
			clientSelectionPage.selectCustomer("MERCER");
			clientSelectionPage.selectAccount("97216906 GHRM Clients");
			clientSelectionPage.clickOnContinue();
			MytoolsPage myTools=new MytoolsPage(driver);
			myTools.header.navigateToMyAccMenu("Platform Administration");
			String parentWindow=driver.getWindowHandle();
			waitForNoOfwindowsTobe(2);
			switchToWindowByTitle("Administration",driver);
			
			PlatformAdminPage patAdminPage=new PlatformAdminPage(driver);
			waitForElementToDisplay(patAdminPage.appheaderTxt);
			verifyElementTextContains(patAdminPage.appheaderTxt, "Platform Administration", test);
			patAdminPage.navigateToMenuLnk("UserManagementOption");
			UserManagemntPage userMgmt = new UserManagemntPage(driver);
			verifyElementTextContains(userMgmt.CompLocalizer, "Compensation Localizer",test);
			verifyElementTextContains(userMgmt.allOtherApps, "All Other Applications",test);
			patAdminPage.navigateToMenuLnk("UserTypeManagement");
			UserTypeMgmtPage userTypeMgmt = new UserTypeMgmtPage(driver);
			verifyElementTextContains(userTypeMgmt.userTypeHeader, "User Type Management",test);
			patAdminPage.navigateToMenuLnk("RoleManagement");
			RoleManagementPage role = new RoleManagementPage(driver);
			verifyElementTextContains(role.roleMgmtHeader, "Role Management",test);
	
			patAdminPage.navigateToMenuLnk("ReleaseDataManagement");

			ReleaseManagerPage relManager = new ReleaseManagerPage(driver);
			verifyElementTextContains(relManager.RelManagerHeader, "Release Manager" ,test);
			verifyElementTextContains(relManager.TaxDataMgmt, "Tax Data Management",test);
			verifyElementTextContains(relManager.TaxPDFMgmt, "Tax PDF Management",test);
			verifyElementTextContains(relManager.TaxDataBulkMgmt, "Tax Data - Bulk Management",test);
			patAdminPage.navigateToMenuLnk("ContentManagement");

			ContentManagementPage contMgmt = new ContentManagementPage(driver);
			verifyElementTextContains(contMgmt.ContMgmtHeader, "Content Management",test);
			patAdminPage.navigateToMenuLnk("ApplicationStatistics");

			AppStatsPage appStats = new AppStatsPage(driver);
			verifyElementTextContains(appStats.AppStatsHeader, " Application Statistics ",test);
			verifyElementTextContains(appStats.CLCReport, " Compensation Localizer Calculation Report ",test);
			patAdminPage.navigateToMenuLnk("LicenseAgreement");

			LicenceAgrMgmtPage licAgrMgmt = new LicenceAgrMgmtPage(driver);
			verifyElementTextContains(licAgrMgmt.licAgrHeader, "License Agreement Management ",test);
			verifyElementTextContains(licAgrMgmt.compReset, " Compensation Localizer Reset ",test);
			patAdminPage.navigateToMenuLnk("ReleaseSIDataManagement");
			SpendIncPage spendInc = new SpendIncPage(driver);
			verifyElementTextContains(spendInc.SpendIncomeHeader, " Spendable Income - Data Management ",test);
			
			spendInc.header.signOut();
			
			driver.close();
			switchToWindow(parentWindow,driver);
			myTools.header.logout();
		
		} 
		catch (Error e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
