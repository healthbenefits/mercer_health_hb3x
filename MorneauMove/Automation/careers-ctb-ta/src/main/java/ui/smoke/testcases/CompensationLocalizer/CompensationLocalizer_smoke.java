
package ui.smoke.testcases.CompensationLocalizer;



import static driverfactory.Driver.delay;
//import static driverfactory.Driver.driver;
//import static driverfactory.Driver.isElementExisting;
//import static driverfactory.Driver.driver;
//import static driverfactory.Driver.getScreenPath;
//import static driverfactory.Driver.initWebDriver;
//import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.killBrowserExe;
import static pages.CompensationLocalizer_pages.Input.AdjustedNetIncomeInHostValue;
import static pages.CompensationLocalizer_pages.Input.AdjustedNetIncomeValue;
import static pages.CompensationLocalizer_pages.Input.Allowance1;
import static pages.CompensationLocalizer_pages.Input.Allowance2;
import static pages.CompensationLocalizer_pages.Input.COLAValue;
import static pages.CompensationLocalizer_pages.Input.Deduction1;
import static pages.CompensationLocalizer_pages.Input.Deduction2;
import static pages.CompensationLocalizer_pages.Input.HardshipPremiumValue;
import static pages.CompensationLocalizer_pages.Input.HomeAdjustedNetValue;
import static pages.CompensationLocalizer_pages.Input.HomeNetIncomeValue;
import static pages.CompensationLocalizer_pages.Input.HomeTotalCompensationValue;
import static pages.CompensationLocalizer_pages.Input.HostAdjustedNetValue;
import static pages.CompensationLocalizer_pages.Input.HostNetIncomeValue;
import static pages.CompensationLocalizer_pages.Input.HostTotalCompensationValue;
import static pages.CompensationLocalizer_pages.Input.NetIncomeValue;
import static pages.CompensationLocalizer_pages.Input.ProposedHostAdjustedNetValue;
import static pages.CompensationLocalizer_pages.Input.ProposedHostNetIncomeValue;
import static pages.CompensationLocalizer_pages.Input.ProposedHostTotalCompensationValue;
import static driverfactory.Driver.refreshpage;
import static utilities.MyExtentReports.reports;
//import static utilities.MyExtentReports.test;
//import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;
import java.io.File;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.CompensationLocalizer_pages.Input;
import pages.CompensationLocalizer_pages.Login;
import pages.CompensationLocalizer_pages.Logout;
import pages.CompensationLocalizer_pages.Preview;
import pages.CompensationLocalizer_pages.Results;
import pages.CompensationLocalizer_pages.Start;
import utilities.FileDownloader;
import utilities.InitTests;
import verify.SoftAssertions;
import static driverfactory.Driver.waitForPageLoad;


public class CompensationLocalizer_smoke extends InitTests {
	public static String MFAChoice = "";
	public static String MFAEmailId = "";
	public static String MFAEmailPassword = "";
	Login loginobj;
	Start startobj;
	Input inputobj;
	Preview previewobj;
	Results resultsobj;
	Logout logoutobj;
	ExtentTest test =null;
	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;

public CompensationLocalizer_smoke(String appname) {
	super(appname);
}
@BeforeClass
public void setUp() throws Exception {
	props.load(input);
	MFAChoice = props.getProperty("MFAChoice");
	MFAEmailId = props.getProperty("MFAEmailId");
	MFAEmailPassword = props.getProperty("MFAEmailPassword");
	//@SuppressWarnings("unused")
	test = reports.createTest("Logging in to Compensationlocalizer");
	test.assignCategory("smoke");
	CompensationLocalizer_smoke obj = new CompensationLocalizer_smoke("CompensationLocalizer");
	Driver driverObj=new Driver();
	webdriver = driverObj.initWebDriver(BASEURL, "PHANTOMJS", "local", "");
	driver=driverObj.getEventDriver(webdriver,test);
	
	//driver=driverObj.getEventDriver(webdriver,test);
	loginobj = new Login(driver);
	startobj = new Start(driver);
	inputobj = new Input(driver);
	previewobj = new Preview(driver);
	resultsobj = new Results(driver);
	logoutobj = new Logout(driver);
	
}
@Test(priority = 1, enabled = true)
public void GHRM() throws Exception {
	try {
		
		loginobj.login(USERNAME, PASSWORD,driver);
		//refreshpage();
		assertTrue(driverFact.isElementExisting(driver,loginobj.AccountSelectionHeader),"Login Successful",test);
		refreshpage();
		loginobj.accountSelection("MERCER","97216906 GHRM Clients", driver);
		verifyElementTextContains(loginobj.LoggedInAccount,"97216906 GHRM Clients",test);
		//refreshpage();
		startobj.openCompensationLocalizer(driver);
		waitForPageLoad(driver);
		//refreshpage();
		verifyElementTextContains(startobj.ApplicationName,"Compensation Localizer",test);
		verifyElementTextContains(startobj.LoggedInAccount,"97216906",test);
		verifyElementTextContains(startobj.ActiveTab,"Start",test);
		startobj.startCalculation("Germany, Berlin", "Switzerland, Zurich");
		verifyElementTextContains(startobj.ActiveTab,"Calculation Inputs",test);
		inputobj.setBaseSalary("100000");
		inputobj.setAllowances("5000", "5%");
		inputobj.setDeductions("15000", "10%");
		inputobj.setMaritalStatus("Married with 3 children", "Married with no children");
		inputobj.setExchangeRate("Specific Month");
		inputobj.setTargetSalary("Yes","40000");
		inputobj.setMercerStandardAssumptions("No");
		inputobj.setAdditionalTaxableIncome("10000");
		inputobj.submit();
		delay(30000);
		verifyElementTextContains(startobj.ActiveTab,"Preview Calculations",test);
		verifyEquals(Integer.parseInt(previewobj.Allowance1Preview.getText()),Allowance1,test);
		verifyEquals(Integer.parseInt(previewobj.Allowance2Preview.getText()),Allowance2,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction1Preview.getText()),Deduction1,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction2Preview.getText()),Deduction2,test);
		assertTrue(previewobj.checkFamilyAllowance("GreaterThan",0),"Family Allowance greater than 0",test);
		verifyEquals(previewobj.computeNetIncome(),NetIncomeValue,test);
		verifyEquals(previewobj.computeCostOfLivingAdjustments(),COLAValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncome(),AdjustedNetIncomeValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncomeInHost(),AdjustedNetIncomeInHostValue,test);
//		previewobj.addEducationAllowance();
//		verifyEquals(Integer.parseInt(previewobj.EducationCostAdded.getAttribute("value")),EducationCostValue,test);
		previewobj.addHardshipPremium();
		verifyElementTextContains(previewobj.HardshipPercentageOfGross,"0",test);
		verifyEquals(previewobj.computeHardshipPremium("10"),HardshipPremiumValue,test);
		previewobj.submit();
		delay(10000);
		verifyElementTextContains(startobj.ActiveTab,"View Results",test);
		verifyEquals(resultsobj.computeHomeCompensation(),HomeTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHomeNetIncome(),HomeNetIncomeValue,test);
		verifyEquals(resultsobj.computeHomeAdjustedNetIncome(),HomeAdjustedNetValue,test);
		verifyEquals(resultsobj.computeCalculatedHostCompensation(),HostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHostNetIncome(),HostNetIncomeValue,test);
		verifyEquals(resultsobj.computeHostAdjustedNetIncome(),HostAdjustedNetValue,test);
		assertTrue(resultsobj.computeAdjustedNetIncomeInHostDifference("LessThan",10),"Adjusted Net Income difference falls within the allowed range",test);
		verifyEquals(resultsobj.computeProposedHostCompensation(),ProposedHostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeProposedHostNetIncome(),ProposedHostNetIncomeValue,test);
		verifyEquals(resultsobj.computeProposedHostAdjustedNetIncome(),ProposedHostAdjustedNetValue,test);
		assertTrue(resultsobj.computeCompensationDifference("LessThan",0),"Compensation difference is within the range",test);
//		resultsobj.excelExport();
		FileDownloader downloadTestFile = new FileDownloader(driver);
        String downloadedFileAbsoluteLocation = downloadTestFile.downloadFile(resultsobj.ExcelOption);
        
        System.out.println(downloadedFileAbsoluteLocation);
        assertTrue(new File(downloadedFileAbsoluteLocation).exists(),"File downloaded successfully",test);
        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
	
		
//		resultsobj.pdfExport();
		FileDownloader downloadTestFilepdf = new FileDownloader(driver);
        String downloadedFileAbsoluteLocationpdf = downloadTestFilepdf.downloadFile(resultsobj.PDFOption);
        
        System.out.println(downloadedFileAbsoluteLocationpdf);
        assertTrue(new File(downloadedFileAbsoluteLocationpdf).exists(),"File downloaded successfully",test);
        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
        
        
		resultsobj.runAnotherCalculation();
		delay(10000);
		verifyElementTextContains(startobj.ActiveTab,"Start",test);
		startobj.startCalculation("Australia, Adelaide", "Vietnam, Hanoi");
		verifyElementTextContains(startobj.ActiveTab,"Calculation Inputs",test);
		inputobj.initialise();
		inputobj.setBaseSalary("80000");
		inputobj.setMaritalStatus("Married with 3 children", "Single with no children");
		inputobj.setExchangeRate("Specific Month");
		inputobj.setTargetSalary("No","");
		inputobj.setMercerStandardAssumptions("No");
		inputobj.setHousingData("Expatriate Level Rental Housing", "Less Expensive Area", "Expatriate Level Rental Housing", "Less Expensive Area");
		inputobj.setAdditionalTaxableIncome("10000");
		inputobj.submit();
		delay(30000);
		verifyElementTextContains(startobj.ActiveTab,"Preview Calculations",test);
		assertTrue(previewobj.checkFamilyAllowance("GreaterThan",0),"Family Allowance greater than 0",test);
		verifyEquals(previewobj.computeNetIncome(),NetIncomeValue,test);
		verifyEquals(previewobj.computeCostOfLivingAdjustments(),COLAValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncome(),AdjustedNetIncomeValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncomeInHost(),AdjustedNetIncomeInHostValue,test);
//		previewobj.addEducationAllowance();
//		verifyEquals(Integer.parseInt(previewobj.EducationCostAdded.getAttribute("value")),EducationCostValue,test);
		previewobj.addHardshipPremium();
		verifyElementTextContains(previewobj.HardshipPercentageOfGross,"22.5",test);
		previewobj.submit();
		delay(10000);
		verifyElementTextContains(startobj.ActiveTab,"View Results",test);
		verifyEquals(resultsobj.computeHomeCompensation(),HomeTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHomeNetIncome(),HomeNetIncomeValue,test);
		verifyEquals(resultsobj.computeHomeAdjustedNetIncome(),HomeAdjustedNetValue,test);
		verifyEquals(resultsobj.computeCalculatedHostCompensation(),HostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHostNetIncome(),HostNetIncomeValue,test);
		verifyEquals(resultsobj.computeHostAdjustedNetIncome(),HostAdjustedNetValue,test);
		assertTrue(resultsobj.computeAdjustedNetIncomeInHostDifference("LessThan",100),"Adjusted Net Income difference falls within the allowed range",test);
		verifyEquals(resultsobj.computeProposedHostCompensation(),ProposedHostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeProposedHostNetIncome(),ProposedHostNetIncomeValue,test);
		verifyEquals(resultsobj.computeProposedHostAdjustedNetIncome(),ProposedHostAdjustedNetValue,test);
		assertTrue(resultsobj.computeCompensationDifference("LessThan",0),"Compensation difference is within the range",test);
		resultsobj.excelExport();
		FileDownloader downloadTestFile1 = new FileDownloader(driver);
        String downloadedFileAbsoluteLocation1 = downloadTestFile1.downloadFile(resultsobj.ExcelOption);
        
        System.out.println(downloadedFileAbsoluteLocation1);
        assertTrue(new File(downloadedFileAbsoluteLocation1).exists(),"File downloaded successfully",test);
        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
        
		resultsobj.pdfExport();
		FileDownloader downloadTestFilepdf1 = new FileDownloader(driver);
        String downloadedFileAbsoluteLocationpdf1 = downloadTestFilepdf1.downloadFile(resultsobj.PDFOption);
        
        System.out.println(downloadedFileAbsoluteLocationpdf1);
        assertTrue(new File(downloadedFileAbsoluteLocationpdf1).exists(),"File downloaded successfully",test);
        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);

		logoutobj.logout(driver);
		
		
		
	} catch (Error e) {
		e.printStackTrace();

		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	}

finally {
		reports.flush();
		
	}

	}
@Test(priority=2, enabled = true)
public void ICS() throws Exception {
	try {
		loginobj.login(USERNAME, PASSWORD,driver);
		assertTrue(driverFact.isElementExisting(driver,loginobj.AccountSelectionHeader),"Login Successful",test);
		//refreshpage();
		loginobj.accountSelection("Mercer (New York)","11325 Multi-National Pay with Americans Abroad - Catherine Bruning",driver);
			verifyElementTextContains(loginobj.LoggedInAccount,"11325 Multi-National Pay with Americans Abroad - Catherine Bruning",test);
			delay(5000);
			startobj.openCompensationLocalizer(driver);
		verifyElementTextContains(startobj.ApplicationName,"Compensation Localizer",test);
		verifyElementTextContains(startobj.LoggedInAccount,"11325",test);
		verifyElementTextContains(startobj.ActiveTab,"Start",test);
		startobj.startCalculation("United Kingdom, Urban United Kingdom", "Germany, Berlin");
		verifyElementTextContains(startobj.ActiveTab,"Calculation Inputs",test);
		inputobj.setBaseSalary("100000");
		inputobj.setAllowances("5000", "5%");
		inputobj.setDeductions("15000", "10%");
		inputobj.setMaritalStatus("Married with 3 children", "Married with no children");
		inputobj.setExchangeRate("Specific Week");
		inputobj.setTargetSalary("Yes","100000");
		inputobj.setMercerStandardAssumptions("No");
		inputobj.setAdditionalTaxableIncome("10000");
		inputobj.submit();
		delay(30000);
		verifyElementTextContains(startobj.ActiveTab,"Preview Calculations",test);
		verifyEquals(Integer.parseInt(previewobj.Allowance1Preview.getText()),Allowance1,test);
		verifyEquals(Integer.parseInt(previewobj.Allowance2Preview.getText()),Allowance2,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction1Preview.getText()),Deduction1,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction2Preview.getText()),Deduction2,test);
		assertTrue(previewobj.checkFamilyAllowance("Equals",0),"Family Allowance greater than 0",test);
		verifyEquals(previewobj.computeNetIncome(),NetIncomeValue,test);
		verifyEquals(previewobj.computeCostOfLivingAdjustments(),COLAValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncome(),AdjustedNetIncomeValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncomeInHost(),AdjustedNetIncomeInHostValue,test);
//		previewobj.addEducationAllowance();
//		verifyEquals(Integer.parseInt(previewobj.EducationCostAdded.getAttribute("value")),EducationCostValue,test);
		previewobj.addHardshipPremium();
		verifyElementTextContains(previewobj.HardshipPercentageOfGross,"",test);
		verifyEquals(previewobj.computeHardshipPremium("10"),HardshipPremiumValue,test);
		previewobj.submit();
		delay(10000);
		verifyElementTextContains(startobj.ActiveTab,"View Results",test);
		verifyEquals(resultsobj.computeHomeCompensation(),HomeTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHomeNetIncome(),HomeNetIncomeValue,test);
		verifyEquals(resultsobj.computeHomeAdjustedNetIncome(),HomeAdjustedNetValue,test);
		verifyEquals(resultsobj.computeCalculatedHostCompensation(),HostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHostNetIncome(),HostNetIncomeValue,test);
		verifyEquals(resultsobj.computeHostAdjustedNetIncome(),HostAdjustedNetValue,test);
		assertTrue(resultsobj.computeAdjustedNetIncomeInHostDifference("Equals",0),"Adjusted Net Income difference falls within the allowed range",test);
		verifyEquals(resultsobj.computeProposedHostCompensation(),ProposedHostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeProposedHostNetIncome(),ProposedHostNetIncomeValue,test);
		verifyEquals(resultsobj.computeProposedHostAdjustedNetIncome(),ProposedHostAdjustedNetValue,test);
		assertTrue(resultsobj.computeCompensationDifference("GreaterThan",0),"Compensation difference is within the range",test);
//		resultsobj.excelExport();
		
		FileDownloader downloadTestFile = new FileDownloader(driver);
        String downloadedFileAbsoluteLocation = downloadTestFile.downloadFile(resultsobj.ExcelOption);
        
        System.out.println(downloadedFileAbsoluteLocation);
        assertTrue(new File(downloadedFileAbsoluteLocation).exists(),"File downloaded successfully",test);
        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
        
       
//		resultsobj.pdfExport();
		FileDownloader downloadTestFilepdf = new FileDownloader(driver);
        String downloadedFileAbsoluteLocationpdf = downloadTestFilepdf.downloadFile(resultsobj.PDFOption);
        
        System.out.println(downloadedFileAbsoluteLocationpdf);
        assertTrue(new File(downloadedFileAbsoluteLocationpdf).exists(),"File downloaded successfully",test);
        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
        
        
		resultsobj.runAnotherCalculation();
		delay(10000);
		verifyElementTextContains(startobj.ActiveTab,"Start",test);
		startobj.startCalculation("Australia, Adelaide", "Egypt, Cairo");
		verifyElementTextContains(startobj.ActiveTab,"Calculation Inputs",test);
		inputobj.initialise();
		inputobj.setBaseSalary("80000");
		inputobj.setMaritalStatus("Single with 3 children", "Single with no children");
		inputobj.setExchangeRate("Specific Month");
		inputobj.setTargetSalary("No","");
		inputobj.setMercerStandardAssumptions("No");
		inputobj.setHousingData("Expatriate Level Rental Housing", "Exclusive Area","Expatriate Level Rental Housing",  "Exclusive Area");
		inputobj.setAdditionalTaxableIncome("10000");
		inputobj.submit();
		delay(30000);
		verifyElementTextContains(startobj.ActiveTab,"Preview Calculations",test);
		assertTrue(previewobj.checkFamilyAllowance("GreaterThan",0),"Family Allowance equals 0",test);
		verifyEquals(previewobj.computeNetIncome(),NetIncomeValue,test);
		verifyEquals(previewobj.computeCostOfLivingAdjustments(),COLAValue,test);
		assertTrue(previewobj.checkIfCOLAIsNegative(),"COLA Value is negetive",test);
		verifyEquals(previewobj.computeAdjustedNetIncome(),AdjustedNetIncomeValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncomeInHost(),AdjustedNetIncomeInHostValue,test);
//		previewobj.addEducationAllowance();
//		verifyEquals(Integer.parseInt(previewobj.EducationCostAdded.getAttribute("value")),EducationCostValue,test);
		previewobj.addHardshipPremium();
		verifyElementTextContains(previewobj.HardshipPercentageOfGross,"25",test);
		previewobj.setCustomAllowance("1000");
		previewobj.submit();
		delay(10000);
		verifyElementTextContains(startobj.ActiveTab,"View Results",test);
		verifyEquals(resultsobj.computeHomeCompensation(),HomeTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHomeNetIncome(),HomeNetIncomeValue,test);
		verifyEquals(resultsobj.computeHomeAdjustedNetIncome(),HomeAdjustedNetValue,test);
		verifyEquals(resultsobj.computeCalculatedHostCompensation(),HostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHostNetIncome(),HostNetIncomeValue,test);
		verifyEquals(resultsobj.computeHostAdjustedNetIncome(),HostAdjustedNetValue,test);
		assertTrue(resultsobj.computeAdjustedNetIncomeInHostDifference("Equals",0),"Adjusted Net Income difference falls within the allowed range",test);
		verifyEquals(resultsobj.computeProposedHostCompensation(),ProposedHostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeProposedHostNetIncome(),ProposedHostNetIncomeValue,test);
		verifyEquals(resultsobj.computeProposedHostAdjustedNetIncome(),ProposedHostAdjustedNetValue,test);
		assertTrue(resultsobj.computeCompensationDifference("LessThan",0),"Compensation difference is within the range",test);
//		resultsobj.excelExport();
		FileDownloader downloadTestFile1 = new FileDownloader(driver);
        String downloadedFileAbsoluteLocation1 = downloadTestFile1.downloadFile(resultsobj.ExcelOption);
        
        System.out.println(downloadedFileAbsoluteLocation1);
        assertTrue(new File(downloadedFileAbsoluteLocation1).exists(),"File downloaded successfully",test);
        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
//		resultsobj.pdfExport();
		FileDownloader downloadTestFilepdf1 = new FileDownloader(driver);
        String downloadedFileAbsoluteLocationpdf1 = downloadTestFilepdf1.downloadFile(resultsobj.PDFOption);
        
        System.out.println(downloadedFileAbsoluteLocationpdf1);
        assertTrue(new File(downloadedFileAbsoluteLocationpdf1).exists(),"File downloaded successfully",test);
        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
		logoutobj.logout(driver);
		
	} catch (Error e) {
		e.printStackTrace();

		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	}

 finally {
		reports.flush();
		driver.quit();
	}

	}

@AfterSuite
public void tearDown() {
	killBrowserExe("CHROME");
}

}