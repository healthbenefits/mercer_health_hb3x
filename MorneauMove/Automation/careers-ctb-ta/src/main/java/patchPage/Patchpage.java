package patchPage;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class Patchpage {
	
	ExtentTest test =null;
	
	
	@FindBy(xpath = "//span[contains(.,'new item')]")
	public WebElement newItem;
		
	@FindBy(xpath = "//select[@title='Patch Name Required Field']")
	WebElement patchname;
	
	@FindBy(xpath = "//select[@title='Application Required Field']")
	WebElement appname;
	
	@FindBy(xpath = "//input[@title='Event Date Required Field']")
	WebElement datepatch;
		
	@FindBy(xpath = "//img[@id='Event_x0020_Start_x0020_Date_x00_3eca6dc0-b676-4368-87f4-bad56fe207a6_$DateTimeFieldDateDatePickerImage']")
	WebElement cal_icon;
	
	
	@FindBy(xpath = "//input[@title='Time (EST) Required Field']")
	WebElement patch_time;
	
	@FindBy(xpath = "//input[@title='Tester Required Field, Enter a name or email address...']")
	WebElement tester;
	
	@FindBy(xpath = "//a[@class='ms-core-menu-link']//div")
	WebElement tester_click;
	
	@FindBy(xpath = "//select[@title='Testing Type Required Field']")
	WebElement testing_type;
	
	@FindBy(xpath = "//span[@title='Sandbox']//input")
	WebElement sb_region;
	@FindBy(xpath = "//span[@title='QAI']//input")
	WebElement qai_region;
	@FindBy(xpath = "//span[@title='Performance *']//input")
	WebElement perf_region;
	@FindBy(xpath = "//span[@title='CI']//input")
	WebElement ci_region;
	@FindBy(xpath = "//span[@title='CSO']//input")
	WebElement cso_region;
	@FindBy(xpath = "//span[@title='Prod']//input")
	WebElement prod_region;
	@FindBy(xpath = "//span[@title='Test']//input")
	WebElement test_region;
	@FindBy(xpath = "//span[@title='QA']//input")
	WebElement qa_region;
	@FindBy(xpath = "//span[@title='Staging']//input")
	WebElement stag_region;
	@FindBy(xpath = "//span[@title='FULLVOL']//input")
	WebElement fv_region;
	@FindBy(xpath = "//span[@title='VDI']//input")
	WebElement vdi_region;
	@FindBy(xpath = "//span[@title='Desktop']//input")
	WebElement dktp_region;
	
	@FindBy(xpath = "//select[@title='Verification Type Required Field']")
	WebElement verf_type;
	
	@FindBy(xpath = "//input[@title='Time (Effort)']")
	WebElement tym_eff;
	
	@FindBy(xpath = "//select[@title='Testing Status Required Field']")
	WebElement test_status;
	
	@FindBy(xpath = "//div[@title='Coordinator Required Field']//input[@type='text']")
	WebElement coordinator;
	
	@FindBy(xpath = "//div[@class='ms-core-menu-label']")
	WebElement coord_click;
	
	@FindBy(xpath = "//input[@title='Comments']")
	WebElement comments;
	
	@FindBy(xpath = "(//input[@value='Save'])[2]")
	WebElement save;
	
	
	public Patchpage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void patchtrack(WebDriver driver,String m,String Patch_name, String Application, String Event_date,String Time_EST,String Tester,String Test_type,String Impact, String Verf_type,String Efforts, String Test_status, String Coordinator,String Comments) throws InterruptedException {
		
		waitForElementToDisplay(newItem);
		clickElement(newItem);
		int test = driver.findElements(By.xpath("//iframe")).size();
        System.out.println(test);
        Thread.sleep(1000);
		WebElement fra=driver.findElement(By.xpath("//iframe[@class='ms-dlgFrame']"));
        driver.switchTo().frame(fra);
		selEleByVisbleText(patchname, Patch_name);
		selEleByVisbleText(appname, Application);
		clickElement(cal_icon);	
		WebElement frame_date = driver.findElement(By.xpath("//iframe[@title='Select a date from the calendar.']"));
		Thread.sleep(2000);
		driver.switchTo().frame(frame_date);
		String date=String.valueOf(Event_date.replace(".0", ""));
		for(int i =1;i<=31;i++)
		{
		
        if(i<10)
		{
        	   String month=String.valueOf(m.replace(".0", ""));
               String date1=driver.findElement(By.xpath("//td//a[@id='20190"+month+"0"+i+"']")).getText();
               WebElement date01= driver.findElement(By.xpath("//td//a[@id='20190"+month+"0"+i+"']"));
               if(date1.equals(date)) {
            	   date01.click();
            	   break;
               }
              
        }
       
        else 
        {
        	String month=String.valueOf(m.replace(".0", ""));
        	String date2=driver.findElement(By.xpath("//td//a[@id='20190"+month+i+"']")).getText();
        	 WebElement date02= driver.findElement(By.xpath("//td//a[@id='20190"+month+i+"']"));
             if(date2.equals(date)) {
          	   date02.click();
          	   break;
             }
            
         }
        }
		
		
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(fra);
		setInput(patch_time, Time_EST);
		setInput(tester, Tester);
		clickElement(tester_click);
		selEleByVisbleText(testing_type, Test_type);
		if(Impact.contentEquals("Non-Prod")) {
			clickElement(qa_region);
			clickElement(ci_region);
			clickElement(cso_region);
			clickElement(test_region);
		}
		if(Impact.contentEquals("Prod")){
			
			clickElement(prod_region);
			
		}
		if(Impact.contentEquals("Desktop")) {
			
			clickElement(dktp_region);
		}
		selEleByVisbleText(verf_type, Verf_type);
		String Effort=String.valueOf(Efforts.replace(".0", ""));
		setInput(tym_eff, Effort);
		selEleByVisbleText(test_status, Test_status);
		setInput(coordinator, Coordinator);
		clickElement(coord_click);
		setInput(comments, Comments);
		//clickElement(save);
		
		
		
	}
}