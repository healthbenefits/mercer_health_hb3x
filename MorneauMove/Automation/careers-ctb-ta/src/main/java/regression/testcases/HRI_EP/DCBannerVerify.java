package regression.testcases.HRI_EP;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HRI_EP.ClientHomePage;
import pages.HRI_EP.Client_Page;
import pages.HRI_EP.Con_BEPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class DCBannerVerify extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  DCBannerVerify(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		DCBannerVerify Tc01 = new DCBannerVerify("HRI");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	@Test(priority = 1, enabled = true)
	public void dcBanner() throws Exception {
		try {
			test = reports.createTest("HRI/EP--verify Login To HRI/EP--" + BROWSER_TYPE);
			test.assignCategory("regression");
			
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			Client_Page clientSelect = new Client_Page(driver); 
			clientSelect.clientClick(Client);
			
			ClientHomePage clientHome= new ClientHomePage(driver);
			waitForElementToDisplay(clientHome.client_HomePage_Verify);
			SoftAssertions.verifyElementTextContains(clientHome.client_HomePage_Verify,Client,test);
			clientHome.con_BE_Click();
			
			Con_BEPage dcVerify= new Con_BEPage(driver);
			waitForElementToDisplay(dcVerify.be_Title);
			SoftAssertions.verifyElementTextContains(dcVerify.be_Title,"Batch Events - Time Elapsed Batches",test);
			dcVerify.data_ICPBanner();
			Thread.sleep(2000);
			switchToWindowWithURL("https://colleagueconnect.mmc.com/en-us/Documents/Services/Procedures%20and%20Policies/Information%20Classification%20Policy.pdf", driver);
			Thread.sleep(3000);
			File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

			try {
				FileUtils.copyFile(src, new File("./src/main/resources/testdata/screenshot/abc.jpg"));
			} catch (IOException e1) {
				
				e1.printStackTrace();
			}
			driver.close();
			switchToWindowWithURL("http://usfkl13ws17v/EventProcessing/#teeEventsList/QAACME", driver);
			waitForElementToDisplay(dcVerify.be_Title);
			dcVerify.data_DHGBanner();
			Thread.sleep(2000);
			switchToWindowWithURL("https://colleagueconnect.mmc.com/en-us/Documents/Services/Procedures%20and%20Policies/Information%20Classification%20Policy.pdf", driver);
			Thread.sleep(3000);
			File src2= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

			try {
				FileUtils.copyFile(src2, new File("./src/main/resources/testdata/screenshot/abcd.jpg"));
			} catch (IOException e2) {
				
				e2.printStackTrace();
			}
			driver.close();		
			
		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
		
