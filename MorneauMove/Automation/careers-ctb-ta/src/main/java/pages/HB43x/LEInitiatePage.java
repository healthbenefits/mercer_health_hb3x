package pages.HB43x;

import static driverfactory.Driver.setInput;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeNoException;
import static verify.SoftAssertions.verifyElementTextContains;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.Executor;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.gargoylesoftware.htmlunit.javascript.host.dom.Document;

import driverfactory.Driver;
import verify.SoftAssertions;

import static driverfactory.Driver.*;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;

public class LEInitiatePage {

	ExtentTest test =null;
	
	@FindBy(id = "ctl00_MainBody_PageTitle")
	public WebElement verify_LEI_Page;
	
	@FindBy(id = "ctl00_MainBody_SelectLifeEvent1_HBGridView1_ctl04_ChooseLifeRadio")
	WebElement birth_Adopt;
	
	@FindBy(xpath = "//button[@class='ui-datepicker-trigger']")
	WebElement calendar_Icon;
	
	@FindBy(xpath = "//div[@id='ui-datepicker-div']")
	WebElement date_Picker;
	
	@FindBy(id = "ctl00_MainBody_SelectLifeContinueBtn")
	public WebElement le_Continue;
	
	
	
	public LEInitiatePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	
	public void leInitiate() throws InterruptedException {
		
		clickElement(birth_Adopt);
		DateFormat dateFormat2 = new SimpleDateFormat("dd"); 
	    Date date2 = new Date();
	    String today = dateFormat2.format(date2);
	    System.out.println(today);
	    if(today.charAt(0)==(0)) {
	    	
	    	today= today.replaceFirst("0", "");
	    	System.out.println(today);
	    	
	    }
		
		clickElement(calendar_Icon);
		List<WebElement> columns = date_Picker.findElements(By.tagName("td"));
		for (WebElement cell:columns) {

	            if (cell.getText().equals(today)) {
	                cell.click();
	                break;
	            }
	        }
		
		clickElement(le_Continue);
	}
}
