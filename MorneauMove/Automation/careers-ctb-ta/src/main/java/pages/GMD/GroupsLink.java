package pages.GMD;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class GroupsLink {
	
	ExtentTest test =null;
	

	@FindBy(xpath = "//div[@class='groups-hdr-icons-actions group-actions']//i")
	public WebElement groupsAll;
	
	
	public GroupsLink(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void groupPage() throws InterruptedException {
		
		clickElement(groupsAll);
		
	}
	
}