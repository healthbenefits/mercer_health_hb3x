package pages.pat;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class RoleManagementPage {

	//RoleMgmt
	
	
	@FindBy(id="divRoleManagement")
	public WebElement RoleMgmt;

	@FindBy(xpath="//h4[contains(text(),'Role Management')]")
	public WebElement roleMgmtHeader;
	
	@FindBy(id = "btnDelete")
	public WebElement deleteBtn;
	
	WebDriver driver;
	
	public RoleManagementPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);;
	}

	
}
