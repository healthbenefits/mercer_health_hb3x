package pages.pat;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SpendIncPage {
	//Spendable Income
	WebDriver driver;
	
	@FindBy(id="divSpendableIncome")
	public WebElement SpendIncome;

	@FindBy(xpath="//h4[contains(text(),'Spendable Income - Data Management')]")
	public WebElement SpendIncomeHeader;
	public HeaderPage header;

	public SpendIncPage(WebDriver driver) {
		header=new HeaderPage(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public void clickSpendIncome() throws InterruptedException {
		clickElement(SpendIncome);
		waitForPageLoad(driver);
	}


}
