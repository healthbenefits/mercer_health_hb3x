package pages.pat;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LicenceAgrMgmtPage {

	//License Agreement Management

	WebDriver driver;
	
	@FindBy(id="divLicenseManager")
	public WebElement licenseAgreement;

	@FindBy(xpath="//h4[contains(text(),'License  Agreement Management')]")
	public WebElement licAgrHeader;

	@FindBy(xpath="//a[contains(text(),'Compensation Localizer Reset')]")
	public WebElement compReset;
	
	public LicenceAgrMgmtPage(WebDriver driver) {
		
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public void clickLicenceAgreement() throws InterruptedException {
		clickElement(licenseAgreement);
		waitForPageLoad(driver);
		waitForElementToDisplay(licAgrHeader);
	}
}
