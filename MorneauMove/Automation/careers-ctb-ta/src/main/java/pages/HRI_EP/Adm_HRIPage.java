package pages.HRI_EP;

import static driverfactory.Driver.*;
import static utilities.SOAPUI_Request.getSoapServiceResponse;
import static verify.SoftAssertions.verifyContains;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import utilities.InitTests;
import static utilities.MyExtentReports.reports;

import static utilities.SOAPUI_Request.getSoapServiceResponse;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;
import java.io.IOException;

import javax.xml.soap.SOAPException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class Adm_HRIPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h2[text()='HR Import - File Dashboard']")
	public WebElement hri_Title;
	
	@FindBy(xpath = "//select[@class='col-4'][1]")//Last 365 Days
	public WebElement timePeriod;
	
	@FindBy(xpath = "//option[text()='Last 365 Days']")//Last 365 Days
	public WebElement duration;
	
	@FindBy(xpath = "//a[text()='Search']")
	public WebElement searchButton;
	
	@FindBy(xpath = "//input[@id='data-derivation-rules-filter']")
	public WebElement fileFilter;
	
	@FindBy(xpath = "//span[@data-bind='    text: FileName']")
	public WebElement fileClick;
	
	public Adm_HRIPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}

	public void search_File(String File_id) throws InterruptedException {
		
		selEleByVisbleText(timePeriod, "Last 365 Days");
		clickElement(searchButton);
		String fileid=String.valueOf(File_id.replace(".0", ""));
		clickElement(fileFilter);
		Thread.sleep(2000);
		setInput(fileFilter, fileid);
	
	}
	
	public void fileTrigger(String Soap_endpointURL, String Soap_resourcePath) throws InterruptedException {
		
		String wsdlUrl = Soap_endpointURL;
		String requestUrl = "http://tempuri.org/ITaskManagerApi/SubmitTask";
		String requestFilePath= Soap_resourcePath;
		
			try {
				String response=getSoapServiceResponse(wsdlUrl,requestUrl, requestFilePath);
			} catch (SOAPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			
		
		Thread.sleep(10000);
		refreshpage();
	
		
	
	}
	public void File_click() throws InterruptedException {
		
		clickElement(fileClick);
		
	
	}

}