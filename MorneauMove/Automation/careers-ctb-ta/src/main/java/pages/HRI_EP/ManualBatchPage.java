package pages.HRI_EP;

import static driverfactory.Driver.setInput;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeNoException;
import static verify.SoftAssertions.verifyElementTextContains;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.Executor;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.gargoylesoftware.htmlunit.javascript.host.dom.Document;

import driverfactory.Driver;
import verify.SoftAssertions;

import static driverfactory.Driver.*;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;


public class ManualBatchPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//span[contains(text(),'Setup Manual Batch')]")
	public WebElement mbpage_Title;
		
	@FindBy(xpath = "//textarea[@id=\"eval-point-expression-textarea\"]")
	 public WebElement mbExp;
	
	@FindBy(xpath = "//select[@class='col-10']")
	 public WebElement schema;
	
	@FindBy(xpath = "//input[@id=\"manual-batch-description\"]")
	 public WebElement batchDesc;
	
	@FindBy(xpath = "//select[@id=\"action\"]")
	 public WebElement le_Action;
	
	@FindBy(xpath = "//select[@id=\"event\"]")
	 public WebElement le;
	
	@FindBy(xpath = "//img[@class=\"ui-datepicker-trigger\"]")
	WebElement calendarIcon;
	
	@FindBy(xpath = "//div[@class=\"ui-datepicker-group ui-datepicker-group-first\"]")
	WebElement calCurrentMonth;
	
	@FindBy(xpath = "//a[@title=\"Continue\"]")
	WebElement continueMB;
	
	public ManualBatchPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void mb_Details(String mbExpression, String batchDescription, String action ) throws InterruptedException {
	
		setInput(mbExp, mbExpression);
		setInput(batchDesc, batchDescription);
		selEleByVisbleText(le_Action,action); 
		Thread.sleep(2000);
		selEleByVisbleText(le, "3 - Birth/Adoption");
		clickElement(calendarIcon);
		DateFormat dateFormat2 = new SimpleDateFormat("dd"); 
	    Date date2 = new Date();
	    String today = dateFormat2.format(date2).replaceFirst("0", "");
		System.out.println(today);		
		List<WebElement> columns = calCurrentMonth.findElements(By.tagName("td"));
		for (WebElement cell: columns) {

	            if (cell.getText().equals(today)) {
	                cell.click();
	                break;
	            }
	        }
		clickElement(continueMB);
		
	}
	public void mbSM_Details(String mbExpression, String batchDescription, String action ) throws InterruptedException {
		
		setInput(mbExp, mbExpression);
		setInput(batchDesc, batchDescription);
		selEleByVisbleText(schema, "Authoring");
		selEleByVisbleText(le_Action,action); 
		Thread.sleep(2000);
		selEleByVisbleText(le, "55 - OE");
		clickElement(continueMB);
		
	}
	}