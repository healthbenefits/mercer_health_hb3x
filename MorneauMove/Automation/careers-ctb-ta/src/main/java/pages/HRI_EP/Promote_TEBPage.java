package pages.HRI_EP;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class Promote_TEBPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h2[text()='Batch Events - Promote Time Elapsed Batches']")
	 public WebElement promote_beTitle;
	
	@FindBy(xpath = "//a[@title='Promote DEV to TEST']")
	public WebElement dev_test_arrow;
	
	@FindBy(xpath = "//button[@title='Promote']")
	 public WebElement promoteButton;
	
	@FindBy(xpath = "//p[text()='Time elapsed batches have been promoted successfully from CIDEV to CITEST']")
	 public WebElement promcomp_dev_test;
	
	@FindBy(xpath = "//span[@title='Close']")
	 public WebElement close_msg;
	
	@FindBy(xpath = "//a[@title='Promote TEST to CSO']")
	public WebElement test_cso_arrow;
	
	@FindBy(xpath = "//input[@id='isupport_ticket2']")
	public WebElement isuptckt;
	
	@FindBy(xpath = "//input[@class='group_lob_db']")
	public WebElement run_cb;
	
	@FindBy(xpath = "//button[@title='Run Now']")
	public WebElement runnowButton;
	
	@FindBy(xpath = "//p[text()='Time elapsed batches have been promoted successfully from CITEST to CSO']")
	 public WebElement promcomp_test_cso;
	
	@FindBy(xpath = "//a[@title='Promote CSO to PROD']")
	public WebElement cso_prod_arrow;
	
	@FindBy(xpath = "//p[text()='Time elapsed batches have been promoted successfully from CSO to PROD']")
	 public WebElement promcomp_cso_prod;
	
	public Promote_TEBPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void promote_tebDT( ) throws InterruptedException {
		
		waitForElementToDisplay(dev_test_arrow);
		clickElement(dev_test_arrow);
		waitForElementToDisplay(promoteButton);
		clickElement(promoteButton);
		waitForElementToDisplay(promcomp_dev_test);
		verifyElementTextContains(promcomp_dev_test, "Time elapsed batches have been promoted successfully from CIDEV to CITEST", test);
		clickElement(close_msg);
	}
	
	public void promote_tebTC( ) throws InterruptedException {
		
		waitForElementToDisplay(test_cso_arrow);
		clickElement(test_cso_arrow);
		waitForElementToDisplay(isuptckt);
		setInput(isuptckt, "56445");
		clickElement(run_cb);
		clickElement(runnowButton);
		waitForElementToDisplay(promcomp_test_cso);
		verifyElementTextContains(promcomp_dev_test, "Time elapsed batches have been promoted successfully from CITEST to CSO", test);
		clickElement(close_msg);
	}
	
	public void promote_tebCP( ) throws InterruptedException {
		
		waitForElementToDisplay(cso_prod_arrow);
		clickElement(cso_prod_arrow);
		waitForElementToDisplay(isuptckt);
		setInput(isuptckt, "5656465");
		clickElement(run_cb);
		clickElement(runnowButton);
		waitForElementToDisplay(promcomp_cso_prod);
		verifyElementTextContains(promcomp_cso_prod, "Time elapsed batches have been promoted successfully from CSO to PROD", test);
		clickElement(close_msg);
	}
}