package pages.mobilityportal;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.mobilityportal.HeaderPage;
public class MytoolsPage {
	public HeaderPage header;
	@FindBy(xpath = "(//a[text()='One-Time Tables'])[4]")
	public static WebElement onetimetable;
    @FindBy(xpath = "//uib-tab-heading[contains(text(),'My Tools')]")
    public static WebElement mytool_link;
    
    @FindBy(xpath = "//uib-tab-heading[contains(text(),'My Data')]")
    public static WebElement mydata_link;
    
    @FindBy(xpath = "//uib-tab-heading[contains(text(),'Custom Report')]")
    public static WebElement customreport_link;
    
    @FindBy(xpath = "//uib-tab-heading[contains(text(),'Notification')]")
    public static WebElement notification_link;
    
  
public MytoolsPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	header=new HeaderPage(driver);
}

public void oneTimeTable() throws InterruptedException {
	clickElement(onetimetable);

}
}
