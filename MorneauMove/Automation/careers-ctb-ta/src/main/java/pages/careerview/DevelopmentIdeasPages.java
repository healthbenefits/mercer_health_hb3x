package pages.careerview;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DevelopmentIdeasPages {

	WebDriver driver;
	@FindBy(xpath = "//section[@id='panel_7']//div[1]/a[@class='toggle showdevidealevels']/span[2]")
	public  WebElement developmentIdeas_TC1;
	
	@FindBy(xpath = "//section[@id='panel_7']//div[2]/a[@class='toggle showdevidealevels']/span[2]")
	public  WebElement developmentIdeas_TC2;
	
	public DevelopmentIdeasPages(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
