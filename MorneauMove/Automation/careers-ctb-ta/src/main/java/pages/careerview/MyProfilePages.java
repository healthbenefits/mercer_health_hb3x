package pages.careerview;

import static driverfactory.Driver.*;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import verify.SoftAssertions;

public class MyProfilePages {


	public String UserCurrentRole="Programmer Analyst I";

	@FindBy(xpath = "//a[@href='#panel_0']")
	public  WebElement OverviewLink;

	@FindBy(xpath = "//a[@href='#panel_1']")
	public  WebElement CoreCompetenciesLink;

	@FindBy(xpath = "//a[@href='#panel_2']")
	public  WebElement TechnicalCompetenciesLink;

	@FindBy(xpath = "//a[@href='#panel_5']")
	public  WebElement paths;

	@FindAll(@FindBy(how = How.XPATH, using = "//section[@id='panel_5']//div[contains(@class,'small-12 small-centered medium-uncentered')]//button"))    
	public List<WebElement> deletePaths;

	@FindAll(@FindBy(how = How.XPATH, using = "//li[contains(@class,'comparison row form-group')]//button"))    
	public List<WebElement> deleteComparison;

	@FindBy(xpath = "//div[@id='modal-profile']/a")
	public  WebElement close_modal;

	@FindBy(xpath = "//section[@id='panel_5']//div[1]//p[1]/b")
	public  WebElement emptyPath;

	@FindBy(xpath = "//form[1]/a[contains(@class,'role-path')]")
	public  WebElement firstPath;

	@FindBy(xpath = "//form[1]//li[1]//span[contains(text(),'Programmer Analyst I')]")
	public  WebElement userRole;

	@FindBy(xpath = "//a[@class='role-path']//li[2]//span")
	public  WebElement userSelectedRoleInPaths;

	@FindBy(xpath = "//form[2]//li[2]//span")
	public  WebElement userSelectedRoleInPaths2;

	@FindBy(xpath = "//a[@class='role-path']")
	public  WebElement UserRolePaths;

	@FindBy(xpath = "(//form[@class='delete-path path-group small-12 large-6 columns end'])[1]/button")
	public  WebElement pathName;

	@FindBy(xpath = "//a[@href='#panel_6']")
	public  WebElement comparisonsLink;

	@FindBy(xpath = "//li[1]/div/a[1]")
	public  WebElement savedComparison;

	@FindBy(xpath = "//span[contains(text(),'My Profile')]")
	public  WebElement myProfileHeader;

	@FindBy(xpath = "//a[@href='#panel_3']")
	public  WebElement managerAssessmentLink;

	@FindBy(xpath = "//a[@href='#panel_7']")
	public  WebElement developmentIdeas_Link;

	WebDriver driver;
	@FindBy(xpath = "//a[@data-category-id= 'basics']")
	public  WebElement overview_Link;

	public MyProfilePages(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void selectUserPath() throws InterruptedException {
		NavigateHomePage nhp = new NavigateHomePage(driver);
		clickElement(UserRolePaths);
		waitForElementToDisplay(nhp.editPathButton);
	}

	public void navigateToSideTab(WebElement element) throws InterruptedException {
		clickElement(element);
	}

	public void closeProfilePopup() throws InterruptedException {
		clickElement(close_modal);
	}

	public void deleteAllPaths() {
/*
		if(emptyPath.isDisplayed()) {
			System.out.println("All paths are deleted");
		}
		else {
			System.out.println(deletePaths.size());
			for(int i=1; i<=deletePaths.size();i--) {

				WebElement button = driver.findElement(By.xpath("//*[@id='panel_5']/div/div[1]/div/div/div/form["+i+"]/button"));
				clickElementUsingJavaScript(driver, button);
				Alert alert = driver.switchTo().alert();
				alert.accept();
				++i;
			}
		}*/
		
		try {
			emptyPath.isDisplayed();
			System.out.println("All paths are deleted");
		}
		catch(Exception e ){
			System.out.println(deletePaths.size());
			for(int i=1; i<=deletePaths.size();i--) {

				WebElement button = driver.findElement(By.xpath("//*[@id='panel_5']/div/div[1]/div/div/div/form["+i+"]/button"));
				clickElementUsingJavaScript(driver, button);
				Alert alert = driver.switchTo().alert();
				alert.accept();
				++i;
			}
		}
	}

	public void deletePathAndCancel() {
		clickElementUsingJavaScript(driver,pathName);
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	public void deletePath() {
		clickElementUsingJavaScript(driver,pathName);
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	public void viewComparisons() throws InterruptedException {
		clickElement(comparisonsLink);
		clickElement(savedComparison);
	}

}
