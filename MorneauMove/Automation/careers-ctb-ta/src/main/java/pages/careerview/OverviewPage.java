package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OverviewPage {

	
	@FindBy(xpath = "//a[text()='My Profile'][@class = 'blue-button modal-button-profile']")
	public  WebElement myProfile_button;

	@FindBy(xpath = "//h2/span[text()='My Profile']")
	public  WebElement myProfile_header;

	@FindBy(id = "modal-profile")
	public  WebElement profile_modal;

	@FindBy(xpath = "//div[text()='Select a Photo (JPG)']")
	public  WebElement uploadPhoto_button;

	@FindBy(xpath = "//input[@name = 'FirstName']")
	public  WebElement firstName_input;
	
	@FindBy(xpath = "//input[@name = 'LastName']")
	public  WebElement lastName_input;

	@FindBy(xpath = "//textarea[@name = 'About']")
	public  WebElement about_input;

	@FindBy(xpath = "//input[@name = 'PreferredEmail']")
	public  WebElement email;
	
	@FindBy(xpath = "//span[text()='Yes']")
	public  WebElement yesBtn;
	
	@FindBy(xpath = "//span[text()='No']")
	public  WebElement noBtn;
	
	@FindBy(xpath = "//button[@type = 'submit']")
	public  WebElement save_button;
	
	@FindBy(xpath = "(//button[text()= 'Saved'])[1]")
	public  WebElement savedInfoStyle_button;

	
	@FindBy(xpath = "//div[@id='modal-profile']/a[contains(@class,'close-reveal-modal')]")
	public  WebElement closeProfile;
	
	WebDriver driver;
	public OverviewPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	
	
	/*public void uploadPhoto() throws FindFailed, InterruptedException {
		SikuliActions sikuliAction = new SikuliActions();
		System.out.println("Sikuli called and created");
		clickElement(uploadPhoto_button);
		System.out.println("Clicked on upload button normal action");
		delay(5000);
		doubleClick("picturesTab.png");
		System.out.println("1");
//		delay(3000);
//		click("samplePicture.png");
		delay(3000);
		doubleClick("samplePicture.png");
		delay(3000);
		click("pictureToBeSelected.png");
		delay(3000);
		click("openButton.png");
		delay(3000);
		System.out.println("Picture uploaded");
		clickElement(save_button);


	}*/

	public void editInfo(String lastName,String aboutInput) throws InterruptedException {
		// TODO Auto-generated method stub
		waitForElementToDisplay(lastName_input);
		lastName_input.clear();
		setInput(lastName_input, lastName);
		about_input.clear();
		setInput(about_input, aboutInput);
		clickElement(save_button);
		waitForElementToDisplay(savedInfoStyle_button);
	}
	public void goTomyProfile() throws InterruptedException {
		// TODO Auto-generated method stub
		clickElement(myProfile_button);
		waitForElementToDisplay(myProfile_header);
	}
	public void closeProfilepopup() throws InterruptedException {
		
		clickElement(closeProfile);
	}
}
