package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterPage {

	WebDriver driver;
	
	@FindBy(xpath="//form[@action='/Account/Register']//input[@id='Password']")
	public WebElement password_input;
	
	@FindBy(id="LastName")
	public WebElement LastName;
	
	@FindBy(id="FirstName")
	public WebElement FirstName;
	
	@FindBy(id="ConfirmPassword")
	public WebElement ConfirmPwd;
	
	@FindBy(xpath="//input[contains(@value,'Submit')]")
	public WebElement SubmitBtn;
	
	@FindBy(xpath="//span[contains(text(),'Employee Id field is required')]")
	public WebElement NoEmpIdMsg;
	
	@FindBy(xpath="//span[contains(text(),'Password field is required')]")
	public WebElement noPasswordMsg;
	
	@FindBy(xpath="//span[contains(text(),'First Name field is required')]")
	public WebElement NoFirstNameMsg;
	
	@FindBy(xpath="//span[contains(text(),'Last Name field is required')]")
	public WebElement NoLastNameMsg;
	
	@FindBy(id="PasswordErrorDescription")
	public WebElement noPwdMsg;
	
	@FindBy(xpath="//form[contains(@id ,'form1')]//input[@id = 'EmployeeId']")
	public  WebElement empId;

	@FindBy(xpath="//input[parent::div[div[@id='PasswordInfo']]]")
	public WebElement password;
	
	@FindBy(xpath="//input[parent::div[div[@id='PasswordErrorDescription']]]")
	public WebElement password2;
	
	@FindBy(xpath="//input[@id='EmployeeId'][contains(@class,'input-validation-error form-control')]")
	public static WebElement empId2;
	
	@FindBy(xpath="//input[@id='EmployeeId'][contains(@value,'123')]")
	public static WebElement empId3;
	
	@FindBy(xpath="//form[@id='form0']/div[7]/ul/li")
	public static WebElement UserNotFound;
	
	public RegisterPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void clickSubmit() throws InterruptedException {
		waitForElementToDisplay(SubmitBtn);
		clickElement(SubmitBtn);
		delay(1000);
	}
	
	public void allFieldsExceptPwd(String empid , String firstname , String lastname) throws InterruptedException {
		
		waitForElementToDisplay(empId2);
		setInput(empId2, empid);
		waitForElementToDisplay(FirstName);
		setInput(FirstName, firstname);
		waitForElementToDisplay(LastName);
		setInput(LastName, lastname);
		clickSubmit();
	}
	
}
