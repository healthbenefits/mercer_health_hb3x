package pages.careerview;


import static driverfactory.Driver.*;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;

public class AdminAnalyticsPage {

	WebDriver driver;
	@FindBy(id="dataSelection")
	public  WebElement rangeSelection_dropdown;

	@FindAll(@FindBy(how = How.XPATH, using = "//select[@id='dataSelection']/option[position()<6]"))    
	public List<WebElement> rangeSelection_dropdownOptions;

	public String[] optionArray = new String[]{"", "Today","Yesterday","Last week","Fortnigtly","Last Month" }; 

	@FindBy(xpath = "//div[contains(@data-reactid,'.2.1.4.0')]//following-sibling::div//tbody//tr")
	public  List<WebElement> dateElement;

	@FindBy(xpath = "//div[contains(@data-reactid,'.2.1.5.0')]//following-sibling::div//tbody//tr")
	public  List<WebElement> graph2_LastMonth;

	@FindBy(xpath = "//div[contains(@data-reactid,'.2.1.6.0')]//following-sibling::div//tbody//tr")
	public  List<WebElement> graph3_LastMonth;

	@FindBy(xpath = "//div[contains(@data-reactid,'.2.1.7.0')]//following-sibling::div//tbody//tr")
	public  List<WebElement> graph4_LastMonth;

	public AdminAnalyticsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void selectFromRangeDropdown() {
		// TODO Auto-generated method stub
		waitForElementToDisplay(rangeSelection_dropdown);
		selEleByVisbleText(rangeSelection_dropdown, "Last Month");
		waitForPageLoad(driver);

	}

	public void selectFortnigtlyFromRangeDropdown() {
		// TODO Auto-generated method stub
		waitForElementToDisplay(rangeSelection_dropdown);
		selEleByVisbleText(rangeSelection_dropdown, "Fortnigtly");
		waitForPageLoad(driver);
	}

	public boolean checkPresenceOfGraphs() {
		try {
		for(int i = 1; i < 5 ; i++) {
			for(int j=1; j<3; j++) {
				WebElement ele = driver.findElement(By.xpath("//div[@id='chartContainer']/div["+i+"]/div["+j+"]"));
				if(isElementExisting(driver, ele))
				System.out.println("Element is present.");
			}
		}
		}
		catch(Exception e){
			return false;
		}
		return true;
	}
	
	
	public boolean checkDropdownOptions() {
		
		try {
			for(int i = 1; i <6; i++) {
				WebElement ele = driver.findElement(By.xpath("//select[@id='dataSelection']/option["+i+"]"));
				waitForElementToDisplay(ele);
				if(ele.getText().equals(optionArray[i])) {
					/*System.out.println();
					System.out.println(ele.getText());
					System.out.println();*/
				}	
			}
			
	}
		catch(Exception e){
			return false;
		}
		return true;
	}
}
