package pages.careerview;


import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NavigatePopUpPage {

	
	WebDriver driver;
	@FindBy(id = "modal-navigate")
	public  WebElement navigatePopUp_modal;
	
	@FindBy(xpath = "//a[@class = 'close-reveal-modal']")
	public  WebElement closeModal_button;
	
	@FindBy(xpath = "//a[@class='right blue-button modal-button-start']")
	public  WebElement start_button;
	
	public NavigatePopUpPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void closePopup() throws InterruptedException {
		clickElement(closeModal_button);
		
	}
	
	public void clickStart() throws InterruptedException {
		clickElement(start_button);
	}
	
}
