package pages.careerview;

import static driverfactory.Driver.*;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SuperAdmin_LandingPage {

	@FindBy(xpath="//a[contains(text(),'Sign out')]")
	public WebElement signOut_button;
	
	@FindBy(xpath="//a[contains(text(),'Select Company')]")
	public WebElement selectCompany_button;
	
	@FindBy(xpath="//button[contains(text(),'Add new client')]")
	public WebElement addNewClient_button;
	
	@FindBy(xpath="//h4[text()='Welcome to CareerView company administration portal']")
	public WebElement SuperAdmin_header;
	
	@FindAll(@FindBy(how = How.XPATH, using = "//select[@id='select']/option[position()>1 and position()<30]"))    
	public List<WebElement> companyList;
	
	@FindBy(id="select")
	public WebElement SuperAdmin_dropdown;
	WebDriver driver;
	public SuperAdmin_LandingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickAddNewClientButton() throws InterruptedException {
		clickElement(addNewClient_button);
	}
	
	public void signOut() throws InterruptedException {
		clickElement(signOut_button);
	}
	
	public void selectCompany() throws InterruptedException {
		waitForElementToDisplay(SuperAdmin_dropdown);
		selEleByVisbleText(SuperAdmin_dropdown, "Demo2016 - (URL: Demo2016)");
		clickElement(selectCompany_button);
	}
	
	
}
