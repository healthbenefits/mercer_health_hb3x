package pages.careerview;

import static driverfactory.Driver.*;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdminTaskPage {

	WebDriver driver;
	
	@FindBy(xpath = "//h5[text()='Reset manager assessment request for an employee']")
	public  WebElement adminTask_header;

	@FindBy(xpath = "//input[@name='employeeId']")
	public  WebElement employeeID_input;

	@FindBy(xpath = "//input[@value='Reset']")
	public  WebElement reset_button;

	public AdminTaskPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}

	public String resetWithWrongUsername() {
		String alertText;
		Alert alert1 = null ;

		try {
			waitForElementToEnable(employeeID_input);
			setInput(employeeID_input, "9000");
			//			clickElement(reset_button);

			clickElementUsingJavaScript(driver, reset_button);
			wait.until(ExpectedConditions.alertIsPresent());
			alert1 = driver.switchTo().alert();

		} catch (Exception e) {
			System.out.println("Element not found.");

		}
		finally {

			alert1.accept();
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert2 = driver.switchTo().alert();
			alertText = alert2.getText();
			alert2.accept();
		}
		return alertText;

	}

	public String resetWithCorrectUsername() {
		String alertText;
		Alert alert1 = null ;

		try {
			setInput(employeeID_input, "00001");
			clickElementUsingJavaScript(driver, reset_button);
			wait.until(ExpectedConditions.alertIsPresent());
			alert1 = driver.switchTo().alert();

		} catch (Exception e) {


		}
		finally {

			alert1.accept();
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert2 = driver.switchTo().alert();
			alertText = alert2.getText();
			alert2.accept();
			//			 alertText = alert1.getText();
			//			 alert1.accept();
		}
		return alertText;

	}

	public void resetAndCancel() {

		employeeID_input.clear();
		setInput(employeeID_input, "9000");
		clickElementUsingJavaScript(driver, reset_button);
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert1 =  driver.switchTo().alert();
		alert1.dismiss();
	}
	
	public String performResetAction(String username,WebDriver driver) {
		// TODO Auto-generated method stub
		String alertText;
		String alertText1;
		Alert alert1 = null ;
		
		try {
			
			setInput(employeeID_input, username);
			driver.findElement(By.xpath("//input[@value='Reset']")).click();
			
			//Driver.clickElementUsingJavaScript(driver, reset_button);
			wait.until(ExpectedConditions.alertIsPresent());
			alert1 = driver.switchTo().alert();
			alertText1 = alert1.getText();
			 System.out.println(alertText1);
			
			 
		} catch (Exception e) {
			
			System.out.println("Exception occurred");
			
		}
		finally {
			 alert1.accept();
			 wait.until(ExpectedConditions.alertIsPresent());
			 Alert alert2 = driver.switchTo().alert();
			 alertText = alert2.getText();
			 System.out.println(alertText);
			 alert2.accept();

		}
		return alertText;

	}

	public boolean performResetAction(String username) {
		// TODO Auto-generated method stub
		String alertText;
		Alert alert1 = null ;
		WebDriverWait wait = new WebDriverWait(driver, 20);
		
			setInput(employeeID_input, username);
			//clickElement(reset_button);
		clickElementUsingJavaScript(driver, reset_button);
			wait.until(ExpectedConditions.alertIsPresent());
			alert1 = driver.switchTo().alert();
			 alert1.accept();
			 Alert alert2 = driver.switchTo().alert();
			 delay(3000);
			 alertText = alert2.getText();
			 alert2.accept();

			 if(alertText.contains("Assessment request cleared"));
			// driver.findElement(By.xpath("//input[@value='Reset']")).click();
			 
		return true;

	}

}
