
@IBCG2Regression @Login_Management_Regression
Feature: Regression set-2 (24 test cases): Regression tests for MAS-IBCG2 login management functionality

   # Regression Test case - 1
  @Pre-requisite @MAS_Update_Username_validation @MAS_Update_ContactMethod_validation @S1
  Scenario: Pre-requisite - User is logged in to IBCG2 app via MAS
    Given  User opens MAS login page for IBCG2-QAWORK application
    When User enters "MASLoginPage|Username" field as "AUTO_LoginManagement@Qawork.Test" value
    And User enters "MASLoginPage|Password" field as "Welcome1!" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When   User performs MFA challenge
    Then "IBCG2|HeaderTabs" page or message should be displayed

  # Regression Test case - 2
  @MAS_Update_Username_validation @MAS_Update_ContactMethod_validation @S1
  Scenario: To verify user is able to access MAS security settings page after login
   When User Clicks on "IBCG2|LoginManagement" Link or Button
    Then Verify that MAS Security Settings page loads successfully in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "Welcome1!" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_Information_header" page or message should be displayed in new browser window

  # Regression Test case - 3
  @MAS_Update_Username_validation
  Scenario: To verify user is able to update username on Contact information page
    When User enters "MASSecuritySettingsPage|EnterUsername" field as "test@a.a" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|UpdateButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_info_update_confirmation" page or message should be displayed in new browser window

  # Regression Test case - 4
  @MAS_Update_Username_validation
  Scenario: To verify error message when user enters existing username on Contact information page
    When User enters "MASSecuritySettingsPage|EnterUsername" field as "AUTO_QAWORK0004@mas.com" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|UpdateButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Username_already_exist_error" page or message should be displayed in new browser window

  # Regression Test case - 5
  @MAS_Update_Username_validation
  Scenario: To verify error message when user enters invalid username format on Contact information page
    When User enters "MASSecuritySettingsPage|EnterUsername" field as "abc123" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|Update_Contact_Overview" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Username_invalid_format_error" page or message should be displayed in new browser window
    When User enters "MASSecuritySettingsPage|EnterUsername" field as "1@!.1" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|Update_Contact_Overview" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Username_invalid_format_error" page or message should be displayed in new browser window
    
   # Regression Test case - 6
  @MAS_Update_Username_validation
  Scenario: Post validation - Reverting the username change
    When User enters "MASSecuritySettingsPage|EnterUsername" field as "AUTO_LoginManagement@Qawork.Test" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|UpdateButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_info_update_confirmation" page or message should be displayed in new browser window

  # Regression Test case - 7
  @MAS_Update_ContactMethod_validation
  Scenario: To verify user is able to update new email address as a contact method on Contact information page
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User enters "MASSecuritySettingsPage|Enteremail" field as "test@test.com" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    And User Clicks on "MASSecuritySettingsPage|UpdateButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_info_update_confirmation" page or message should be displayed in new browser window

      # Regression Test case - 8
  @MAS_Update_ContactMethod_validation
  Scenario: To verify error message when user enters existing email address as an additional contact method
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User enters "MASSecuritySettingsPage|Enteremail" field as "test@test.com" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|email_already_exist_error" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CancelButton" Link or Button in new browser window

          # Regression Test case - 9
  @MAS_Update_ContactMethod_validation @S1
  Scenario: To verify error message when user enters email address which is used as Username as an additional contact method
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User enters "MASSecuritySettingsPage|Enteremail" field as "AUTO_LoginManagement@Qawork.Test" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Email_UsedAsUsername_Error" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CancelButton" Link or Button in new browser window

  # Regression Test case - 9
  @MAS_Update_ContactMethod_validation
  Scenario: To verify error message when user enters email address with wrong email format as a contact method
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User enters "MASSecuritySettingsPage|Enteremail" field as "abc123!@#" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|Add_more_Contact_header" Link or Button in new browser window
    Then "MASSecuritySettingsPage|email_invalid_format_error" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CancelButton" Link or Button in new browser window

      # Regression Test case - 10
  @MAS_Update_ContactMethod_validation
  Scenario: To verify user is able to delete an email address from contact methods on Contact information page
    When User Clicks on "MASSecuritySettingsPage|email_delete_icon" Link or Button in new browser window
    And User Clicks on "MASSecuritySettingsPage|UpdateButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_info_update_confirmation" page or message should be displayed in new browser window

      # Regression Test case - 11
  @MAS_Update_ContactMethod_validation
  Scenario: To verify user is able to update new phone number as a contact method on Contact information page
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User Clicks on "MASSecuritySettingsPage|SelectPhone" Link or Button in new browser window
    And User enters "MASSecuritySettingsPage|Enterphone" field as "1111111111" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    And User Clicks on "MASSecuritySettingsPage|UpdateButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_info_update_confirmation" page or message should be displayed in new browser window

  # Regression Test case - 12
  @MAS_Update_ContactMethod_validation
  Scenario: To verify error message when user enters existing phone number as an additional contact method
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User Clicks on "MASSecuritySettingsPage|SelectPhone" Link or Button in new browser window
    When User enters "MASSecuritySettingsPage|Enterphone" field as "1111111111" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|phone_already_exist_error" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CancelButton" Link or Button in new browser window

   # Regression Test case - 13
  @MAS_Update_ContactMethod_validation
  Scenario: To verify user is able to delete a phone number from contact methods on Contact information page
    When User Clicks on "MASSecuritySettingsPage|phone_delete_icon" Link or Button in new browser window
    And User Clicks on "MASSecuritySettingsPage|UpdateButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_info_update_confirmation" page or message should be displayed in new browser window

  # Regression Test case - 14
  @MAS_Update_ContactMethod_validation
  Scenario: To verify error message when user enters more than 3 email addresses as an additional contact method
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User enters "MASSecuritySettingsPage|Enteremail" field as "test@test.com" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User enters "MASSecuritySettingsPage|Enteremail" field as "test@test1.com" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
        When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User enters "MASSecuritySettingsPage|Enteremail" field as "test@test3.com" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|max_3_email_error" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CancelButton" Link or Button in new browser window

     # Regression Test case - 15
  @MAS_Update_ContactMethod_validation
  Scenario: To verify error message when user enters more than 3 phone numbers as an additional contact method
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User Clicks on "MASSecuritySettingsPage|SelectPhone" Link or Button in new browser window
    When User enters "MASSecuritySettingsPage|Enterphone" field as "(111) 111-1111" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User Clicks on "MASSecuritySettingsPage|SelectPhone" Link or Button in new browser window
    When User enters "MASSecuritySettingsPage|Enterphone" field as "+111111111111" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User Clicks on "MASSecuritySettingsPage|SelectPhone" Link or Button in new browser window
    When User enters "MASSecuritySettingsPage|Enterphone" field as "+111 111 111 1111" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User Clicks on "MASSecuritySettingsPage|SelectPhone" Link or Button in new browser window
    When User enters "MASSecuritySettingsPage|Enterphone" field as "11111111111111" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|max_3_phone_error" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CancelButton" Link or Button in new browser window

         # Regression Test case - 16
  @MAS_Update_ContactMethod_validation
  Scenario: To verify additional contact additional contact method can be either a phone or an email address
    When user deletes all phones from the contact method list
    And user deletes 2 email from the contact method list
    Then verify trash can icon on the available email adderss is greyed out and verify tooltip text is displayed
     When user adds a new phone number to the contact method list
    Then verify trash can icons for available phone and email address are not greyed out and can be deleted
    When user deletes email address from the contact method list
    Then verify trash can icon on the available phone number is greyed out and verify tooltip text is displayed

    # Regression Test case - 17
  @MAS_Update_ContactMethod_validation
  Scenario: Post validation - reverting the contact method change
    When User Clicks on "MASSecuritySettingsPage|AddContactLink" Link or Button in new browser window
    And User enters "MASSecuritySettingsPage|Enteremail" field as "nishant.kolte@mercer.com" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|AddContactButton" Link or Button in new browser window
    When User Clicks on "MASSecuritySettingsPage|phone_1" Link or Button in new browser window
    When User Clicks on "MASSecuritySettingsPage|UpdateButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_info_update_confirmation" page or message should be displayed in new browser window

      # Regression Test case - 18
  @MAS_ChangePassword_validation @test
  Scenario: To verify user is able to change password successfully
    When User Clicks on "MASSecuritySettingsPage|UpdatePasswordLink" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Change_Password_header" page or message should be displayed in new browser window
    When User enters "MASSecuritySettingsPage|Old_Password" field as "Welcome1!" value in new browser window
    And User enters "MASSecuritySettingsPage|New_Password" field as "Welcome2!" value in new browser window
    And User enters "MASSecuritySettingsPage|Confirm_Password" field as "Welcome2!" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|Submit_button" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Password_Change_Confirmation" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|Back_button" Link or Button in new browser window
    Then Verify that MAS Security Settings page loads successfully in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "Welcome2!" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_Information_header" page or message should be displayed in new browser window

       # Regression Test case - 19
  @MAS_ChangePassword_validation @test
  Scenario: To verify error message displayed when user enters previously used 8 password on Change Password screen
    When User Clicks on "MASSecuritySettingsPage|UpdatePasswordLink" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Change_Password_header" page or message should be displayed in new browser window
    When User enters "MASSecuritySettingsPage|Old_Password" field as "Welcome2!" value in new browser window
    And User enters "MASSecuritySettingsPage|New_Password" field as "Welcome1!" value in new browser window
    And User enters "MASSecuritySettingsPage|Confirm_Password" field as "Welcome1!" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|Submit_button" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Password_policy_error" page or message should be displayed in new browser window

       # Regression Test case - 20
  @MAS_ChangePassword_validation @test
  Scenario: To verify error message displayed when user enters wrong old password on Change Password screen
    When User enters "MASSecuritySettingsPage|Old_Password" field as "invalid" value in new browser window
    And User enters "MASSecuritySettingsPage|New_Password" field as "Welcome3!" value in new browser window
    And User enters "MASSecuritySettingsPage|Confirm_Password" field as "Welcome3!" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|Submit_button" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Incorrect_Password_error" page or message should be displayed in new browser window

        # Regression Test case - 21
  @MAS_ChangePassword_validation @test
  Scenario: To verify error validation when user enters same old and new password
    When User enters "MASSecuritySettingsPage|Old_Password" field as "Welcome3!" value in new browser window
    And User enters "MASSecuritySettingsPage|New_Password" field as "Welcome3!" value in new browser window
    And User enters "MASSecuritySettingsPage|Confirm_Password" field as "Welcome3!" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|Change_Password_header" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Same_Old_New_Password_error" page or message should be displayed in new browser window

          # Regression Test case - 22
  @MAS_ChangePassword_validation @test
  Scenario: To verify error validation when user enters different new password and confirm password
    When User enters "MASSecuritySettingsPage|Old_Password" field as "Welcome1!" value in new browser window
    And User enters "MASSecuritySettingsPage|New_Password" field as "Welcome2!" value in new browser window
    And User enters "MASSecuritySettingsPage|Confirm_Password" field as "Welcome3!" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|Change_Password_header" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Password_mismatch_error" page or message should be displayed in new browser window

            # Regression Test case - 23
  @MAS_ChangePassword_validation @test
  Scenario: To verify error validation when user enters password not as per password rule in new password field
    When User enters "MASSecuritySettingsPage|Old_Password" field as "Welcome1!" value in new browser window
    And User enters "MASSecuritySettingsPage|New_Password" field as "ABCD@123" value in new browser window
    And User enters "MASSecuritySettingsPage|Confirm_Password" field as "ABCD@123" value in new browser window
    And User Clicks on "MASSecuritySettingsPage|Change_Password_header" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Password_rule_error" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|Cancel_button" Link or Button in new browser window
    Then "MASSecuritySettingsPage|EnterPassword" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CloseButton" Link or Button in new browser window
    Then MAS security setting page is closed successfully
    And "IBCG2|HeaderTabs" page or message should be displayed

  # Regression Test case - 24
  @MAS_Security_Settings_account_locked @test
  Scenario: To verify user's account gets locked on security settings page if user makes 5 invalid password attempts
    When User Clicks on "IBCG2|LoginManagement" Link or Button
    Then Verify that MAS Security Settings page loads successfully in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "invalid" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Incorrect_Password_error" page or message should be displayed in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "invalid" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|two_attempts_remaining_message" page or message should be displayed in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "invalid" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|one_attempt_remaining_message" page or message should be displayed in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "invalid" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Account_locked_message" page or message should be displayed in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "invalid" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Account_locked_message" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CloseButton" Link or Button in new browser window
    Then MAS security setting page is closed successfully
    And "IBCG2|HeaderTabs" page or message should be displayed
    When User Clicks on "IBCG2|Logout" Link or Button
    When User clicks on YES button on the logout popup alert box
    Then "MASLoginPage|LogoutMessage" page or message should be displayed
    When User enters "MASLoginPage|Username" field as "AUTO_LoginManagement@Qawork.Test" value
    And User enters "MASLoginPage|Password" field as "Welcome2!" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then "MASLoginPage|Account_locked_Message" page or message should be displayed

# @Issue
#   Scenario: Test
#         Given  User opens MAS security settings page
#         When User enters "MASSecuritySettingsPage|EnterPassword" field as "Test@001" value
#          When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button
#         Then "MASSecuritySettingsPage|Contact_Information_header" page or message should be displayed
#         Then Verify one additional email address "nishant.kolte@mercer.com" is added in Contact method section