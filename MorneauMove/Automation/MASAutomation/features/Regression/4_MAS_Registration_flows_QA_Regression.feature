@IBCG2Regression @Registration_Flows_Regression @Pre-Login_FLows
Feature: Regression set-4 (8 Test cases): Regression test for MAS-IBCG2 Registration flows

  # Regression Test case - 1
  @IBCG2Regression @RegistrationFlow-1
  Scenario: To verify end-to-end MAS registration flow for IBCG2 user (email on file)
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|GetStarted_Button" Link or Button
    Then "RegistrationFlow|Registration_header" page or message should be displayed
    When User enters "RegistrationFlow|Last4SSN_field" field as "1830" value
    And User enters "RegistrationFlow|LastName_field" field as "Auto'Last-Name" value
    And User enters "RegistrationFlow|DOB_field" field as "3/12/1970" value
    And User enters "RegistrationFlow|PostalCode_field" field as "43068" value
    And User Clicks on "RegistrationFlow|Next_Button" Link or Button
    Then "MFApage|SelectContact" page or message should be displayed
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then "RegistrationFlow|SelectUsername_label" page or message should be displayed
    And User Clicks on "RegistrationFlow|EnterUsername_checkbox" Link or Button
    When User enters "RegistrationFlow|EnterNewUsername_Field" field as "AUTO_QAWORK0004@mas.com" value
    And User Clicks on "RegistrationFlow|Continue_Button" Link or Button
    Then "RegistrationFlow|AlreadyRegistered_Popup" page or message should be displayed
    And User Clicks on "RegistrationFlow|Cancel_Button" Link or Button
    When User enters "RegistrationFlow|EnterNewUsername_Field" field as "registrationtest1@qawork.com" value
    And User Clicks on "RegistrationFlow|Continue_Button" Link or Button
    Then "RegistrationFlow|CompleteRegistration_Page" page or message should be displayed
    And "RegistrationFlow|Email_On_File_Field" page or message should be displayed
    And "RegistrationFlow|Phone_On_File_Field" page or message should be displayed
    And User Clicks on "RegistrationFlow|Email_On_File_Field" Link or Button
    And User enters "RegistrationFlow|NewPassword_field" field as "Test@001" value
    And User enters "RegistrationFlow|ConfirmPassword_field" field as "Test@001" value
    And User Clicks on "RegistrationFlow|CompleteRegistration_Button" Link or Button
    Then "RegistrationFlow|Confirmation_message" page or message should be displayed
    When User Clicks on "RegistrationFlow|LoginToYourAccount_Button" Link or Button
    Then "MASLoginPage|labelLoginTextMessage" page or message should be displayed
    When User enters "MASLoginPage|Username" field as "registrationtest1@qawork.com" value
    And User enters "MASLoginPage|Password" field as "Test@001" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When   User performs MFA challenge
    Then Verify SSO to target application - "IBCG2" happend successfully

 # Regression Test case - 2
  @IBCG2Regression @RegistrationFlow-2 @retest
  Scenario: To verify end-to-end MAS registration flow for IBCG2 user (email not on file)
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|GetStarted_Button" Link or Button
    Then "RegistrationFlow|Registration_header" page or message should be displayed
    When User enters "RegistrationFlow|Last4SSN_field" field as "9602" value
    And User enters "RegistrationFlow|LastName_field" field as "Mungay" value
    And User enters "RegistrationFlow|DOB_field" field as "03/25/1949" value
    And User enters "RegistrationFlow|PostalCode_field" field as "75093" value
    And User Clicks on "RegistrationFlow|Next_Button" Link or Button
    Then "RegistrationFlow|CreateUsername_label" page or message should be displayed
    When User enters "RegistrationFlow|CreateUsername_TextField" field as "nishant.kolte@mercer.com" value
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then "RegistrationFlow|CompleteRegistration_Page" page or message should be displayed
    When User Clicks on "RegistrationFlow|Email_radiobutton" Link or Button
    When User enters "RegistrationFlow|Enter_Email_Or_Phone_field" field as "nishant.kolte@mercer.com" value
    And User enters "RegistrationFlow|NewPassword_field" field as "Test@001" value
    And User enters "RegistrationFlow|ConfirmPassword_field" field as "Test@001" value
    Then "RegistrationFlow|Same_Username_and_Email_Error" page or message should be displayed
    When User Clicks on "RegistrationFlow|Phone_radiobutton" Link or Button
    When User enters "RegistrationFlow|Enter_Email_Or_Phone_field" field as "+918788748033" value
    And User Clicks on "RegistrationFlow|CompleteRegistration_Button" Link or Button
    Then "RegistrationFlow|Confirmation_message" page or message should be displayed
    When User Clicks on "RegistrationFlow|LoginToYourAccount_Button" Link or Button
    Then "MASLoginPage|labelLoginTextMessage" page or message should be displayed
    When User enters "MASLoginPage|Username" field as "nishant.kolte@mercer.com" value
    And User enters "MASLoginPage|Password" field as "Test@001" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When   User performs MFA challenge
    Then Verify SSO to target application - "IBCG2" happend successfully

 # Regression Test case - 3
  @IBCG2Regression @RegistrationFlow-3
  Scenario: To verify error message in registration flow when user does not have web access
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|GetStarted_Button" Link or Button
    Then "RegistrationFlow|Registration_header" page or message should be displayed
    When User enters "RegistrationFlow|Last4SSN_field" field as "6496" value
    And User enters "RegistrationFlow|LastName_field" field as "Myers" value
    And User enters "RegistrationFlow|DOB_field" field as "10/8/1958" value
    And User enters "RegistrationFlow|PostalCode_field" field as "63366" value
    And User Clicks on "RegistrationFlow|Next_Button" Link or Button
    Then  Verify "RegistrationFlow|NoWebAccess_Error" error message displayed is "We have found your account by the information you provided, but you are not eligible for this website"
    
 # Regression Test case - 4
  @IBCG2Regression @RegistrationFlow-4
  Scenario: To verify error message in registration flow when user is an returning user (already registered to MAS)
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|GetStarted_Button" Link or Button
    Then "RegistrationFlow|Registration_header" page or message should be displayed
    When User enters "RegistrationFlow|Last4SSN_field" field as "0699" value
    And User enters "RegistrationFlow|LastName_field" field as "O'Brien" value
    And User enters "RegistrationFlow|DOB_field" field as "12/29/1949" value
    And User enters "RegistrationFlow|PostalCode_field" field as "75252" value
    And User Clicks on "RegistrationFlow|Next_Button" Link or Button
    Then  Verify "RegistrationFlow|UserNotFoundError" error message displayed is "We are not able to find you by the information you provided or the information is already associated to an account."

  # Regression Test case - 5
  @IBCG2Regression @RegistrationFlow-5
  Scenario: To verify error message in registration flow when invaid 4 piece of information is entered
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|GetStarted_Button" Link or Button
    Then "RegistrationFlow|Registration_header" page or message should be displayed
    When User enters "RegistrationFlow|Last4SSN_field" field as "1111" value
    And User enters "RegistrationFlow|LastName_field" field as "O'Brien" value
    And User enters "RegistrationFlow|DOB_field" field as "11/11/1911" value
    And User enters "RegistrationFlow|PostalCode_field" field as "11111" value
    And User Clicks on "RegistrationFlow|Next_Button" Link or Button
    Then  Verify "RegistrationFlow|UserNotFoundError" error message displayed is "We are not able to find you by the information you provided or the information is already associated to an account."

  # Regression Test case - 6
  @IBCG2Regression @RegistrationFlow-6
  Scenario: To verify error message in registration flow when multiple users has same 4 piece of information
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|GetStarted_Button" Link or Button
    Then "RegistrationFlow|Registration_header" page or message should be displayed
    When User enters "RegistrationFlow|Last4SSN_field" field as "1902" value
    And User enters "RegistrationFlow|LastName_field" field as "TEST" value
    And User enters "RegistrationFlow|DOB_field" field as "6/14/1955" value
    And User enters "RegistrationFlow|PostalCode_field" field as "75044" value
    And User Clicks on "RegistrationFlow|Next_Button" Link or Button
    Then  Verify "RegistrationFlow|MultipleUsersFound_Error" error message displayed is "Something went wrong. Please try again."

  # Regression Test case - 7
  @IBCG2Regression @RegistrationFlow-7 @retest
  Scenario: To verify registration flow for IBCG2 user having usercode6/usercode7='P' in BW database (email on file)
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|GetStarted_Button" Link or Button
    Then "RegistrationFlow|Registration_header" page or message should be displayed
    When User enters "RegistrationFlow|Last4SSN_field" field as "9452" value
    And User enters "RegistrationFlow|LastName_field" field as "Jame's-Bond" value
    And User enters "RegistrationFlow|DOB_field" field as "3/28/1985" value
    And User enters "RegistrationFlow|PostalCode_field" field as "80234" value
    And User Clicks on "RegistrationFlow|Next_Button" Link or Button
    Then "MFApage|SelectContactPageOverview" page or message should be displayed
    And "MFApage|SelectEmail" page or message should be displayed

# Regression Test case - 8
      @IBCG2Regression @RegistrationFlow-8 @RF_test
  Scenario: To verify registration flow for IBCG2 user having only phone number on file(no email on file)
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|GetStarted_Button" Link or Button
    Then "RegistrationFlow|Registration_header" page or message should be displayed
    When User enters "RegistrationFlow|Last4SSN_field" field as "5340" value
    And User enters "RegistrationFlow|LastName_field" field as "Auto'Lname" value
    And User enters "RegistrationFlow|DOB_field" field as "10/17/1977" value
    And User enters "RegistrationFlow|PostalCode_field" field as "74112" value
    And User Clicks on "RegistrationFlow|Next_Button" Link or Button
    Then "MFApage|SelectContactPageOverview" page or message should be displayed
    And "MFApage|SelectPhone" page or message should be displayed