@IBCG2Regression @Forgot_Flows_Regression @Pre-Login_FLows
Feature: Regression set-3 (13 test cases): Regression test for MAS-IBCG2 Forgot Username and Forgot Password flows

  # Regression Test case - 1
  @IBCG2Regression @ForgotUsernameFlow-1 @retest
  Scenario: To verify end-to-end forgot username flow for an active MAS user of IBCG2
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotUsernameLink" Link or Button
    Then "ForgotUsernameFlow|ForgotUsername_header" page or message should be displayed
    When User enters "ForgotUsernameFlow|Last4SSN_field" field as "0699" value
    And User enters "ForgotUsernameFlow|LastName_field" field as "O'Brien" value
    And User enters "ForgotUsernameFlow|DOB_field" field as "12/29/1949" value
    And User enters "ForgotUsernameFlow|PostalCode_field" field as "75252" value
    And User Clicks on "ForgotUsernameFlow|Next_Button" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then "ForgotUsernameFlow|YourUsernameIs_header" page or message should be displayed
    Then Verify on final page - "ForgotUsernameFlow|RecoveredUsername_field", Username is succsessfully displayed as "auto_forgotflowtest@qawork.com"
    When User Clicks on "ForgotUsernameFlow|LoginToYourAccount_Button" Link or Button
    Then "MASLoginPage|labelLoginTextMessage" page or message should be displayed

# Regression Test case - 2
    @IBCG2Regression @ForgotUsernameFlow-2 @retest
  Scenario: To verify end-to-end forgot username flow for a migrated MAS user of IBCG2 (email from core/BW should be displayed)
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotUsernameLink" Link or Button
    Then "ForgotUsernameFlow|ForgotUsername_header" page or message should be displayed
    When User enters "ForgotUsernameFlow|Last4SSN_field" field as "3854" value
    And User enters "ForgotUsernameFlow|LastName_field" field as "Akins" value
    And User enters "ForgotUsernameFlow|DOB_field" field as "6/10/1962" value
    And User enters "ForgotUsernameFlow|PostalCode_field" field as "80229" value
    And User Clicks on "ForgotUsernameFlow|Next_Button" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then "ForgotUsernameFlow|YourUsernameIs_header" page or message should be displayed
    Then Verify on final page - "ForgotUsernameFlow|RecoveredUsername_field", Username is succsessfully displayed as "auto_migrateduserforgotflowtest"
    When User Clicks on "ForgotUsernameFlow|LoginToYourAccount_Button" Link or Button
    Then "MASLoginPage|labelLoginTextMessage" page or message should be displayed

# Regression Test case - 3
    @IBCG2Regression @ForgotUsernameFlow-3
  Scenario: To verify forgot username flow for a migrated MAS user of IBCG2 (no contact methods on file)
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotUsernameLink" Link or Button
    Then "ForgotUsernameFlow|ForgotUsername_header" page or message should be displayed
    When User enters "ForgotUsernameFlow|Last4SSN_field" field as "8531" value
    And User enters "ForgotUsernameFlow|LastName_field" field as "Park" value
    And User enters "ForgotUsernameFlow|DOB_field" field as "3/25/1954" value
    And User enters "ForgotUsernameFlow|PostalCode_field" field as "66209" value
    And User Clicks on "ForgotUsernameFlow|Next_Button" Link or Button
    Then "ForgotUsernameFlow|NoContactMethodPage" page or message should be displayed
    When User Clicks on "ForgotUsernameFlow|ReturnToLoginPage_Button" Link or Button
    Then "MASLoginPage|labelLoginTextMessage" page or message should be displayed

# Regression Test case - 4
    @IBCG2Regression @ForgotUsernameFlow-4
  Scenario: To verify error message in forgot username flow when a user is not registered to MAS
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotUsernameLink" Link or Button
    Then "ForgotUsernameFlow|ForgotUsername_header" page or message should be displayed
    When User enters "ForgotUsernameFlow|Last4SSN_field" field as "6496" value
    And User enters "ForgotUsernameFlow|LastName_field" field as "Myers" value
    And User enters "ForgotUsernameFlow|DOB_field" field as "10/8/1958" value
    And User enters "ForgotUsernameFlow|PostalCode_field" field as "63366" value
    And User Clicks on "ForgotUsernameFlow|Next_Button" Link or Button
    Then "ForgotUsernameFlow|UserNotRegisteredError" page or message should be displayed
    
# Regression Test case - 5
    @IBCG2Regression @ForgotUsernameFlow-5 @retest
  Scenario: To verify error message in forgot username flow when invalid 4 piece of information is entered
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotUsernameLink" Link or Button
    Then "ForgotUsernameFlow|ForgotUsername_header" page or message should be displayed
    When User enters "ForgotUsernameFlow|Last4SSN_field" field as "1111" value
    And User enters "ForgotUsernameFlow|LastName_field" field as "Test111" value
    And User enters "ForgotUsernameFlow|DOB_field" field as "1/11/1911" value
    And User enters "ForgotUsernameFlow|PostalCode_field" field as "11111" value
    And User Clicks on "ForgotUsernameFlow|Next_Button" Link or Button
    Then "ForgotUsernameFlow|UserNotFoundError" page or message should be displayed

# Regression Test case - 6
    @IBCG2Regression @ForgotUsernameFlow-6
  Scenario: To verify error messages in forgot username flow when invalid format of 4 piece of information are entered
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotUsernameLink" Link or Button
    Then "ForgotUsernameFlow|ForgotUsername_header" page or message should be displayed
    When User enters "ForgotUsernameFlow|Last4SSN_field" field as "abcd" value
    And User enters "ForgotUsernameFlow|DOB_field" field as "20-20-2018" value
    And User enters "ForgotUsernameFlow|PostalCode_field" field as "222" value
    And User enters "ForgotUsernameFlow|LastName_field" field as "Test111" value
    Then "ForgotUsernameFlow|InvalidSSNformat_Error" page or message should be displayed
    Then "ForgotUsernameFlow|InvalidDOB_Error" page or message should be displayed
    Then "ForgotUsernameFlow|InvalidPostalCode_Error" page or message should be displayed

# Regression Test case - 7 
@IBCG2Regression @ForgotPasswordFlow-1
  Scenario: To verify end-to-end forgot Password flow for an active MAS user of IBCG2
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotPasswordLink" Link or Button
    Then "ForgotPasswordFlow|EnterUsername_header" page or message should be displayed
    When User enters "ForgotPasswordFlow|Username_field" field as "AUTO_ForgotFlowtest@Qawork.com" value
    And User Clicks on "ForgotPasswordFlow|Submit_Button" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    When verify tooltip message is displayed when user does mousehover on 'I dont have access' link
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then "ForgotPasswordFlow|CreatePassword_header" page or message should be displayed
    And "ForgotPasswordFlow|MaskedUsername_field" page or message should be displayed
    When User enters "ForgotPasswordFlow|NewPassword_field" field as "Test@001" value
    And User enters "ForgotPasswordFlow|ConfirmPassword_field" field as "Test@001" value
    And User Clicks on "ForgotPasswordFlow|Submit_Button" Link or Button
    Then "ForgotPasswordFlow|Confirmation_message" page or message should be displayed
    And User Clicks on "ForgotPasswordFlow|LoginToYourAccount_Button" Link or Button
    Then "MASLoginPage|labelLoginTextMessage" page or message should be displayed

# Regression Test case - 8
    @IBCG2Regression @ForgotPasswordFlow-2 @retest
  Scenario: To verify end-to-end forgot password flow for a migrated MAS user of IBCG2 (email from core/BW should be displayed)
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotPasswordLink" Link or Button
    Then "ForgotPasswordFlow|EnterUsername_header" page or message should be displayed
    When User enters "ForgotPasswordFlow|Username_field" field as "AUTO_Migrateduserforgotflowtest" value
    And User Clicks on "ForgotPasswordFlow|Submit_Button" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then "ForgotPasswordFlow|CreatePassword_header" page or message should be displayed
    And "ForgotPasswordFlow|MaskedUsername_field" page or message should be displayed
    When User enters "ForgotPasswordFlow|NewPassword_field" field as "Test@001" value
    And User enters "ForgotPasswordFlow|ConfirmPassword_field" field as "Test@001" value
    And User Clicks on "ForgotPasswordFlow|Submit_Button" Link or Button
    Then "ForgotPasswordFlow|Confirmation_message" page or message should be displayed
    And User Clicks on "ForgotPasswordFlow|LoginToYourAccount_Button" Link or Button
    Then "MASLoginPage|labelLoginTextMessage" page or message should be displayed

# Regression Test case - 9
    @IBCG2Regression @ForgotPasswordFlow-3
  Scenario: To verify forgot password flow for a migrated MAS user of IBCG2 (no contact methods on file)
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotPasswordLink" Link or Button
    Then "ForgotPasswordFlow|EnterUsername_header" page or message should be displayed
    When User enters "ForgotPasswordFlow|Username_field" field as "AUTO_Migrateduserforgotflowtest3" value
    And User Clicks on "ForgotPasswordFlow|Submit_Button" Link or Button
    Then "ForgotPasswordFlow|NoContactMethodPage" page or message should be displayed
    When User Clicks on "ForgotUsernameFlow|ReturnToLoginPage_Button" Link or Button
    Then "MASLoginPage|labelLoginTextMessage" page or message should be displayed

# Regression Test case - 10
    @IBCG2Regression @ForgotPasswordFlow-4
  Scenario: To verify error message in forgot password flow when invalid username is entered
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotPasswordLink" Link or Button
    Then "ForgotPasswordFlow|EnterUsername_header" page or message should be displayed
    When User enters "ForgotPasswordFlow|Username_field" field as "invalidusername" value
    And User Clicks on "ForgotPasswordFlow|Submit_Button" Link or Button
    Then "ForgotPasswordFlow|UsernameNotFoundError" page or message should be displayed

    # Regression Test case - 11
    @IBCG2Regression @ForgotPasswordFlow-5 @retest
  Scenario: To verify error message in forgot password flow when username entered is having inactive account status
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotPasswordLink" Link or Button
    Then "ForgotPasswordFlow|EnterUsername_header" page or message should be displayed
    When User enters "ForgotPasswordFlow|Username_field" field as "AUTO_inactiveuser@mas.com" value
    And User Clicks on "ForgotPasswordFlow|Submit_Button" Link or Button
    Then "ForgotPasswordFlow|InactiveuserError" page or message should be displayed

        # Regression Test case - 12
    @IBCG2Regression @ForgotPasswordFlow-6
  Scenario: To verify error message in forgot password flow when username entered is having locked account status(locked by mercer)
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    And User Clicks on "MASLoginPage|ForgotPasswordLink" Link or Button
    Then "ForgotPasswordFlow|EnterUsername_header" page or message should be displayed
    When User enters "ForgotPasswordFlow|Username_field" field as "AUTO_lockedByMercer@mas.com" value
    And User Clicks on "ForgotPasswordFlow|Submit_Button" Link or Button
    Then "ForgotPasswordFlow|LockedByMercerError" page or message should be displayed

          # Regression Test case - 13
    @IBCG2Regression @ForgotPasswordFlow-6
  Scenario: To verify error message in forgot password flow when username entered is locked due to 5 invalid attempts
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_Accountlocked@mas.com" value
    And User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    When User enters "MASLoginPage|Username" field as "AUTO_Accountlocked@mas.com" value
    And User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    When User enters "MASLoginPage|Username" field as "AUTO_Accountlocked@mas.com" value
    And User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    When User enters "MASLoginPage|Username" field as "AUTO_Accountlocked@mas.com" value
    And User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    When User enters "MASLoginPage|Username" field as "AUTO_Accountlocked@mas.com" value
    And User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User Clicks on "MASLoginPage|ForgotPasswordLink" Link or Button
    Then "ForgotPasswordFlow|EnterUsername_header" page or message should be displayed
    When User enters "ForgotPasswordFlow|Username_field" field as "AUTO_Accountlocked@mas.com" value
    And User Clicks on "ForgotPasswordFlow|Submit_Button" Link or Button
    Then "ForgotPasswordFlow|AccountlockedError" page or message should be displayed