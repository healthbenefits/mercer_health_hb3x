@IBCG2Regression @Login_Flow_Regression 
Feature: Regression set-1 (16 testcases): Regression tests for MAS-IBCG2 login flows

  # Regression Test case - 1
  @IBCG2Regression @Login_test @InactiveUserLogin @testvb
  Scenario: To verify login error message when inactive user try to login
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_inactiveuser@mas.com" value
    And User enters "MASLoginPage|Password" field as "QAWORKpass1" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then  Verify "MASLoginPage|InactiveUserMessage" error message displayed is "There is an issue with your access to this account. Please contact your plan administrator for assistance."

  # Regression Test case - 2
  @IBCG2Regression @Login_test @Account_Locked_By_Mercer
  Scenario: To verify login error message when locked user (locked by Mercer) try to login
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_lockedbyMercer@mas.com" value
    And User enters "MASLoginPage|Password" field as "QAWORKpass1" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then  Verify "MASLoginPage|LockedWarning" error message displayed is "In order to protect your security, your account has been locked. Please call your plan's toll free number at 1-877-200-9105 for assistance."

  # Regression Test case - 3
  @IBCG2Regression @Login_test @Account_Locked_By_Client
  Scenario: To verify login error message when locked user (locked by Client) try to login
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_lockedbyclient@mas.com" value
    And User enters "MASLoginPage|Password" field as "QAWORKpass1" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then  Verify "MASLoginPage|LockedWarning" error message displayed is "In order to protect your security, your account has been locked. Please call your plan's toll free number at 1-877-200-9105 for assistance."

  # Regression Test case - 4
  @IBCG2Regression @Account_Locked_By_User @Login_test
  Scenario: To verify login error message when locked user (User requested) try to login
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_lockedbyuser@mas.com" value
    And User enters "MASLoginPage|Password" field as "QAWORKpass1" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then  Verify "MASLoginPage|LockedWarning" error message displayed is "In order to protect your security, your account has been locked. Please call your plan's toll free number at 1-877-200-9105 for assistance."

  # Regression Test case - 5
  @IBCG2Regression @Accountlockout @Login_test
  Scenario: To verify when IBCG2 user's account gets locked when user makes 5 invalid login attempts
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_Accountlocked@mas.com" value
    And User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then  Verify "MASLoginPage|InvalidCredentialMessage" error message displayed is "Either Username or password is incorrect."
    When User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then  Verify "MASLoginPage|InvalidCredentialMessage" error message displayed is "Either Username or password is incorrect."
    When User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then  Verify "MASLoginPage|Invalid_3rd_attempt" error message displayed is "The password is wrong. You have only 2 attempts to enter the correct password or your account will be locked."
    When User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then  Verify "MASLoginPage|Invalid_4th_attempt" error message displayed is "The password is wrong. You have only 1 attempt to enter the correct password or your account will be locked."
    When User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then  Verify "MASLoginPage|Account_locked_Message" error message displayed is "You have exceeded the maximum number of login attempts and your account is now locked for ten minutes. Once the ten minutes has passed we recommend you use the Forgot Password link below to update your password."
    When User enters "MASLoginPage|Password" field as "invalid" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then  Verify "MASLoginPage|Account_locked_Message" error message displayed is "You have exceeded the maximum number of login attempts and your account is now locked for ten minutes. Once the ten minutes has passed we recommend you use the Forgot Password link below to update your password."


  # Regression Test case - 6
  @IBCG2Regression @Login_Logout_MFA_Enabled
  Scenario: To verify IBCG2 user with MFA enabled is able to successfully login and logout using MAS
    Given  User opens MAS login page url "https://auth-qa.mercer.com/IBCG2/login"  for target application
    When User enters "MASLoginPage|Username" field as "auto_qalife1186@mas.com" value
    And User enters "MASLoginPage|Password" field as "Welcome1!" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When   User performs MFA challenge
    Then Verify SSO to target application - "IBCG2" happend successfully
    Then "IBCG2|HeaderTabs" page or message should be displayed
    When User Clicks on "IBCG2|Logout" Link or Button
    When User clicks on YES button on the logout popup alert box
    Then "MASLoginPage|LogoutMessage" page or message should be displayed

  # Regression Test case - 7
  @IBCG2Regression @Login_Logout_MFA_Disabled
  Scenario: To verify IBCG2 user with MFA disabled is able to successfully login and logout using MAS
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "QAWORK6267@mercer.com" value
    And User enters "MASLoginPage|Password" field as "Welcome1!" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then Verify SSO to target application - "IBCG2" happend successfully
    Then "IBCG2|HeaderTabs" page or message should be displayed
    When User Clicks on "IBCG2|Logout" Link or Button
    When User clicks on YES button on the logout popup alert box
    Then "MASLoginPage|LogoutMessage" page or message should be displayed

  # Regression Test case - 8
  @IBCG2Regression @Migrated_user_Login_UN_Contact_PW_update
  Scenario: To verify when IBCG2 - migrated user login via MAS for first time, user is asked to update Username,Contact Methods and Password as per MAS Enhance Security requirements
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_migrateduserlogintest1" value
    And User enters "MASLoginPage|Password" field as "test0001" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then "MAS|Enhance_Security" page or message should be displayed
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then "MAS|Update_Usename_warning" page or message should be displayed
    And "MAS|Update_Contact_Overview" page or message should be displayed
    When User enters "MAS|Username_field" field as "AUTO_migrateduserlogintest1@mas.com" value
    Then Verify one additional email address "nishant.kolte@mercer.com" is added in Contact method section
    When User Clicks on "MAS|Update_button" Link or Button
    Then "MAS|Contact_info_update_confirmation" page or message should be displayed
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then "MAS|Password_Update" page or message should be displayed
    When User enters "MAS|New_Password" field as "Test@001" value
    And User enters "MAS|Confirm_Password" field as "Test@001" value
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then Verify SSO to target application - "IBCG2" happend successfully
    Then "IBCG2|HeaderTabs" page or message should be displayed

  # Regression Test case - 9
  @IBCG2Regression @Migrated_user_Login_PW_update
  Scenario: To verify when IBCG2 - migrated user login via MAS for first time, user is asked to update Username,Contact Methods and Password as per MAS Enhance Security requirements
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_migrateduserlogintest2" value
    And User enters "MASLoginPage|Password" field as "test0001" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then "MAS|Enhance_Security" page or message should be displayed
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then "MAS|Update_Usename_warning" page or message should be displayed
    And "MAS|Update_Contacts_warning" page or message should be displayed
    When User enters "MAS|Username_field" field as "AUTO_migrateduserlogintest2@mas.com" value
    Then Verify one additional email address "nishant.kolte@mercer.com" is added in Contact method section
    When User Clicks on "MAS|Update_button" Link or Button
    Then "MAS|Contact_info_update_confirmation" page or message should be displayed
    When User Clicks on "MAS|Cancel_button" Link or Button
    When User clicks on YES button on the cancel transaction popup alert box
    Then "MASLoginPage|Reqest_Canceled_Message" page or message should be displayed
    When User enters "MASLoginPage|Username" field as "AUTO_migrateduserlogintest2@mas.com" value
    And User enters "MASLoginPage|Password" field as "test0001" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then "MAS|Enhance_Security_PW_update" page or message should be displayed
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then "MAS|Password_Update" page or message should be displayed
    When User enters "MAS|New_Password" field as "Test@001" value
    And User enters "MAS|Confirm_Password" field as "Test@001" value
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then Verify SSO to target application - "IBCG2" happend successfully
    Then "IBCG2|HeaderTabs" page or message should be displayed

  #  Regression Test case - 10
  @IBCG2Regression @Migrated_user_Login_UN_Contact_update
  Scenario: To verify when IBCG2 - migrated user login via MAS for first time, user is asked to update Username and Contact Methods as per MAS Enhance Security requirements
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_migrateduserlogintest3" value
    And User enters "MASLoginPage|Password" field as "Test@001" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    Then "MAS|Enhance_Security_UN_Contact_update" page or message should be displayed
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then "MAS|Update_Usename_warning" page or message should be displayed
    And "MAS|Update_Contact_Overview" page or message should be displayed
    When User enters "MAS|Username_field" field as "AUTO_migrateduserlogintest3@mas.com" value
    Then Verify one additional email address "nishant.kolte@mercer.com" is added in Contact method section
    When User Clicks on "MAS|Update_button" Link or Button
    Then "MAS|Contact_info_update_confirmation" page or message should be displayed
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then Verify SSO to target application - "IBCG2" happend successfully
    Then "IBCG2|HeaderTabs" page or message should be displayed

  # Regression Test case - 11
  @IBCG2Regression @Registered_user_PW_update_required
  Scenario: To verify when IBCG2 - registered user login via MAS, user is asked to update password as per MAS Enhance Security requirements
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_PasswordUpdatetest@qawork.com" value
    And User enters "MASLoginPage|Password" field as "Welcome1!" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then "MAS|Enhance_Security_PW_update" page or message should be displayed
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then "MAS|Password_Update" page or message should be displayed
    When User enters "MAS|New_Password" field as "Test@001" value
    And User enters "MAS|Confirm_Password" field as "Test@001" value
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then Verify SSO to target application - "IBCG2" happend successfully
    Then "IBCG2|HeaderTabs" page or message should be displayed

  # Regression Test case - 12
  @IBCG2Regression @Registered_user_ContactMethod_update_required @R11
  Scenario: To verify when IBCG2 - registered user login via MAS, user is asked to review/update contact method as per MAS Enhance Security requirements
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_ContactinfoUpdatetest@qawork.com" value
    And User enters "MASLoginPage|Password" field as "Test@001" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then "MAS|Enhance_Security_UN_Contact_update" page or message should be displayed
    When User Clicks on "MAS|Continue_Button" Link or Button
    And "MAS|Contact_Method_Review_Warning" page or message should be displayed
    And "MAS|Update_Contact_Overview" page or message should be displayed
    When User enters "MAS|Username_field" field as "AUTO_ContactinfoUpdatetest@qawork.com" value
    Then Verify one additional email address "nishant.kolte@mercer.com" is added in Contact method section
    When User Clicks on "MAS|Update_button" Link or Button
    When User Clicks on "MAS|Continue_Button" Link or Button
    Then Verify SSO to target application - "IBCG2" happend successfully
    Then "IBCG2|HeaderTabs" page or message should be displayed

# Regression Test case - 13
  @IBCG2Regression @AltLogin-1 @AltLogin
  Scenario: To verify IBCG2 user with MFA disabled is able to successfully login using MAS alternate login page
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/altlogin"  for target application
    When User enters "MASaltLoginPage|Username" field as "QAWORK6267@mercer.com" value
    And User enters "MASaltLoginPage|Password" field as "Welcome1!" value
    And User Clicks on "MASaltLoginPage|Login" Link or Button
    Then Verify SSO to target application - "IBCG2" happend successfully
    Then "IBCG2|HeaderTabs" page or message should be displayed

# Regression Test case - 14
  @IBCG2Regression @AltLogin-2 @AltLogin
  Scenario: To verify login error code when inactive user try to login via altlogin
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/altlogin"  for target application
    When User enters "MASaltLoginPage|Username" field as "AUTO_inactiveuser@mas.com" value
    And User enters "MASaltLoginPage|Password" field as "invalid" value
    And User Clicks on "MASaltLoginPage|Login" Link or Button
    Then "MASaltLoginPage|Auth_failed_Message" page or message should be displayed
    Then "MASaltLoginPage|InactiveUserMessage" page or message should be displayed

# Regression Test case - 15
  @IBCG2Regression @AltLogin-3 @AltLogin
  Scenario: To verify login error code when invalid credentials are entered on altlogin
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/altlogin"  for target application
    When User enters "MASaltLoginPage|Username" field as "AUTO_accountlocked2@mas.com" value
    And User enters "MASaltLoginPage|Password" field as "invalid" value
    And User Clicks on "MASaltLoginPage|Login" Link or Button
    Then "MASaltLoginPage|Auth_failed_Message" page or message should be displayed
    Then "MASaltLoginPage|InvalidCredentialMessage" page or message should be displayed
    When User enters "MASaltLoginPage|Username" field as "AUTO_accountlocked2@mas.com" value
    And User enters "MASaltLoginPage|Password" field as "invalid" value
    And User Clicks on "MASaltLoginPage|Login" Link or Button
    Then "MASaltLoginPage|Auth_failed_Message" page or message should be displayed
    Then "MASaltLoginPage|InvalidCredentialMessage" page or message should be displayed
    When User enters "MASaltLoginPage|Username" field as "AUTO_accountlocked2@mas.com" value
    And User enters "MASaltLoginPage|Password" field as "invalid" value
    And User Clicks on "MASaltLoginPage|Login" Link or Button
    Then "MASaltLoginPage|Auth_failed_Message" page or message should be displayed
    Then "MASaltLoginPage|Invalid_3rd_attempt" page or message should be displayed
    When User enters "MASaltLoginPage|Username" field as "AUTO_accountlocked2@mas.com" value
    And User enters "MASaltLoginPage|Password" field as "invalid" value
    And User Clicks on "MASaltLoginPage|Login" Link or Button
    Then "MASaltLoginPage|Auth_failed_Message" page or message should be displayed
    Then "MASaltLoginPage|Invalid_4th_attempt" page or message should be displayed
    When User enters "MASaltLoginPage|Username" field as "AUTO_accountlocked2@mas.com" value
    And User enters "MASaltLoginPage|Password" field as "invalid" value
    And User Clicks on "MASaltLoginPage|Login" Link or Button
    Then "MASaltLoginPage|Auth_failed_Message" page or message should be displayed
    Then "MASaltLoginPage|Account_locked_Message" page or message should be displayed

# Regression Test case - 16
  @IBCG2Regression @AltLogin-4 @AltLogin
  Scenario: To verify login error code when locked user(locked by mercer) try to login via altlogin
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/altlogin"  for target application
    When User enters "MASaltLoginPage|Username" field as "AUTO_lockedbymercer@mas.com" value
    And User enters "MASaltLoginPage|Password" field as "invalid" value
    And User Clicks on "MASaltLoginPage|Login" Link or Button
    Then "MASaltLoginPage|Auth_failed_Message" page or message should be displayed
    Then "MASaltLoginPage|Locked_user" page or message should be displayed