@MASSmokePROD
Feature: Smoke suit for MAS login flows with respect to target apps in PROD region

  @MASSmokePROD @IBCG2PROD
  Scenario: To verify IBCG2 user with MFA enabled is able to successfully login, access security settings and logout using MAS
    Given  User opens MAS login page url "https://auth.mercer.com/SLSDM/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_SLSDM1002@mas.com" value
    And User enters "MASLoginPage|Password" field as "SLSDMpass1" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When   User performs MFA challenge
    Then Verify SSO to target application - "IBCG2" happend successfully
    Then "IBCG2|HeaderTabs" page or message should be displayed
    When User Clicks on "IBCG2|LoginManagement" Link or Button
    Then Verify that MAS Security Settings page loads successfully in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "SLSDMpass1" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_Information_header" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CloseButton" Link or Button in new browser window
    Then MAS security setting page is closed successfully
    When User Clicks on "IBCG2|Logout" Link or Button
    When User clicks on YES button on the logout popup alert box
    Then "MASLoginPage|LogoutMessage" page or message should be displayed


  @MASSmokePROD @MBCPROD
  Scenario: To verify MBC user with MFA enabled is able to successfully login, access security settings and logout using MAS
    Given User opens MAS login page url "https://auth.mercer.com/MBCTRA/login"  for target application
    When User enters "MASLoginPage|Username" field as "MBCTRA0030@mas.com" value
    And User enters "MASLoginPage|Password" field as "Welcome1!" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then Verify SSO to target application - "MBC" happend successfully
    Then "MBC|Dashboard" page or message should be displayed
    When User Clicks on "MBC|SecSettingslink" Link or Button
    And User Clicks on "MBC|Clickherelink" Link or Button
    Then Verify that MAS Security Settings page loads successfully in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "Welcome1!" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_Information_header" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CloseButton" Link or Button in new browser window
    Then MAS security setting page is closed successfully for mmx365P site is displayed
    When User Clicks on "MBC|Logout" Link or Button
    Then "MASLoginPage|LogoutMessage" page or message should be displayed
