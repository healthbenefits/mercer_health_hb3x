@MASSmoke
Feature: Smoke suit for MAS login flows with respect to target apps-IBCG2, MBC, MMX365+(MBC),MMX365+(Consultant Portal)

  @IBCG2 @MASSmoke
  Scenario: To verify IBCG2 user with MFA enabled is able to successfully login, access security settings and logout using MAS
    Given  User opens MAS login page url "https://auth-qa.mercer.com/QAWORK/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_QAWORK0004@mas.com" value
    And User enters "MASLoginPage|Password" field as "QAWORKpass1" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When   User performs MFA challenge
    Then Verify SSO to target application - "IBCG2" happend successfully
    Then "IBCG2|HeaderTabs" page or message should be displayed
    When User Clicks on "IBCG2|LoginManagement" Link or Button
    Then Verify that MAS Security Settings page loads successfully in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "QAWORKpass1" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_Information_header" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CloseButton" Link or Button in new browser window
    Then MAS security setting page is closed successfully
    When User Clicks on "IBCG2|Logout" Link or Button
    When User clicks on YES button on the logout popup alert box
    Then "MASLoginPage|LogoutMessage" page or message should be displayed


  @MBC-QA @MASSmoke
  Scenario: To verify MBCQA5 (MBC) user with MFA enabled is able to successfully login, access security settings and logout using MAS
    Given User opens MAS login page url "https://auth-qa.mercer.com/MBCQA5/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_QAtest_HBuser@MBCQA5.com" value
    And User enters "MASLoginPage|Password" field as "Welcome1!" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then Verify SSO to target application - "MBC" happend successfully
    Then "MBC|Dashboard" page or message should be displayed
    When User Clicks on "MBC|SecSettingslink" Link or Button
    And User Clicks on "MBC|Clickherelink" Link or Button
    Then Verify that MAS Security Settings page loads successfully in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "Welcome1!" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_Information_header" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CloseButton" Link or Button in new browser window
    Then MAS security setting page is closed successfully for mmx365P site is displayed
    When User Clicks on "MBC|Logout" Link or Button
    Then "MASLoginPage|LogoutMessage" page or message should be displayed

  @MMX365P_MBC @MASSmoke
  Scenario: To verify mmx365+ user with MFA enabled is able to successfully login, access security settings and logout using MAS
    Given User opens MAS login page url "https://auth-qa.mercer.com/MMX365PLUS/Member/login"  for target application
    When User enters "MASLoginPage|Username" field as "AUTO_QAtestuser@mmx365P.com" value
    And User enters "MASLoginPage|Password" field as "Test@001" value
    And User Clicks on "MASLoginPage|Login" Link or Button
    And User selects email address from the "MFApage|SelectContact" list
    And User Clicks on "MFApage|ContinueButton" Link or Button
    When User performs MFA challenge
    Then Verify SSO to target application - "MMX365P_MBC" happend successfully
    Then "mmx365P|Dashboard" page or message should be displayed
    When User Clicks on "mmx365P|SecSettingslink" Link or Button
    And User Clicks on "mmx365P|Clickherelink" Link or Button
    Then Verify that MAS Security Settings page loads successfully in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "Test@001" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_Information_header" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CloseButton" Link or Button in new browser window
    Then MAS security setting page is closed successfully for mmx365P site is displayed
    When User Clicks on "mmx365P|Logout" Link or Button
    Then "MASLoginPage|LogoutMessage" page or message should be displayed


  @MMX365P_CP @MASSmoke
  Scenario: To verify MMX365+ Consultant Portal user with MFA enabled is able to successfully login, access security settings and logout using MAS
    Given User opens MAS login page url "https://auth-qa.mercer.com/MMX365Plus/Con/login"  for target application
    When User enters "MASLoginPage|Username" field as "AR4.arunrajendiran@gisqa.mercer.com" value
    And User enters "MASLoginPage|Password" field as "Mercer08" value
    And User Clicks on "MASLoginPage|ContinueButton" Link or Button
    Then Verify SSO to target application - "MMX365P_CP" happend successfully
    Then "mmx365P|CP_Dashboard" page or message should be displayed
    When user does mousehover on user profile icon in top right corner
    And User Clicks on "mmx365P|CP_secsettings" Link or Button
    Then Verify that MAS Security Settings page loads successfully in new browser window
    When User enters "MASSecuritySettingsPage|EnterPassword" field as "Mercer08" value in new browser window
    When User Clicks on "MASSecuritySettingsPage|ContinueButton" Link or Button in new browser window
    Then "MASSecuritySettingsPage|Contact_Information_header" page or message should be displayed in new browser window
    When User Clicks on "MASSecuritySettingsPage|CloseButton" Link or Button in new browser window
    Then MAS security setting page is closed successfully for mmx365_CP site is displayed
    When user does mousehover on user profile icon in top right corner
    And User Clicks on "mmx365P|CP_logout" Link or Button
    Then "MASLoginPage|LogoutMessage" page or message should be displayed


