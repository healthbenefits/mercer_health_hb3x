var data = require('../test_data/MAS_IBCG2_Regression_TestData.js');
module.exports = {
    sections: {
        MASLoginPage: {
            selector: 'body',
            elements: {
                labelLoginTextMessage: {locateStrategy: 'xpath', selector: '//p[contains(text(),"Log in to your existing account.")]'},
                Username: {locateStrategy: 'xpath', selector: '//*[@id="username"]'},
                Password: {locateStrategy: 'xpath', selector: '//*[@id="password"]'},
                Login: {locateStrategy: 'xpath', selector: '//button[text()="Login"]'},
                InvalidCredentialMessage: {locateStrategy: 'xpath', selector: '//div[text()="Either Username or password is incorrect."]'},
                InactiveUserMessage: {locateStrategy: 'xpath', selector: '//div[text()=\'There is an issue with your access to this account. Please contact your plan administrator for assistance.\']'},
                LockedWarning: {locateStrategy: 'xpath', selector: '//div[contains(text(),"In order to protect your security, your account has been locked. Please call your plan\'s toll free number at 1-877-200-9105 for assistance.")]' },
                LogoutMessage: {locateStrategy: 'xpath', selector: '//div[contains(text(),"You have now logged out. We recommend that you close your web browser to protect your personal information.")]'},
                Reqest_Canceled_Message: {locateStrategy: 'xpath', selector: '//div[text()=\'Your request has been canceled\']'},
                Account_locked_Message: {locateStrategy: 'xpath', selector: '//div[text()=\'You have exceeded the maximum number of login attempts and your account is now locked for ten minutes. Once the ten minutes has passed we recommend you use the Forgot Password link below to update your password. \']'},
                Invalid_3rd_attempt: {locateStrategy: 'xpath', selector: '//div[contains(text(),"The password is wrong. You have only 2 attempts to enter the correct password or your account will be locked.")]'},
                Invalid_4th_attempt: {locateStrategy: 'xpath', selector: '//div[contains(text(),"The password is wrong. You have only 1 attempt to enter the correct password or your account will be locked.")]'},
                ContinueButton: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Continue")]'},
                ForgotUsernameLink: {locateStrategy: 'xpath', selector: '//a[text()="Username"]'},
                ForgotPasswordLink: {locateStrategy: 'xpath', selector: '//a[text()="Password"]'},
                GetStarted_Button: {locateStrategy: 'xpath', selector: '//a[text()="Get Started"]'},
            },
        },

        MASaltLoginPage: {
            selector: 'body',
            elements: {
                Username: {locateStrategy: 'xpath', selector: '//*[@name="userId"]'},
                Password: {locateStrategy: 'xpath', selector: '//*[@name="password"]'},
                Login: {locateStrategy: 'xpath', selector: '//*[@type="submit"]'},
                Auth_failed_Message: {locateStrategy: 'xpath', selector: '//span[text()="authentication failed"]'},
                InvalidCredentialMessage: {locateStrategy: 'xpath', selector: '//span[text()="invalid_credentials"]'},
                InactiveUserMessage: {locateStrategy: 'xpath', selector: '//span[text()="account_inactive"]'},
                Locked_user: {locateStrategy: 'xpath', selector: '//span[text()="locked_by_mercer"]' },
                Account_locked_Message: {locateStrategy: 'xpath', selector: '//span[text()="locked_by_invalid_attempts"]'},
                Invalid_3rd_attempt: {locateStrategy: 'xpath', selector: '//span[text()="invalid_attempts_left"]/following::span[text()="2"]'},
                Invalid_4th_attempt: {locateStrategy: 'xpath', selector: '//span[text()="invalid_attempts_left"]/following::span[text()="1"]'},

                
            },
        },
        MFApage: {
            selector: 'body',
            elements: {
                
                SelectContactPageOverview: {locateStrategy: 'xpath', selector: "//p[text()='Select a way to receive your verification code. If you choose to receive your code via e-mail, it could take up to 5 minutes to arrive in your inbox depending on your e-mail provider.']"},
                SelectContact: {locateStrategy: 'xpath', selector: '//label[contains(text(),"Email ni*****@mercer.com")]'},
                SelectEmail: {locateStrategy: 'xpath', selector: '//label[contains(text(),"Email")]'},
                SelectPhone: {locateStrategy: 'xpath', selector: '//label[contains(text(),"Text")]'},
                ContinueButton: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Continue")]'},
                EnterVerificationCode: {locateStrategy: 'xpath', selector: '//input[@id="verificationCode"]'},
            },
        },
        MAS: {
            selector: 'body',
            elements: {
                Enhance_Security: {locateStrategy: 'xpath', selector: '//div[contains(text(),"       We have recently enhanced the security on this website with Multifactor Authentication to better protect your account. We now require your Username to be an email address and we have new password requirements to keep your account secure. We also require that you provide a secondary contact method which can be either another email address or a phone number. In the future you will be able to choose any one of these contact methods to receive the verification code. Plese click Continue to review your settings and make the required changes.      ")]'},
                Enhance_Security_PW_update: {locateStrategy: 'xpath', selector: '//div[contains(text(),"       We have recently enhanced the security on this website to better protect your account. As part of these changes we have new password requirements to keep your account secure. Plese click Continue to review your settings and make the required changes.      ")]'},
                Enhance_Security_UN_Contact_update: {locateStrategy: 'xpath', selector: '//div[contains(text(),"       We have recently enhanced the security on this website with Multifactor Authentication to better protect your account. We now require your Username to be an email address. We also require that you provide a secondary contact method which can be either another email address or a phone number. In the future you will be able to choose any one of these contact methods to receive the verification code. Plese click Continue to review your settings and make the required changes.      ")]'},
                Update_Usename_warning: {locateStrategy: 'xpath', selector: '//div[@class="mas-message mas-message-warning"]'},
                Update_Contacts_warning: {locateStrategy: 'xpath', selector: '//h2[text()=\'Contact Methods\']/following::div[@class=\'mas-message mas-message-warning\']'},
                Contact_Method_Review_Warning: {locateStrategy: 'xpath', selector: '//h2[text()=\'Contact Methods\']/following::div[@class=\'mas-message mas-message-warning\']'},
                Update_Contact_Overview: {locateStrategy: 'xpath', selector: '//p[text()="We require two contact methods that will be used for enhanced security purposes. The first contact method is your Username and the second contact method can be either another email address or a phone number. We recommend that one of your contact methods be a personal email or phone number so that you can access your account when you are away from work."]'},
                Continue_Button: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Continue")]'},
                Username_field: {locateStrategy: 'xpath', selector: '//input[@id="userId"]'},
                trash_icon: {locateStrategy: 'xpath', selector: '//span[@class="mas-icon mas-icon-remove"]'},
                Add_contact_method_link: {locateStrategy: 'xpath', selector: '//a[text()="Add more contact methods"]'},
                Email_field: {locateStrategy: 'xpath', selector: '//input[@id="email"]'},
                Add_Contact_button: {locateStrategy: 'xpath', selector: '//button[text()="Add contact"]'},
                Update_button: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Update")]'},
                Cancel_button: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Cancel")]'},
                additonal_contactmethod: {locateStrategy: 'xpath', selector: '//div[contains(text(),"nishant.kolte@mercer.com")]'},
                Contact_info_update_confirmation: {locateStrategy: 'xpath', selector: '//div[@class=\'mas-message mas-message-success\'] '},
                Password_Update: {locateStrategy: 'xpath', selector: '//h2[text()=\'Update Password\']'},
                New_Password: {locateStrategy: 'xpath', selector: '//a[@class=\'mas-link mas-tooltip-link\'] /following::input[1]'},
                Confirm_Password: {locateStrategy: 'xpath', selector: '//a[@class=\'mas-link mas-tooltip-link\'] /following::input[2]'},
                email_trash_icon: {locateStrategy: 'xpath', selector: '//li/div[contains(text(),"@")]/span[@class="mas-icon mas-icon-remove"]'},
            },
        },
        ForgotUsernameFlow: {
            selector: 'body',
            elements: {
                ForgotUsername_header: {locateStrategy: 'xpath', selector: '//h2[text()="Forgot Username"]'},
                Continue_Button: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Continue")]'},
                Last4SSN_field: {locateStrategy: 'xpath', selector: '//input[@id="lastFourSsn"]'},
                LastName_field: {locateStrategy: 'xpath', selector: '//input[@id="lastname"]'},
                DOB_field: {locateStrategy: 'xpath', selector: '//input[@id="dob"]'},
                PostalCode_field: {locateStrategy: 'xpath', selector: '//input[@id="postalCodeUS"]'},
                Next_Button: {locateStrategy: 'xpath', selector: '//button[text()="Next"]'},
                UserNotFoundError: {locateStrategy: 'xpath', selector: '//div[text()="We are not able to find you by the information you provided or the information is already associated to an account."]'},
                UserNotRegisteredError: {locateStrategy: 'xpath', selector: '//div[text()="You are not registered in application yet."]'},
                YourUsernameIs_header: {locateStrategy: 'xpath', selector: '//h2[text()="Your Username is:"]'},
                RecoveredUsername_field: {locateStrategy: 'xpath', selector: '//div[@class="mas-data-value text-center"]'},
                LoginToYourAccount_Button: {locateStrategy: 'xpath', selector: '//a[text()="Log in to Your Account Now"]'},
                NoContactMethodPage: {locateStrategy: 'xpath', selector: '//div[text()="                 Looks like there is no contact information on your file             "]'}, 
                ReturnToLoginPage_Button: {locateStrategy: 'xpath', selector: '//button[text()=" Return to login page "]'},
                InvalidSSNformat_Error: {locateStrategy: 'xpath', selector: '//small[text()="You have entered an invalid Social Security number; please enter the last 4 digits of your Social Security number."]'},
                InvalidDOB_Error: {locateStrategy: 'xpath', selector: '//small[text()="Please enter a valid date of birth (MM/DD/YYYY)."]'},               
                InvalidPostalCode_Error: {locateStrategy: 'xpath', selector: '//small[text()="       Postal code format is incorrect. For country United States, please enter your 5-digit postal code.     "]'},
            },
        },
        ForgotPasswordFlow: {
            selector: 'body',
            elements: {
                EnterUsername_header: {locateStrategy: 'xpath', selector: '//h2[text()="Provide the following information"]'},
                Submit_Button: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Submit")]'},
                Username_field: {locateStrategy: 'xpath', selector: '//input[@type="email"]'},        
                UsernameNotFoundError: {locateStrategy: 'xpath', selector: '//div[contains(text(),"Unfortunately, we were unable to identify you based on the information you entered")]'},
                CreatePassword_header: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"Create a new password")]'},
                MaskedUsername_field: {locateStrategy: 'xpath', selector: '//div[@class="mas-data-value"]'},
                NewPassword_field: {locateStrategy: 'xpath', selector: '//input[@type="password"][1]'},
                NoContactMethodPage: {locateStrategy: 'xpath', selector: '//div[text()="                 Looks like there is no contact information on your file             "]'}, 
                ConfirmPassword_field: {locateStrategy: 'xpath', selector: '//label[text()="Confirm Password"]/following::input[@type="password"]'},
                Confirmation_message: {locateStrategy: 'xpath', selector: '//div[text()="                 Congratulations! You have successfully modified your password.             "]'},
                Password_policy_error: {locateStrategy: 'xpath', selector: '//div[contains(text(),\'Password does not meet password policy\')]'},          
                LoginToYourAccount_Button: {locateStrategy: 'xpath', selector: '//a[text()="Log in to Your Account Now"]'},              
                InactiveuserError: {locateStrategy: 'xpath', selector: '//div[text()="There is an issue with your access to this account. Please contact your plan administrator for assistance."]'},          
                LockedByMercerError: {locateStrategy: 'xpath', selector: '//div[contains(text(),"In order to protect your security, your account has been locked. Please call your plan\'s toll free number at 1-877-200-9105 for assistance.")]'},          
                AccountlockedError: {locateStrategy: 'xpath', selector: '//div[contains(text(),"You have exceeded the maximum number of login attempts and your account is now locked for ten minutes.")]'},          
                NoAccessToEmailLink: {locateStrategy: 'xpath', selector: '//a[text()="I don\'t have access to these anymore. Help me!                 "]'},          
                NoAccessToEmailLink_Tooltip: {locateStrategy: 'xpath', selector: '//strong[text()="We are here for you! Please call us at 1-877-200-9105 to get help with your account setup process."]'},          
            },
        },
        RegistrationFlow: {
            selector: 'body',
            elements: {
                Registration_header: {locateStrategy: 'xpath', selector: '//h2[text()="Register Your Account"]'},
                Continue_Button: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Continue")]'},
                Last4SSN_field: {locateStrategy: 'xpath', selector: '//input[@id="lastFourSsn"]'},
                LastName_field: {locateStrategy: 'xpath', selector: '//input[@id="lastname"]'},
                DOB_field: {locateStrategy: 'xpath', selector: '//input[@id="dob"]'},
                PostalCode_field: {locateStrategy: 'xpath', selector: '//input[@id="postalCodeUS"]'},
                InvalidSSNformat_Error: {locateStrategy: 'xpath', selector: '//small[text()="You have entered an invalid Social Security number; please enter the last 4 digits of your Social Security number."]'},
                InvalidDOB_Error: {locateStrategy: 'xpath', selector: '//small[text()="Please enter a valid date of birth (MM/DD/YYYY)."]'},               
                InvalidPostalCode_Error: {locateStrategy: 'xpath', selector: '//small[text()="       Postal code format is incorrect. For country United States, please enter your 5-digit postal code.     "]'},
                Next_Button: {locateStrategy: 'xpath', selector: '//button[text()="Next"]'},
                UserNotFoundError: {locateStrategy: 'xpath', selector: '//div[text()="We are not able to find you by the information you provided or the information is already associated to an account."]'},
                NoWebAccess_Error: {locateStrategy: 'xpath', selector: '//div[text()="We have found your account by the information you provided, but you are not eligible for this website"]'},
                MultipleUsersFound_Error: {locateStrategy: 'xpath', selector: '//div[text()="Something went wrong. Please try again."]'},
                CreateUsername_label: {locateStrategy: 'xpath', selector: '//label[contains(text(),"Enter an email address to receive the temporary verification code and use as your Username")]'},
                CreateUsername_TextField: {locateStrategy: 'xpath', selector: '//input[@type="email"]'},
                AlreadyRegistered_Popup: {locateStrategy: 'xpath', selector: '//h2[text()="Already Registered!"]'},
                PopupError_text: {locateStrategy: 'xpath', selector: '//div[contains(text(),\'The email address you specified is already in use. If you own this email address and used it to register on this website already, proceed with login by clicking on "Login" button. Click "Cancel" to specify a different email address or contact us at 1-877-200-9105 for assistance\')]'},
                Cancel_Button: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Cancel")]'},
                SelectUsername_label: {locateStrategy: 'xpath', selector: '//label[text()="Select an email address for your Username.         "]'},
                EnterUsername_checkbox: {locateStrategy: 'xpath', selector: '//div[@class="mas-chooser-cell mas-chooser-cell-select"]'},
                EnterNewUsername_Field: {locateStrategy: 'xpath', selector: '//input[@type="text"]'},
                CompleteRegistration_Page: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"You\'re almost there!")]'},
                Email_On_File_Field: {locateStrategy: 'xpath', selector: '//div[text()="nishant.kolte@mercer.com"]'},
                Phone_On_File_Field: {locateStrategy: 'xpath', selector: '//div[text()="+91 (878) 874-8033"]'},
                Email_radiobutton: {locateStrategy: 'xpath', selector: '//label[@for="radio_email"]'},
                Phone_radiobutton: {locateStrategy: 'xpath', selector: '//label[@for="radio_phone"]'},
                Same_Username_and_Email_Error: {locateStrategy: 'xpath', selector: '//small[text()="Your second contact method cannot be the same as your Username."]'},
                InvalidEmailFormat_Error: {locateStrategy: 'xpath', selector: '//small[text()="                   Email must be specified in the following format emailname@domainname.extension and maximum length of email equal to 100 characters                 "]'},
                Enter_Email_Or_Phone_field: {locateStrategy: 'xpath', selector: '//input[@type="text"]'},
                NewPassword_field: {locateStrategy: 'xpath', selector: '//input[@type="password"][1]'},
                ConfirmPassword_field: {locateStrategy: 'xpath', selector: '//label[text()="Confirm Password"]/following::input[@type="password"]'},
                CompleteRegistration_Button: {locateStrategy: 'xpath', selector: '//button[text()="Complete Registration"]'},
                Confirmation_message: {locateStrategy: 'xpath', selector: '//div[text()="             Congratulations! Your registration is complete.         "]'},
                LoginToYourAccount_Button: {locateStrategy: 'xpath', selector: '//a[text()="Log in to Your Account Now"]'},
                AlreadyRegistered_Popup: {locateStrategy: 'xpath', selector: '//h2[text()="Already Registered!"]'},
                
            },
        },
        MASSecuritySettingsPage:{
            selector: 'body',
            elements: {
                EnterPassword: {locateStrategy: 'xpath', selector: '//input[@id="password"]'},
                ContinueButton: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Continue")]'},
                CloseButton: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Close")]'},
                Contact_Information_header: {locateStrategy: 'xpath', selector: '//h2[text()="Contact Information"]'},
                CancelButton: {locateStrategy: 'xpath', selector: '//a[text()=\'Cancel\']'},

                EnterUsername: {locateStrategy: 'xpath', selector: '//input[@id=\'userId\']'},
                UpdatePasswordLink: {locateStrategy: 'xpath', selector: '//a[text()=\'Update Password\']'},
                AddContactLink: {locateStrategy: 'xpath', selector: '//a[text()=\'Add more contact methods\']'},

                email_delete_icon: {locateStrategy: 'xpath', selector: '//div[contains(text(),"test@test.com")]//following::span[@class="mas-icon mas-icon-remove"]'},
                phone_delete_icon: {locateStrategy: 'xpath', selector: '//li[contains(text(),"(111) 111-1111")]//following::span[@class="mas-icon mas-icon-remove"]'},

                SelectPhone: {locateStrategy: 'xpath', selector: '//label[@for="radio_phone"]'},
                Enteremail: {locateStrategy: 'xpath', selector: '//input[@id=\'email\']'},
                Enterphone: {locateStrategy: 'xpath', selector: '//input[@id=\'phone\']'},
                Email_UsedAsUsername_Error: {locateStrategy: 'xpath', selector: '//div[text()="Your second contact method cannot be the same as your Username."]'},
                Add_more_Contact_header: {locateStrategy: 'xpath', selector: '//h3[text()=\'Add more contact methods\']'},
                AddContactButton: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Add contact")]'},
                UpdateButton: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Update")]'},
                Contact_info_update_confirmation: {locateStrategy: 'xpath', selector: '//div[@class=\'mas-message mas-message-success\'] '},
                Username_already_exist_error: {locateStrategy: 'xpath', selector: '//div[text()="Username already exists"]'},
                Username_invalid_format_error: {locateStrategy: 'xpath', selector: '//small[contains(text(),"Email must be specified in the following format emailname@domainname.extension and maximum length of email equal to 100 characters")]'},
                Update_Contact_Overview: {locateStrategy: 'xpath', selector: '//p[text()="We require two contact methods that will be used for enhanced security purposes. The first contact method is your Username and the second contact method can be either another email address or a phone number. We recommend that one of your contact methods be a personal email or phone number so that you can access your account when you are away from work."]'},
                email_invalid_format_error: {locateStrategy: 'xpath', selector: '//small[contains(text(),"Email must be specified in the following format emailname@domainname.extension and maximum length of email equal to 100 characters")]'},
                email_already_exist_error: {locateStrategy: 'xpath', selector: '//div[text()=\'Email already added. Please enter another email\']'},
                phone_already_exist_error: {locateStrategy: 'xpath', selector: '//div[text()=\'Phone already added. Please enter another phone\']'},
                max_3_email_error: {locateStrategy: 'xpath', selector: '//div[text()=\'You can add maximum 3 contact methods of each type\']'},
                max_3_phone_error: {locateStrategy: 'xpath', selector: '//div[text()=\'You can add maximum 3 contact methods of each type\']'},
                blank_field_error: {locateStrategy: 'xpath', selector: '//small[text()=\'This field is required\']'},
                phone_1: {locateStrategy: 'xpath', selector: '//li[contains(text(),"(111) 111-1111")]/span[@class="mas-icon mas-icon-remove"][1]'},
                phone_2: {locateStrategy: 'xpath', selector: '//li[contains(text(),"+11 (111) 111-1111")]/span[@class=\'mas-icon mas-icon-remove\'][1]'},
                phone_3: {locateStrategy: 'xpath', selector: '//li[contains(text(),"+111 (111) 111-1111")]/span[@class=\'mas-icon mas-icon-remove\'][1]'},
                email_1: {locateStrategy: 'xpath', selector: '//div[contains(text(),"nishant.kolte@mercer.com")]/span[@class="mas-icon mas-icon-remove"][1]'},
                email_2: {locateStrategy: 'xpath', selector: '//div[contains(text(),"test@test.com")]/span[@class=\'mas-icon mas-icon-remove\'][1]'},
                email_3: {locateStrategy: 'xpath', selector: '//div[contains(text(),"test@test1.com")]/span[@class=\'mas-icon mas-icon-remove\'][1]'},
                email_1_disabled: {locateStrategy: 'xpath', selector: '//div[contains(text(),"nishant.kolte@mercer.com")]/span[@class=\'mas-icon mas-icon-remove disabled\'][1]'},
                phone_1_disabled: {locateStrategy: 'xpath', selector: '//li[contains(text(),"(111) 111-1111")]/span[@class=\'mas-icon mas-icon-remove disabled\'][1]'},

                Change_Password_header: {locateStrategy: 'xpath', selector: '//h2[text()="Change Password"]'},
                Create_Password_header: {locateStrategy: 'xpath', selector: '//h2[text()="Create Your Password"]'},
                Old_Password: {locateStrategy: 'xpath', selector: '//label[text()=\'Enter Your Current Password   \']/following::input[@type=\'password\'][1]'},
                New_Password: {locateStrategy: 'xpath', selector: '//label[text()=\'     Password     \']/following::input[@type=\'password\'][1]'},
                Confirm_Password: {locateStrategy: 'xpath', selector: '//label[text()=\'Confirm Password\']/following::input[@type=\'password\']'},
                Password_policy_error: {locateStrategy: 'xpath', selector: '//div[contains(text(),\'Password does not meet password policy\')]'},
                Password_rule_error: {locateStrategy: 'xpath', selector: '//small[text()=\'The password must match password rules\']'},
                Password_mismatch_error: {locateStrategy: 'xpath', selector: '//small[text()=\'Your passwords do not match. Please try again.\']'},
                Same_Old_New_Password_error: {locateStrategy: 'xpath', selector: '//small[text()=\'Old password and new password must be different\']'},
                Incorrect_Password_error: {locateStrategy: 'xpath', selector: '//div[text()=\'Your password is incorrect\']'},
                Submit_button: {locateStrategy: 'xpath', selector: '//button[text()=\'Submit\']'},
                Cancel_button: {locateStrategy: 'xpath', selector: '//a[text()=\'Cancel\']'},
                Back_button: {locateStrategy: 'xpath', selector: '//a[contains(text(),\'Back\')]'},
                Close_button: {locateStrategy: 'xpath', selector: '//a[contains(text(),\'Close\')]'},
                Password_Change_Confirmation: {locateStrategy: 'xpath', selector: '//div[contains(text(),\'You have successfuly changed your password!\')]'},
                two_attempts_remaining_message: {locateStrategy: 'xpath', selector: '//div[text()=\'The password is wrong. You have only 2 attempts to enter the correct password or your account will be locked.\']'},
                one_attempt_remaining_message: {locateStrategy: 'xpath', selector: '//div[text()=\'The password is wrong. You have only 1 attempt to enter the correct password or your account will be locked.\']'},
                Account_locked_message: {locateStrategy: 'xpath', selector: '//div[text()=\'You have exceeded the maximum number of login attempts and your account is now locked for ten minutes. \']'},




            },
        },
        IBCG2: {
            selector: 'body',
            elements: {
                linkContactUs: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Contact Us")]'},
                LoginManagement: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Login Management")]'},
                linkHelp: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Help")]'},
                linkSiteMap: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Site Map")]'},
                linkContactUsinheader: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Contact Us")]'},
                Logout: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Log Out")]'},
                HeaderTabs: {locateStrategy: 'xpath', selector: '//ul[@class="tabnavbar_navigator"]'},
                PlansLink: {locateStrategy: 'xpath', selector: '//ul[@class="tabnavbar_navigator_level2_selected"]/li/a'},
                HomeTab: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Home")]'},
                WealthTab: {locateStrategy: 'xpath', selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Wealth')]"},
                HealthTab: {locateStrategy: 'xpath', selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Health')]"},
                AbsenceTab: {locateStrategy: 'xpath', selector: "//ul[@class='tabnavbar_navigator']/li/a/span[contains(text(),'Absence')]"},
                TotalRewardsTab: {locateStrategy: 'xpath', selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Total Rewards')]"},
                FormsTab: {locateStrategy: 'xpath', selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Forms')]"},
                ResourceCenterTab: {locateStrategy: 'xpath', selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Resource Center')]"},
                AdministrationTab: {locateStrategy: 'xpath', selector: "//ul[@class='tabnavbar_navigator']/li/a//span[contains(text(),'Administration')]"},
                ImagemyWealthContainer: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"myWealth")]'},
                ImagemyHealthContainer: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"myHealth")]'},
                ImagemyAbsenceContaier: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"myAbsence")]'},
                //T&C url:https://qai.ibenefitcenter.com/QAWORK/tnc.tpz?ReturnUrl=https://qai.ibenefitcenter.com/QAWORK/HealthLanding.tpz
                TermsAndConditions_header: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"Terms and Conditions Agreement")]'},
                TermsAndConditions_checkbox: {locateStrategy: 'xpath', selector: '//input[@type="checkbox"]'},
                TermsAndConditions_NextButton: {locateStrategy: 'xpath', selector: '//input[@type="submit"]'},
            },
        },
        IBCHealthPage: {
            selector: 'body',
            elements: {
                linkOverview: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Overview")][1]'
                },
                linkBeneficiaries: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Beneficiaries")]'
                },
                linkCurrentCoverages: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Current Coverages")]'
                },
                linkFamilydata: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Family Data")]'
                },
                linkLifeStatusChange: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Life Status Change")]'
                },
                linkProviders: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Providers")]'
                },
                linkTools: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Tools")][0]'

                },
                linkForms: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Forms")][1]'
                },
                WhattoIhavecontainer: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"What do I have? Mercer Testing")]'
                },
                WhattoIneedcontainer: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"What do I need?")]'
                },
            }
        },
        IBCWealthPage: {
            selector: 'body',
            elements: {
                linkOverview: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Overview")][0]'
                },
                ListPlans: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Plans")][0]'
                },

                linkResources: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Resources")][0]'
                },
                linkForms: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Forms")][0]'
                },
            }
        },
        IBCFormsPage: {
            selector: 'body',
            elements: {
                tabWealthForms: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Wealth Forms")]'
                },
                tabHealthForms: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Health Forms")]'
                },

            }
        },
        IBCContactUsPage: {
            selector: 'body',
            elements: {
                ChatContainer: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Chat online with a Service Representative")]'
                },
                linkChatOnline: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Chat online with a Service Representative")]'
                },

                EmailUsContainer: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Visit the Message Center")]'
                },

                linkVisitMessageCenter: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Visit the Message Center")]'
                },
            }
        },
        IBCSiteMapPage: {
            selector: 'body',
            elements: {
                HeadingSiteMap: {
                    locateStrategy: 'xpath',
                    selector: '//h1[contains(text(),"Site Map")]'
                },
            }
        },
        IBCAdminLandingPage: {
            selector: 'body',
            elements: {
                linkEmployeesearch: {
                    locateStrategy: 'xpath',
                    selector: '//table/tbody/tr/td/div/div[1]/div/a/span'
                    //*[@id="WebPart_twp1268416957"]/tbody/tr/td/div/div[1]/div/a/span
                },
                SearchBySSN: {
                    locateStrategy: 'xpath',
                    selector: '//td[@class="searchCriteriaCell"]/select/option[2]'
                },
                SearchByLastName: {
                    locateStrategy: 'xpath',
                    selector: '//td[@class="searchCriteriaCell"]/select/option[1]'
                },

                inputKeyword: {
                    locateStrategy: 'xpath',
                    selector: '//td[@class="searchTextCell"]/input'
                },


                btnSearchProxy: {
                    locateStrategy: 'xpath',
                    selector: '//input[@type="submit"]'
                },
                selectlink: {
                    locateStrategy: 'xpath',
                    selector: '//td[@class="TableCellEmployee"]/a'
                },

                SSNfirstrowresult: {
                    locateStrategy: 'xpath',
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[2]/td[2]'
                },
                LastNamefirstrowresult: {
                    locateStrategy: 'xpath',
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[2]/td[3]'
                },
                FirstNamefirstrowresult: {
                    locateStrategy: 'xpath',
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[2]/td[4]'
                },

                labelImpersonate: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"You are currently impersonating")]'
                },

                linkEndImpersonation: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"End Impersonation")]'
                }
            },
        },
        mmx365P: {
            selector: 'body',
            elements: {

                Logout: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Logout")]'},
                Dashboard: {locateStrategy: 'xpath', selector: '//span[text()="Dashboard"]'},
                SecSettingslink: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Update Security Settings")]'},
                Clickherelink: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Click here")]'},
                CP_Dashboard: {locateStrategy: 'xpath', selector: '//a[text()=\'Dashboard\']'},
                CP_usericon: {locateStrategy: 'xpath', selector: '//div[@class=\'user-pic small\']'},
                CP_secsettings: {locateStrategy: 'xpath', selector: '//a[text()=\'Security Settings\']'},
                CP_logout: {locateStrategy: 'xpath', selector: '//a[text()=\'Log Out\']'},


            },
        },
        MBC: {
            selector: 'body',
            elements: {

                Logout: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Logout")]'},
                Dashboard: {locateStrategy: 'xpath', selector: '//h2[@class="title ng-binding"]'},
                SecSettingslink: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Update Security Settings")]'},
                Clickherelink: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Click here")]'},


            },
        },
    }

};
