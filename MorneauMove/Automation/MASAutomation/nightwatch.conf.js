var seleniumServer = require('selenium-server');
var chromedriver=require('chromedriver');
var ieDriver = require('iedriver');
var firefoxDriver = require('geckodriver');
var data = require('./test_data/MAS_IBCG2_Regression_TestData.js');
sauce_labs_username = "Nishant-Kolte";
sauce_labs_access_key = "82105826-03a4-4a4b-a195-f761fcc8ae4e";

var fnRandomNumber = function (no_of_digits) {
    var max = 9, min = 0, str="", i;
    for(i=0; i < no_of_digits; i++){
        str = str + Math.floor(Math.random() * (max - min) + min);
    }
    return str;
};

var RandomPort =fnRandomNumber(4);


require('nightwatch-cucumber')({
    supportFiles: ['./utils/TestExecListener.js'],
    stepTimeout: 300000,
    defaultTimeoutInterval: 300000,
    nightwatchClientAsParameter: true
});

module.exports = {
    output_folder: 'reports',
    custom_commands_path: '',
    custom_assertions_path: '',
    page_objects_path: "objects",
    live_output: false,
    disable_colors: false,
    test_workers: {
        enabled: false,
        workers: 3
    },
    selenium: {
        start_process: true,
        server_path: seleniumServer.path,
        host: '127.0.0.1',
        port: RandomPort,
        platform: 'Windows 10',
        cli_args: {
            'webdriver.chrome.driver' :chromedriver.path,
            'webdriver.ie.driver': './IEDriverServer.exe',
            'webdriver.firefox.driver': firefoxDriver.path
        }
    },
   //------------- to execute the script locally---------------------//

     test_settings: {
         default: {
             launch_url: 'http://localhost',
             page_objects_path: 'objects',
             selenium_host: '127.0.0.1',
             selenium_port: RandomPort,
             silent: true,
             disable_colors: false,
             screenshots: {
                 enabled: true,
                 on_failure: true,
                 on_error: true,
                 path: 'screenshots'
             },
             desiredCapabilities: {
                 browserName: data.NameofThebrowser,
                 //browserName: "internet explorer",
                 javascriptEnabled: true,
                 acceptSslCerts: true
             },
         },
     },

//-----------To execute the script in SauceLabs--------------//
/*
    "test_settings": {
        default: {
            launch_url: 'http://ondemand.saucelabs.com:80',
            selenium_port: 80,
            page_objects_path: "repository",
            selenium_host: 'ondemand.saucelabs.com',
            silent: true,
            username: sauce_labs_username,
            maxDuration: 10800,
            access_key: sauce_labs_access_key,
            screenshots: {
                enabled: false,
                on_failure: false,
                on_error: false,
                path: './screenshots'
            },
            globals: {
                waitForConditionTimeout: 10000
            },
            desiredCapabilities: {
                name: 'RegressionTest',
                browserName: 'chrome',
                parentTunnel: 'erik-horrell',
                tunnelIdentifier: 'MercerTunnel1',
                maxDuration: 10800,
            }
        },
        chrome: {
            desiredCapabilities: {
                browserName: 'chrome',
                maxDuration: 10800,
                javascriptEnabled: true,
                acceptSslCerts: true,
                chromeOptions: {
                    prefs: {
                        "credentials_enable_service": false,
                        "profile.password_manager_enabled": false
                    }
                },
                platform: "Windows 10",
                version: "64"
            },

        },
        chrome1: {
            desiredCapabilities: {
                browserName: 'chrome',
                maxDuration: 10800,
                javascriptEnabled: true,
                acceptSslCerts: true,
                recordMp4: true,
                chromeOptions: {
                    prefs: {
                        "credentials_enable_service": false,
                        "profile.password_manager_enabled": false
                    }
                },
                platform: "Windows 7",
                version: "64"
            },

        },
        "firefox": {
            "desiredCapabilities": {
                browserName: 'firefox',
                javascriptEnabled: true,
                acceptSslCerts: true,
                platform: "Windows 7",
                version: "58"
            }
        },
        "internet_explorer_11": {
            "desiredCapabilities": {
                "platform": "Windows 10",
                "browserName": "internet explorer",
                "version": "11"
            }
        },
        "IEEdge": {
            "desiredCapabilities": {
                "platform": "Windows 10",
                "browserName": "edge",
                "version": "14.14393",
                "javascriptEnabled": "true",
                "acceptSslCerts": "true",
                "device": "",
            }
        },
        "android_s4_emulator": {
            "desiredCapabilities": {
                "browserName": "android",      //android
                "deviceOrientation": "portrait",
                "deviceName": "Samsung Galaxy S4 Emulator"
            }
        },

        "android_7_emulator": {
            "desiredCapabilities": {
                "browserName": "android",
                "deviceOrientation": "portrait",
                "deviceType":"phone",
                "deviceName": "Android GoogleAPI Emulator",
                "platformVersion":"7.1"
            }
        },

        "iphone_6_simulator": {
            "desiredCapabilities": {
                "browserName": "iPhone",
                "deviceOrientation": "portrait",
                "deviceName": "iPhone 6",
                "platform": "OSX 10.10",
                "version": "8.4"
            }
        },

        "Samsung_Galaxy": {
            "desiredCapabilities": {
                "browserName": "android",
                "deviceOrientation": "portrait",
                "deviceName": "Samsung Galaxy Nexus GoogleAPI Emulator",
                //"platform": "",
                // "version": "",
            }
        }
    }
*/
    //-------------------------------------------------------------------------------//

}

