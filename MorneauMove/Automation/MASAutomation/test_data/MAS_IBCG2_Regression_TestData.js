module.exports = {

   //  NameofThebrowser: "firefox",
    NameofThebrowser: "chrome",
  // NameofThebrowser: "internet explorer",

    shortpause: 2000,
    averagepause: 7000,
    longpause: 40000,
    shortwaittime: 20000,
    averagewaittime: 40000,
    longwaittime: 120000,


    /* QA */  URL: 'https://qai.ibenefitcenter.com/QAWORK/login.tpz',
    /* CIT */ //URL: 'https://ctesti.ibenefitcenter.com/SLSDM/login.tpz',
    /* CSO */ //URL: 'https://csoi.ibenefitcenter.com/SLSDM/login.tpz',
    /* PROD */ //URL: 'https://ibenefitcenter2.mercerhrs.com/SLSDM/login.tpz',
    MMX365PURL: 'https://auth-qa.mercer.com/MMX365PLUS/Member/login',

    mmx365P_UN: 'AUTO_QAtestuser@mmx365P.com',
    mmx365P_PW: 'Test@001',

//-----QA Test data ------//

//----Login errors:
            inactive_user_UN: 'AUTO_inactiveuser@mas.com', inactive_user_PW: 'test@001',
            MercerLocked_user_UN: 'AUTO_lockedbyMercer@mas.com', MercerLocked_user_PW: 'test@001',
            ClientLocked_user_UN: 'AUTO_lockedbyclient@mas.com', ClientLocked_user_PW: 'test@001',
            LockedByUser_UN: 'AUTO_lockedbyuser@mas.com', LockedByUser_PW: 'test@001',
            AccountLocked_UN: 'AUTO_Accountlocked@mas.com', AccountLocked_PW: 'test@001',
            invalid_UN: 'invalidUN', invalid_PW: 'test@001',

//---- migrateduser login----//
            additional_email: 'nishant.kolte@mercer.com',
};
