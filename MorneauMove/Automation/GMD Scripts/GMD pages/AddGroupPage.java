package pages.GMD;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class AddGroupPage {
	
	ExtentTest test =null;
	
	
	@FindBy(xpath = "//input[@id='group-name']")
	public WebElement grp_Name;
	
	@FindBy(xpath = "//select[@id='group-category']")
	WebElement grp_Category;
	
	@FindBy(xpath = "//input[@id='group-description']")
	WebElement grp_Desc;
	
	@FindBy(xpath = "//input[@id='group-lob-hb']")
	WebElement grp_LOB;
	
	@FindBy(xpath = "//textarea[@id='group-comments']")
	WebElement grp_Comments;
	
	@FindBy(xpath = "//css = \"#expression_editor Textarea\"")
	WebElement grp_Rule;
	
	@FindBy(xpath = "//button[@id='Button3']")
	WebElement synButton;
	
	@FindBy(xpath = "//p[text()='The rule you have written is valid.']")
	WebElement synConfirm;
	
	@FindBy(xpath = "//button[@id='add-edit-group-save-btn']")
	WebElement grp_Save;
	
	public AddGroupPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void newGroup(String Group) throws InterruptedException {
		
		waitForElementToDisplay(grp_Name);
		setInput(grp_Name, Group);
		selEleByVisbleText(grp_Category, "Demographic");
		setInput(grp_Desc, "test description");
		clickElement(grp_LOB);
		setInput(grp_Comments, "test comments");
		setInput(grp_Rule,"CEmployee.Ssn='100613111'");
		clickElement(synButton);
		waitForElementToDisplay(synConfirm);
		verifyElementTextContains(synConfirm, "The rule you have written is valid.", test);
		clickElement(grp_Save);
		
	}
}