-------------------------------------------------------------------------------------------------
-- File        : HB19.1_MasterPatch.sql
-- Created Date        : 01/08/2019
-- Updated Date        : 
-- Description  : This is the consolidated master patch for following Patches:

--      BW5x_21
--      MBCMasterPatch_HB_17
--      MBCMasterPatch_HB_16
--      MBCMasterPatch_HB_15
--      MBCMasterPatch_HB_14


-- Version    : 
-------------------------------------------------------------------------------------------------

SET ECHO ON;
SET VERIFY ON;
SET HEADING OFF;
SET LINESIZE 132;
SET SERVEROUT ON;

spool spool_HB19.1_MasterPatch.log;

-------------------------------------------------------------------------------------------------
-- File        : HB19.1_MasterPatch.sql
-- Date        : 01/08/2019
-- Description  : This patch will create the  tables for following  tables 
--					1.	MODEL_LE_USERCODES 
--					2.	PCE_NATIONAL_AVG_DATA 
--					3.	PCE_REGION_MAPPING 
--					4.	PCE_PLAN_DETAILS 
--					5.	PCE_MSA_ZIPS 
--					6.	DEP_LE_SNAPSHOT 
--					
-- Version    : 1.0
-------------------------------------------------------------------------------------------------




---------------------------------------------------------------
--
-- file: HB Modelling Patch .sql
-- patch name: 'MBCMasterPatch_HB_14.sql'
-- date: 01/08/2019
-- created by: John O Grady
-- project: HB
-- description: Add 2 columns to EMPLOYEE_EFF_DATE and create table DEP_LE_SNAPSHOT for modeling changes
------------------------------------

DECLARE
	table_exists number :=0;
	column_exists number := 0;  
BEGIN
	SELECT count(*) INTO table_exists FROM USER_TABLES WHERE UPPER(TABLE_NAME) = 'EMPLOYEE_LIFE_EVENTS';
	IF table_exists > 0 THEN
	  dbms_output.put_line ('Altering table EMPLOYEE_LIFE_EVENTS, table exists');
	  SELECT count(*) INTO column_exists FROM user_tab_cols WHERE column_name = 'IS_MODEL' AND table_name = 'EMPLOYEE_LIFE_EVENTS';
		
		IF column_exists = 0 THEN
		  dbms_output.put_line ('APPLIED: ALTER TABLE EMPLOYEE_LIFE_EVENTS, IS_MODEL columns');
			EXECUTE IMMEDIATE 'ALTER TABLE EMPLOYEE_LIFE_EVENTS ADD ("IS_MODEL" VARCHAR2(1))';
		ELSE
			dbms_output.put_line ('SKIPPED: ALTER TABLE EMPLOYEE_LIFE_EVENTS, IS_MODEL columns already exists');
		END IF;
	ELSE
		dbms_output.put_line ('SKIPPED: ALTER TABLE EMPLOYEE_LIFE_EVENTS, table does not exist');
	END IF;
END;

/



DECLARE
	table_exists number :=0;
	column_exists number := 0;  
BEGIN
	SELECT count(*) INTO table_exists FROM USER_TABLES WHERE UPPER(TABLE_NAME) = 'EMPLOYEE_LIFE_EVENTS';
	IF table_exists > 0 THEN
	  dbms_output.put_line ('Altering table EMPLOYEE_LIFE_EVENTS, table exists');
	  SELECT count(*) INTO column_exists FROM user_tab_cols WHERE column_name = 'DATE_ENTERED' AND table_name = 'EMPLOYEE_LIFE_EVENTS';
		
		IF column_exists = 0 THEN
		  dbms_output.put_line ('APPLIED: ALTER TABLE EMPLOYEE_LIFE_EVENTS, DATE_ENTERED columns');
			EXECUTE IMMEDIATE 'ALTER TABLE EMPLOYEE_LIFE_EVENTS ADD ("DATE_ENTERED" DATE)';
		ELSE
			dbms_output.put_line ('SKIPPED: ALTER TABLE EMPLOYEE_LIFE_EVENTS, DATE_ENTERED columns already exists');
		END IF;
	ELSE
		dbms_output.put_line ('SKIPPED: ALTER TABLE EMPLOYEE_LIFE_EVENTS, table does not exist');
	END IF;
END;

/



DECLARE NUM NUMBER :=0;
BEGIN

SELECT COUNT(*) INTO NUM FROM USER_TABLES WHERE UPPER(TABLE_NAME) = 'DEP_LE_SNAPSHOT';
	IF NUM = 0 THEN
		dbms_output.put_line ('CREATED: Creating table "DEP_LE_SNAPSHOT"');
       EXECUTE IMMEDIATE         
                  'CREATE TABLE "DEP_LE_SNAPSHOT" 
   	                ("CONTROL_ID" VARCHAR2(10) NOT NULL ENABLE, 
	                "SSN" VARCHAR2(10) NOT NULL ENABLE, 
	                "DEPN_SSN" VARCHAR2(10) NOT NULL ENABLE, 
	                "EFF_DATE" DATE NOT NULL ENABLE, 
	                "LIFE_EVENT_DATE" DATE NOT NULL ENABLE, 
	                "LE_SEQ_NO" NUMBER(22,0) NOT NULL ENABLE, 
	                "ADDRESS_KEY" NUMBER(2,0), 
	                "FIRST_NAME" VARCHAR2(30), 
	                "LAST_NAME" VARCHAR2(30), 
	                "MIDDLE_INIT" CHAR(1), 
	                "BIRTHDATE" DATE, 
	                "RELATION" VARCHAR2(10), 
	                "STATUS" VARCHAR2(10), 
	                "SEX" CHAR(1), 
	                "IS_DEP_EMP_IND" CHAR(1), 
	                "RELATION_CHANGE_DATE" DATE, 
	                "STATUS_CHANGE_DATE" DATE, 
	                "IS_DEP_BEN_IND" CHAR(1), 
	                "MIDDLE_NAME" VARCHAR2(30), 
	                "IS_MEDICARE_ELIG_IND" CHAR(1), 
	                "MEDICARE_ELIG_STATUS_DATE" DATE, 
	                "IS_DEP_VERIFIED_IND" CHAR(1), 
	                "DEP_VERIFICATION_STATUS_DATE" DATE, 
	                "IS_STATE_TAX_EXCLUDED_IND" CHAR(1), 
	                "SEQUENCE_NUMBER" NUMBER(2,0), 
	                "IS_LOCKED_IND" CHAR(1), 
	                "USERCODE_1" VARCHAR2(50), 
	                "USERCODE_2" VARCHAR2(50), 
	                "USERCODE_3" VARCHAR2(50), 
	                "DATE_1" DATE, 
	                "DATE_2" DATE, 
	                "DATE_3" DATE, 
	                "NUMBER_1" NUMBER(14,2), 
	                "NUMBER_2" NUMBER(14,2), 
	                "NUMBER_3" NUMBER(14,2),
	                "SSNCHG_NEW" VARCHAR2(10)
                    )';
             
       EXECUTE IMMEDIATE  'alter table DEP_LE_SNAPSHOT add constraint PK_DEP_LE_SNAPSHOT primary key ("CONTROL_ID", "SSN", "DEPN_SSN", "EFF_DATE", "LIFE_EVENT_DATE", "LE_SEQ_NO") using index';
	ELSE
		dbms_output.put_line ('SKIPPED: Creating table "DEP_LE_SNAPSHOT", Table already exists');
    END IF;
END;

/



----------------------------------------------------------------------------------------------------
-- file: MBCMasterPatch_HB_15.sql
-- patch name: 'MBCMasterPatch_HB_15.sql'
-- date: 01/08/2019
-- created by: Jill Barrow
-- project: MBC
-- description: Add PCE_NATIONAL_AVG_DATA table to the BW schema
-----------------------------------------------------------------------------------------------------

DECLARE NUM NUMBER :=0;
BEGIN

SELECT COUNT(*) INTO NUM FROM USER_TABLES WHERE UPPER(TABLE_NAME) = 'PCE_NATIONAL_AVG_DATA';
	IF NUM = 0 THEN
		dbms_output.put_line ('CREATED: Creating table PCE_NATIONAL_AVG_DATA');
       EXECUTE IMMEDIATE         
                  'CREATE TABLE PCE_NATIONAL_AVG_DATA
                  (
                    CONTROL_ID           varchar2(10) not null,
                    EFF_DATE             date not null,
                    GENDER               char(1) not null,
                    AGE_BAND             number(3) not null,
                    REGION               varchar2(30) not null,
                    MSA		       varchar2(10) not null,
                    UTILIZATION_CATEGORY varchar2(30) not null,
                    AVG_USAGES           number(15,12),
                    UNIT_COST            number(12,5)
                  ) ';
             
       EXECUTE IMMEDIATE  'alter table PCE_NATIONAL_AVG_DATA add constraint PCE_NATIONAL_AVG_DATA_PK primary key (CONTROL_ID, EFF_DATE, GENDER, AGE_BAND, REGION, MSA, UTILIZATION_CATEGORY) using index';
	ELSE
		dbms_output.put_line ('SKIPPED: Creating table PCE_NATIONAL_AVG_DATA, Table already exists');
    END IF;
END;

/


DECLARE NUM NUMBER :=0;
BEGIN

SELECT COUNT(*) INTO NUM FROM USER_TABLES WHERE UPPER(TABLE_NAME) = 'PCE_REGION_MAPPING';
	IF NUM = 0 THEN
		dbms_output.put_line ('CREATED: Creating table PCE_REGION_MAPPING');
       EXECUTE IMMEDIATE         
                  ' create table PCE_REGION_MAPPING
                   (
                     CONTROL_ID varchar2(10) not null,
                     STATE      varchar2(2) not null,
                     REGION     varchar2(30)
                   )';
             
       EXECUTE IMMEDIATE  'alter table PCE_REGION_MAPPING add constraint PCE_REGION_MAPPING_PK primary key (CONTROL_ID, STATE) using index';			
	ELSE
		dbms_output.put_line ('SKIPPED: Creating table PCE_REGION_MAPPING, Table already exists');
    END IF;
END;

/



DECLARE NUM NUMBER :=0;
BEGIN

SELECT COUNT(*) INTO NUM FROM USER_TABLES WHERE UPPER(TABLE_NAME) = 'PCE_PLAN_DETAILS';
	IF NUM = 0 THEN
		dbms_output.put_line ('CREATED: Creating table PCE_PLAN_DETAILS');
       EXECUTE IMMEDIATE         
                  'create table PCE_PLAN_DETAILS
                  (
                    CONTROL_ID        varchar2(10) not null,
                    COMPANY_ID         varchar2(10) not null,
                    EFF_DATE          date not null,
                    BENEFIT_ID        varchar2(10) not null,
                    PLAN_ID           varchar2(10) not null,
                    PLAN_DETAIL_NAME  varchar2(50) not null,
                    PLAN_DETAIL_VALUE number(9,2)
                  )';
             
       EXECUTE IMMEDIATE  'alter table PCE_PLAN_DETAILS add constraint PCE_PLAN_DETAILS_PK primary key (CONTROL_ID, COMPANY_ID, EFF_DATE, BENEFIT_ID, PLAN_ID, PLAN_DETAIL_NAME) using index';
	ELSE
		dbms_output.put_line ('SKIPPED: Creating table PCE_PLAN_DETAILS, Table already exists');
    END IF;
END;

/




DECLARE NUM NUMBER :=0;
BEGIN

SELECT COUNT(*) INTO NUM FROM USER_TABLES WHERE UPPER(TABLE_NAME) = 'PCE_MSA_ZIPS';
	IF NUM = 0 THEN
		dbms_output.put_line ('CREATED: Creating table PCE_MSA_ZIPS');
       EXECUTE IMMEDIATE         
                  ' create table PCE_MSA_ZIPS
                  (
                    CONTROL_ID VARCHAR2(10) not null,
                    ZIP        VARCHAR2(5) not null
                  )';
             
       EXECUTE IMMEDIATE  'alter table PCE_MSA_ZIPS add constraint PCE_MSA_ZIPS_PK primary key (CONTROL_ID, ZIP) using index';

	ELSE
		dbms_output.put_line ('SKIPPED: Creating table PCE_MSA_ZIPS, Table already exists');
    END IF;
END;

/


------------------------------------
-- New table MODEL_LE_USERCODES--
-- file: MBCMasterPatch_HB_16.sql
-- patch name: 'MBCMasterPatch_HB_16.sql'
-- date: 01/08/2019
-- created by: Vipin
-- project: MBC
-- description: Add MODEL_LE_USERCODES table to the BW schema

------------------------------------
DECLARE NUM NUMBER :=0;
BEGIN

SELECT COUNT(*) INTO NUM FROM USER_TABLES WHERE UPPER(TABLE_NAME) = 'MODEL_LE_USERCODES';
  IF NUM = 0 THEN
  dbms_output.put_line ('CREATED: Creating table MODEL_LE_USERCODES');
     EXECUTE IMMEDIATE 
                 'create table MODEL_LE_USERCODES
                 (
                   CONTROL_ID      VARCHAR2(10) not null,
                   SSN             VARCHAR2(10) not null,
                   LIFE_EVENT_DATE DATE not null,
                   LE_SEQ_NO       NUMBER(4) not null,
                   USERCODE_1      VARCHAR2(50),
                   USERCODE_2      VARCHAR2(50),
                   DATE_1          DATE,
                   DATE_2          DATE
                 )';
         
       EXECUTE IMMEDIATE 'alter table MODEL_LE_USERCODES add constraint MODEL_LE_USERCODES_PK primary key (CONTROL_ID, SSN, LIFE_EVENT_DATE, LE_SEQ_NO) using index';
	ELSE
		dbms_output.put_line ('SKIPPED: Creating table MODEL_LE_USERCODES, Table already exists');
  END IF;
END;

/

-------------------------------------------------------------------------------------------------
-- File		: MBCMasterPatch_HB_17.sql
-- Date		: 03/05/2019
-- created by: John O
-- project: MBC
-- description: Add column
--------------------------------------------------------------------------------------------------
DECLARE 
NUM NUMBER :=0;
BEGIN
NUM :=0;
SELECT COUNT(*) INTO NUM FROM USER_TAB_COLUMNS WHERE TABLE_NAME = 'AAC_LIFE_EVENTS' AND COLUMN_NAME IN ('GUIDED_SHOPPING_ENABLED') ;
IF NUM = 0 THEN
	EXECUTE IMMEDIATE 'ALTER TABLE AAC_LIFE_EVENTS ADD 
	(
			GUIDED_SHOPPING_ENABLED CHAR(1) 
	)';

END IF;
END;
/




-------------------------------------------------------------------------------------------------
-- File		: BW5x_21
-- Date		: 04/02/2019
-- created by: 
-- project: 
-- description: Creating Trigger
--------------------------------------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TR_UPDATE_EMPLOYEE_DATA
AFTER  UPDATE
   ON  EMPLOYEE_DATA
   FOR EACH ROW
   
BEGIN
   IF :NEW.USERCODE6 = 'Y' OR :NEW.USERCODE7 = 'Y' THEN
   
      --insert record in AAC_MEMSRV_ACCOUNT_SETUP if row is not there for particular ssn
      INSERT INTO AAC_MEMSRV_ACCOUNT_SETUP
      SELECT e.CONTROL_ID, e.SSN, SUBSTR(e.EMAIL_ADDRESS,1,50),'Y', 'N' 
      FROM EMPLOYEE e 
      WHERE e.SSN NOT IN (SELECT SSN FROM AAC_MEMSRV_ACCOUNT_SETUP WHERE SSN=e.SSN)
      AND e.SSN = :OLD.SSN;
      
      --insert record in AAC_MEMSRV_ACCOUNT if row is not there for particular ssn
      INSERT INTO AAC_MEMSRV_ACCOUNT
      SELECT e.CONTROL_ID, e.SSN, NULL, e.SSN, '0F26A18DD3E702501D6F330E9ADB6C804460D326C8ECD88C0D58D8807936735D', NULL, NULL, NULL, NULL, NULL,NULL, NULL, 'N', NULL, 'N'
      FROM EMPLOYEE e 
      WHERE e.SSN NOT IN (SELECT SSN FROM AAC_MEMSRV_ACCOUNT WHERE SSN=e.SSN) 
      AND e.SSN = :OLD.SSN;
      
   END IF;
END;

/

commit;

---------------------------------------------------------------------------
--
--  Version control
--
---------------------------------------------------------------------------
 

 insert into versn_dbschema (item_type, version_date, version_no, comments, user_name)
values('CUMHB19.1P',sysdate,'HB19.1_MstrPtch','HB19.1_MasterPatch.sql','&2.');


insert into versn_dbschema (item_type, version_date, version_no, comments, user_name)
values ('CUMHB19.1P', sysdate + 1/86400, 'MBCPatch_HB_14','MBCMasterPatch_HB_14.sql applied by HB19.1_MasterPatch.sql', '&2.');

insert into versn_dbschema (item_type, version_date, version_no, comments, user_name)
values ('CUMHB19.1P', sysdate + 2/86400, 'MBCMstP_HB_15','MBCMasterPatch_HB_15.sql applied by HB19.1_MasterPatch.sql', '&2.' );

insert into versn_dbschema (item_type, version_date, version_no, comments, user_name)
values ('CUMHB19.1P', sysdate + 3/86400, 'MBCPatch_HB_16','MBCPatch_HB_16.sql applied by HB19.1_MasterPatch.sql', '&2.');

insert into versn_dbschema (item_type, version_date, version_no, comments, user_name)
values ('CUMHB19.1P', sysdate + 4/86400, 'MBCPatch_HB_17','MBCPatch_HB_17.sql applied by HB19.1_MasterPatch.sql', '&2.');

insert into versn_dbschema (item_type, version_date, version_no, comments, user_name)
values ('CUMHB19.1P', sysdate + 5/86400, 'BW5x_21','BW5x_21.sql applied by HB19.1_MasterPatch.sql', '&2.');


--connect &2./&3.@&4..world

commit;
spool off;
exit;
