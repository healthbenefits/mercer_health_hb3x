-------------------------------------------------------------------------------------------------
-- File        : HB19.2_MasterPatch.sql
-- Created Date        : 05/21/2019
-- Updated Date        : 
-- Description  : This is the patch to be applied AFTER HB19.1_MasterPatch 

-- Version    : 1.0
-------------------------------------------------------------------------------------------------

SET ECHO ON;
SET VERIFY ON;
SET HEADING OFF;
SET LINESIZE 132;
SET SERVEROUT ON;

spool spool_HB19.2_MasterPatch.log;


CREATE OR REPLACE TRIGGER TR_UPDATE_EMPLOYEE_DATA
AFTER  UPDATE
   ON  EMPLOYEE_DATA
   FOR EACH ROW
   
BEGIN
   IF :NEW.USERCODE6 = 'Y' OR :NEW.USERCODE7 = 'Y' THEN
   
      --insert record in AAC_MEMSRV_ACCOUNT_SETUP if row is not there for particular ssn
      INSERT INTO AAC_MEMSRV_ACCOUNT_SETUP
      SELECT e.CONTROL_ID, e.SSN, SUBSTR(e.EMAIL_ADDRESS,1,50),'Y', 'N' 
      FROM EMPLOYEE e 
      WHERE e.SSN NOT IN (SELECT SSN FROM AAC_MEMSRV_ACCOUNT_SETUP WHERE SSN=e.SSN)
      AND e.SSN = :NEW.SSN;
	  
      
      --insert record in AAC_MEMSRV_ACCOUNT if row is not there for particular ssn
      INSERT INTO AAC_MEMSRV_ACCOUNT
      SELECT e.CONTROL_ID, e.SSN, NULL, e.SSN, '0F26A18DD3E702501D6F330E9ADB6C804460D326C8ECD88C0D58D8807936735D', NULL, NULL, NULL, NULL, NULL,NULL, NULL, 'N', NULL, 'N'
      FROM EMPLOYEE e 
      WHERE e.SSN NOT IN (SELECT SSN FROM AAC_MEMSRV_ACCOUNT WHERE SSN=e.SSN) 
      AND e.SSN = :NEW.SSN;
      
   END IF;
END;

/

commit;

---------------------------------------------------------------------------
--
--  Version control
--
---------------------------------------------------------------------------
 

 insert into versn_dbschema (item_type, version_date, version_no, comments, user_name)
values('CUMHB19.2P',sysdate,'HB19.2_MstrPtch','HB19.2_MasterPatch.sql','&2.');


--connect &2./&3.@&4..world

commit;
spool off;
exit;
