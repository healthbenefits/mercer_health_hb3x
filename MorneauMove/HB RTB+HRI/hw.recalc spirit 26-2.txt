//***********************************************************************
// SPIRIT ID: hw.recalc
// CURRENT VERSION: 1.04
//
// VERSION HISTORY:
// Version 1.00 02/08/2012 AYelskiy - Initial version
// Version 1.01 10/20/2014 JDunn - Carried forward LE Status date for MBC
// Version 1.02 01/10/2017 JBARROW - Added outbound attr so we will know when 
//                         a process was a recalc
// Version 1.03 06/27/2018 JBARROW - Carry forward IS_MODEL flag on recalc
// Version 1.04 07/25/2018 SJAIN - Updates for Story S-46404; MBC PRB Eligibility Tool Updates
//
// PARAMETERS:
//   Parm/Part
//       1/1   The rule_id of this rule instance
//       1/2   The name of this spirit module (ex. hw.xxxxxx)
//
// INBOUND ATTRIBUTES: 
// ATTR(232)  - Control ID
// ATTR(189)  - Participant's SSN
// ATTR(322)  - Life Event ID
// ATTR(324)  - Life Event Date
// ATTR(1609) - LE Sequence Number
// ATTR(1999) - Business Object flag
// ATTR(190)  - Benefit ID (we hard code to "0")
// ATTR(191)  - Plan ID (we hard code to "0")
// ATTR(225)  - Participant's Birthdate
// ATTR(226)  - Latest Hire Date
// ATTR(227)  - Last Term Date
// ATTR(230)  - Current Company
// ATTR(231)  - Current Payroll
// ATTR(237)  - Pay Frequency
// ATTR(284)  - Gender
// ATTR(325)  - Marital Status
// ATTR(326)  - Controlled Group Status (Active, Retired, LOA, etc)
// ATTR(605)  - Sysdate
// ATTR(900)  - hard coded to "dw"
// ATTR(1993) - Calc sequence number (for asynchronous calcs)
//
// OUTBOUND ATTRIBUTES:
// ATTR(2900) - This life event is a recalc (processed through this Spirit module from the web)
//
// PURPOSE:  Entry-point rule for web-initiated recalc
//
// NOTES: The module duplicates the logic behind the 'Recalc' button in BW
//
//        1) Status for LE to be recalced is set to I
//
//        2) New row is inserted into the employee life events table for
//           the same LE ID and effective date.
//
//           The le_seq_no is incremented by one.
//
//        3) ATTR(746) is set to Y
//
//        4) If the life event is not OE (55), then ATTR(748) is set to Y
//
//        5) Call hw.life
//
// OTHER:
// Tab=3 spaces in this module.
//***********************************************************************
//VARIABLE DECLARATIONS
RULE     this_rule, hwlife_rule

STRING   ssn, control_id, where_ssn, where, life_event_id, temp_le_id,
         s_old_le_seq_no, temp_le_status, proceed_sw, temp_s, bad_data_sw,
         found_sw, modeling_flag

DATE     life_event_date, temp_le_date, calc_date_time, old_le_status_date

NUMBER   old_le_seq_no, new_le_seq_no, temp_le_seq_no,
         new_life_event_number

TABLE_HANDLE EMPLOYEE_LIFE_EVENTS
//***********************************************************************

// --- Read ATTRs -------------------------------------------------------

ssn = @ATTR(189)
control_id = @ATTR(232)

life_event_id = @ATTR(322)
life_event_date = @ATTR(324)

s_old_le_seq_no = @ATTR(1609)
old_le_seq_no = NUMBER(s_old_le_seq_no)

//calc_date_time = @ATTR(605)
calc_date_time = DATETIME_TODAY

// --- Initialize other vars --------------------------------------------

this_rule = PARSE(@PARM(1), ",", 1)
where_ssn = "control_id = '" + control_id + "' and ssn = '" + ssn + "'"

// --- OPEN TABLES ------------------------------------------------------

OPEN "EMPLOYEE_LIFE_EVENTS" TO EMPLOYEE_LIFE_EVENTS
ACTIVATE_COLUMNS(EMPLOYEE_LIFE_EVENTS, "@ALL")
RESTRICT_UPDATES(EMPLOYEE_LIFE_EVENTS, "@ALL")
SET_ARRAY_SIZE(EMPLOYEE_LIFE_EVENTS, 100)
ACCESS_DATETIME(EMPLOYEE_LIFE_EVENTS, "CALC_DATE_TIME")

// ----------------------------------------------------------------------

// --- MAIN LOGIC -------------------------------------------------------

// 1) Status for LE to be recalced is set to I ---

// When we insert a new row, we will need an incremented life_event_number
// (not to be confused with life_event_id), so we need to fetch
// the max row for the ssn.
//
// If the top-of-stack row turns out to belong to our event, we will
// invalidate it, and skip to the Step 2 - inserting the new row; otherwise,
// we will simply get the new life_event_number, and look for our row in
// the next section.

where = where_ssn
where = where + " order by life_event_number desc"

//Initialize to DATE_TODAY 
old_le_status_date = DATE_TODAY //v1.04

//Set to blank by default
modeling_flag = "" //v1.04

proceed_sw = "N"
bad_data_sw = "N"
found_sw = "N"
new_life_event_number = 0
SET_WHERE_CLAUSE(EMPLOYEE_LIFE_EVENTS, where)
FETCH EMPLOYEE_LIFE_EVENTS
{
   new_life_event_number = EMPLOYEE_LIFE_EVENTS.life_event_number + 1

   // Let's check if this row happens to belong to our LE

   temp_le_id = EMPLOYEE_LIFE_EVENTS.life_event_id
   temp_le_seq_no = EMPLOYEE_LIFE_EVENTS.le_seq_no
   temp_le_date = EMPLOYEE_LIFE_EVENTS.life_event_date
   old_le_status_date = EMPLOYEE_LIFE_EVENTS.life_event_status_date //v1.01

   IF temp_le_date = life_event_date THEN
   {
      IF temp_le_seq_no = old_le_seq_no THEN
      {
         // Yay! It's looks like our row.
         found_sw = "Y"

	 modeling_flag = EMPLOYEE_LIFE_EVENTS.is_model //v1.03

         // Just to be sure
         IF temp_le_id = life_event_id THEN
         {
            // We're good
            proceed_sw = "Y"
            new_le_seq_no = temp_le_seq_no + 1

            // Let's invalidate it.
            EMPLOYEE_LIFE_EVENTS.life_event_status = "I"
            WRITE(EMPLOYEE_LIFE_EVENTS)
         }
         ELSE
         {
            // !!! ERROR CONDITION !!!
            // We have data inconsistency
            proceed_sw = "N"
            bad_data_sw = "Y"
            // TODO: What do we want to do? cause an exception in code?
         }
      }
   }
}


IF found_sw = "N" THEN
{
   // Since the row fetched did not belong to us, query for events on the
   // current event date, including any rows that may have a greater
   // sequence number than our event.  This will allow us to catch any 
   // row(s) inserted unbeknownst to us, such as a recalc from BW while 
   // the participant is on the web.  While somewhat unlikely with LE, the
   // risk goes up if the LE date is the same as OE.
   // In most cases we would only return one row - the current one.

   where = where_ssn
   where = where + " and life_event_date =  to_date('" + life_event_date + "','mm/dd/yyyy') "
   where = where + " and le_seq_no >= " + s_old_le_seq_no
   where = where + " order by le_seq_no desc"

   new_le_seq_no = 0
   SET_WHERE_CLAUSE(EMPLOYEE_LIFE_EVENTS, where)
   FOR_EACH_ROW EMPLOYEE_LIFE_EVENTS
   {
      temp_le_id = EMPLOYEE_LIFE_EVENTS.life_event_id
      temp_le_seq_no = EMPLOYEE_LIFE_EVENTS.le_seq_no
      temp_le_status = EMPLOYEE_LIFE_EVENTS.life_event_status
      
      // Check the top-of-stack le_seq_no
      IF temp_le_seq_no = old_le_seq_no THEN
      {
         proceed_sw = "Y"

         // Check LE id to confirm that it's our row
         IF temp_le_id <> life_event_id THEN
         {
            // !!! ERROR CONDITION !!!
            // We have data inconsistency
            proceed_sw = "N"
            bad_data_sw = "Y"
            // TODO: What do we want to do? cause an exception in code?
         }
      }
      ELSE
      {
         // Not our row
         proceed_sw = "N"

         // Since not our row, check the status
         IF temp_le_status = "P" THEN
         {
            // Only other permitted pending row is OE
            IF temp_le_id <> 55 THEN
            {
               // !!! ERROR CONDITION !!!
               // We have a renegade row
               bad_data_sw = "Y"
               // TODO: What do we want to do? cause an exception in code?
            }
         }
      }

      // Set new le_seq_no
      IF new_le_seq_no = 0 THEN
      {
         // regardless of whether this is our row or not
         new_le_seq_no = temp_le_seq_no + 1
      }

      // If everything checks out - update the le status to 'I'
      IF proceed_sw = "Y" THEN
      {
         EMPLOYEE_LIFE_EVENTS.life_event_status = "I"
         WRITE(EMPLOYEE_LIFE_EVENTS)
      }
   }
}


IF proceed_sw = "Y" THEN
{

   // 2) New row is inserted into the employee life events table for
   //    the same LE ID and effective date.

   NEW_ROW (EMPLOYEE_LIFE_EVENTS)
   EMPLOYEE_LIFE_EVENTS.control_id = control_id
   EMPLOYEE_LIFE_EVENTS.ssn = ssn
   EMPLOYEE_LIFE_EVENTS.life_event_date = life_event_date
   EMPLOYEE_LIFE_EVENTS.life_event_id = life_event_id
   EMPLOYEE_LIFE_EVENTS.user_id = "WEB"
   EMPLOYEE_LIFE_EVENTS.life_event_status = "P"
   //EMPLOYEE_LIFE_EVENTS.life_event_status_date = DATE_TODAY //v 1.01
   EMPLOYEE_LIFE_EVENTS.life_event_status_date = old_le_status_date //v 1.01
   EMPLOYEE_LIFE_EVENTS.elec_eff_date = life_event_date
   EMPLOYEE_LIFE_EVENTS.calc_date_time = DATETIME_TODAY
   EMPLOYEE_LIFE_EVENTS.life_event_number = new_life_event_number
   EMPLOYEE_LIFE_EVENTS.le_seq_no = new_le_seq_no
   EMPLOYEE_LIFE_EVENTS.changing_ssn = "N"
   EMPLOYEE_LIFE_EVENTS.is_model = modeling_flag //v 1.03
   WRITE(EMPLOYEE_LIFE_EVENTS)
   COMMIT
}


// ----------------------------------------------------------------------

// --- CLOSE TABLES -----------------------------------------------------

CLOSE EMPLOYEE_LIFE_EVENTS

// ----------------------------------------------------------------------

IF proceed_sw = "Y" THEN
{

   // 3) ATTR(746) is set to Y
   @ATTR(746) = "Y"

   // 4) If the life event is not OE (55), then ATTR(748) is set to Y
   IF life_event_id <> "55" THEN
   {
      @ATTR(748) = "Y"
   }
   
   //Version 1.02 Start
   @ATTR(2900) = "Y"
   //Version 1.02 End

   // 5) Call hw.life

   @ATTR(1609) = PARSE(STRING(new_le_seq_no), ".", 1)

   hwlife_rule = "hw.life"
   CALL_RULE hwlife_rule
}

// ************************** END CODE **********************************
