package regression.HRI_EP.tests;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HRI_EP.Adm_BEPage;
import pages.HRI_EP.ClientHomePage;
import pages.HRI_EP.Client_Page;
import pages.HRI_EP.Con_BEPage;
import pages.HRI_EP.MB_SubmitPage;
import pages.HRI_EP.ManualBatchPage;
import pages.HRI_EP.OnHoldLEPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class OnHoldLEVerify extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  OnHoldLEVerify(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		OnHoldLEVerify Tc01 = new OnHoldLEVerify("HRI");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	@Test(priority = 1, enabled = true)
	public void ohLE() throws Exception {
		try {
			test = reports.createTest("HRI/EP--verify Login To HRI/EP--" + BROWSER_TYPE);
			test.assignCategory("regression");
			
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			Client_Page clientSelect = new Client_Page(driver); 
			clientSelect.clientClick(Client);
			
			ClientHomePage clientHome= new ClientHomePage(driver);
			waitForElementToDisplay(clientHome.client_HomePage_Verify);
			SoftAssertions.verifyElementTextContains(clientHome.client_HomePage_Verify,Client,test);
			clientHome.adm_BE_Click();
			
			Adm_BEPage manualBatch= new Adm_BEPage(driver);
			waitForElementToDisplay(manualBatch.be_title);
			SoftAssertions.verifyElementTextContains(manualBatch.be_title,"Batch Events - Dashboard - Delivery",test);
			manualBatch.mb_Icon_Click();
			
			ManualBatchPage batchDetails= new ManualBatchPage(driver);
			waitForElementToDisplay(batchDetails.mbpage_Title);
			SoftAssertions.verifyElementTextContains(batchDetails.mbpage_Title,"Setup Manual Batch",test);
			batchDetails.mb_Details(mbExpression, batchDescription, action);
			
			MB_SubmitPage submitBatch= new MB_SubmitPage(driver);
			submitBatch.submit_MB();
			
			Thread.sleep(30000);
			Adm_BEPage mbComplete= new Adm_BEPage(driver);
			mbComplete.mb_Progress();
			waitForElementToDisplay(mbComplete.batch_Complete);
			
			manualBatch.mb_Icon_Click();
			batchDetails.mb_Details(mbExpression, batchDescription, action);
			submitBatch.submit_MB();
			mbComplete.mb_Progress();
			waitForElementToDisplay(mbComplete.batch_Complete);
			
			Adm_BEPage ohleClick= new Adm_BEPage(driver);
			waitForElementToDisplay(ohleClick.oh_LE);
			ohleClick.onHold_Click();
			
			OnHoldLEPage ohleVerify= new OnHoldLEPage(driver);
			waitForElementToDisplay(ohleVerify.onHoldTitle);
			SoftAssertions.verifyElementTextContains(ohleVerify.onHoldTitle,"Batch Events - On Hold Life Events",test);
			ohleVerify.onHoldVerify();
			waitForElementToDisplay(ohleVerify.ohleReason);
			SoftAssertions.verifyElementTextContains(ohleVerify.ohleReason,"Another pending Non-OE event already exists",test);
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("OnHold LE Verification failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("OnHold LE Verification failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
