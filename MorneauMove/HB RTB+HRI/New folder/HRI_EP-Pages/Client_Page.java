package pages.HRI_EP;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class Client_Page {
	
	ExtentTest test =null;
	
	
	@FindBy(id = "client-list-filter")
	public WebElement client_Tb;
	
	@FindBy(xpath = "//*[@id=\"js-client-list-table\"]/tbody/tr/td[1]/a")
	WebElement client;
	
	
	
	public Client_Page(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void clientClick(String Client) throws InterruptedException {
		
		waitForElementToDisplay(client_Tb);
		setInput(client_Tb, Client);
		clickElement(client);
		
		
	}
}