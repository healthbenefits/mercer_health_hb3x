package pages.HRI_EP;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class Adm_BEPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//i[@class=\"mul-image-icon mul-image-icon-size-medium mul-image-icon-position-right manual-import-icon\"]")
	 public WebElement manualBatchIcon;
	
	@FindBy(xpath = "//h2[text()='Batch Events - Dashboard - Delivery']")
	public WebElement be_title;
	
	@FindBy(xpath = "//span[text()='100']")
	 public WebElement mb_Complete;
	
	@FindBy(xpath = "//i[@title='Batch Complete']")
	 public WebElement batch_Complete;
	
	@FindBy(xpath = "//a[text()='On Hold Life Events']")
	 public WebElement oh_LE;
	
	@FindBy(xpath = "//a[text()='Batch Events Dashboard - Authoring']")
	 public WebElement bed_Auth;
	
	
	@FindBy(xpath = "//a[text()='Schedule Time Elapsed Batches']")
	public WebElement sb_TimeElapsed;
	
	@FindBy(xpath = "//h2[text()='Batch Events - Schedule Time Elapsed Batches']")
	public WebElement adm_STEB;
	
	@FindBy(xpath = "//h2[text()='Batch Events - On Hold Life Events']")
	public WebElement adm_OHLE;
	
	@FindBy(xpath = "//a[text()='Home']")
	 public WebElement be_Home;
	
	
	
	public Adm_BEPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void mb_Icon_Click( ) throws InterruptedException {
		
		clickElement(manualBatchIcon);
		
	}
	
	public void mb_Progress() throws InterruptedException {
		
		waitForElementToDisplay(mb_Complete);
		refreshpage();
	}
	
	public void onHold_Click( ) throws InterruptedException {
		
		clickElement(oh_LE);
		
	}
	
	public void home_Click( ) throws InterruptedException {
		
		clickElement(be_Home);
		
	}

	public void steb_Click( ) throws InterruptedException {
	
		clickElement(sb_TimeElapsed);
	

}

	public void teb_Progress() throws InterruptedException {
		
		waitForElementToDisplay(mb_Complete);
		refreshpage();
	}
	
	
	public void dash_Auth() throws InterruptedException {
		
		waitForElementToDisplay(bed_Auth);
		clickElement(bed_Auth);
		
	}
}