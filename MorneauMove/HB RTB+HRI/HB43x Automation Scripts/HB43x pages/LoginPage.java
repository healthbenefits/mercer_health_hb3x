package pages.HB43x;

import static driverfactory.Driver.setInput;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeNoException;
import static verify.SoftAssertions.verifyElementTextContains;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.Executor;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.gargoylesoftware.htmlunit.javascript.host.dom.Document;

import driverfactory.Driver;
import verify.SoftAssertions;

import static driverfactory.Driver.*;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;

public class LoginPage {

	@FindBy(id = "username")
	public static WebElement Username_TB;

	@FindBy(id = "password")
	WebElement Password_TB;


	@FindBy(xpath = "//button[contains(text(),'Login')]")
	WebElement Login;
	
	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	
	public void login(String USERNAME,String PASSWORD) throws InterruptedException {
		waitForElementToDisplay(Username_TB);
		setInput(Username_TB, USERNAME);
		Thread.sleep(2000);
		setInput(Password_TB, PASSWORD);;
		Thread.sleep(2000);
		clickElement(Login);
	}
}
