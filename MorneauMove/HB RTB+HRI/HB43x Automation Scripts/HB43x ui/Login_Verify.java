package smoke.testcases.HB43x;
import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HB43x.HealthPage;
import pages.HB43x.LandingPage;
import pages.HB43x.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Login_Verify    extends InitTests {
	
	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public Login_Verify  (String appName) {
		super(appName);
		
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		Login_Verify Tc01 = new Login_Verify("HB43x");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
	}
	
	@Test(enabled = false)
	public void login() throws Exception{
		try {
			test = reports.createTest("HB43x--verify Login To HB43x--" + BROWSER_TYPE);
			test.assignCategory("smoke");
			
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
		
			LoginPage loginPage = new LoginPage(driver); 
			loginPage.login(USERNAME, PASSWORD);
			
			LandingPage landingPage= new LandingPage(driver);
			landingPage.landing_Health();
			
			HealthPage healthVerify= new HealthPage(driver);
			waitForElementToDisplay(healthVerify.action_Tb);
			SoftAssertions.verifyElementTextContains(healthVerify.action_Tb," Take Action ",test);
			healthVerify.healthVerify(driver, urlVerify,test);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}



/*Boolean Verify=Driver.isElementExisting(loginPage.PurchasejobButton, 10);
SoftAssertions.assertTrue(Verify, "HomepagePage Verification");*/

