package smoke.testcases.HB43x;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HB43x.DepVerificationPage;
import pages.HB43x.HealthPage;
import pages.HB43x.LandingPage;
import pages.HB43x.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class DepVerify extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  DepVerify(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		DepVerify Tc01 = new DepVerify("HB43x");
		webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
		
	}
	@Test(priority = 1, enabled = true)
	public void verifyLogin() throws Exception {
		try {
			test = reports.createTest("HB43x--verify Login To HB43x--" + BROWSER_TYPE);
			test.assignCategory("smoke");
			
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			LoginPage loginPage = new LoginPage(driver); 
			loginPage.login(USERNAME, PASSWORD);
			
			LandingPage landingPage= new LandingPage(driver);
			landingPage.landing_Health();
			
			HealthPage healthVerify= new HealthPage(driver);
			waitForElementToDisplay(healthVerify.action_Tb);
			SoftAssertions.verifyElementTextContains(healthVerify.action_Tb," Take Action ",test);
			healthVerify.healthVerify(driver, urlVerify,test);
			
			
			HealthPage depVerify= new HealthPage(driver);
			depVerify.depVerify();
			
			DepVerificationPage documentUpload= new DepVerificationPage(driver);
			documentUpload.depDocumentUpload();
			
			DepVerificationPage dependentVerify= new DepVerificationPage(driver);
			waitForElementToDisplay(dependentVerify.ud_Confirmation);
			SoftAssertions.verifyElementTextContains(dependentVerify.ud_Confirmation, "You have successfully uploaded your document(s).",test);
			
			
			
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
