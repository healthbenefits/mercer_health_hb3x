package pages.HRI_EP;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;


public class OnHoldLEPage {
	
	ExtentTest test =null;
	
	
	@FindBy(xpath = "//h2[text()='Batch Events - On Hold Life Events']")
	public WebElement onHoldTitle;
	
	
	@FindBy(xpath = "//a[text()='Received Date']")
	public WebElement sortReceivedDate;
	
	
	@FindBy(xpath = "//*[@id=\"applicationHost\"]/div/section/div/div/div[2]/div[2]/div[2]/div[1]/div/div/div/table/tbody/tr[1]/td[10]")
	public WebElement ohleReason;
	
	
	public OnHoldLEPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void onHoldVerify( ) throws InterruptedException {
		
		for(int i=0;i<2;i++) {
			
			clickElement(sortReceivedDate);
			
		}
		
		
	}
}