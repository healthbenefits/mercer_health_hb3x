package pages.HRI_EP;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import driverfactory.Driver;
import verify.SoftAssertions;

public class DtMPeditPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//*[@id=\"js-source-target-mapping-table\"]/tbody/tr[7]/td[2]/input")
	 public WebElement editCB;
	
	@FindBy(xpath = "//a[@title='Save and Close']")
	 public WebElement save;
	
	public DtMPeditPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void dmedit(WebDriver driver) throws InterruptedException {
		
		clickElement(editCB);
		Thread.sleep(1000);
		clickElement(save);
		
	}
	
	}