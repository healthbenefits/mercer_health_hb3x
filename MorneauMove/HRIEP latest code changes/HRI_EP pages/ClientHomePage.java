package pages.HRI_EP;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;


import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class ClientHomePage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h1[@class='mul-hdr-rule']")
	 public WebElement client_HomePage_Verify;
	
	@FindBy(xpath = "//h2[text()='Administration']/..//table[2]//a[text()='Batch Events']")
	 public WebElement adm_BE;
	
	@FindBy(xpath = "//h2[text()='Administration']/..//table[1]//a[text()='Batch Events']")
	 public WebElement con_BE;
	
	@FindBy(xpath = "//h2[text()='Administration']/..//table[1]//a[text()='HR Import']")
	 public WebElement con_HRI;
	
	@FindBy(xpath = "//h2[text()='Administration']/..//table[2]//a[text()='HR Import']")
	 public WebElement adm_HRI;
	
	

	

	public ClientHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	public void adm_BE_Click( ) throws InterruptedException {
		
		clickElement(adm_BE);
		
	}
	
	public void con_BE_Click( ) throws InterruptedException {
		
		waitForElementToDisplay(con_BE);
		clickElement(con_BE);
		
	}
	public void con_HRI_Click( ) throws InterruptedException {
	
	waitForElementToDisplay(con_HRI);
	clickElement(con_HRI);
	}

	public void adm_HRI_Click( ) throws InterruptedException {
	
	waitForElementToDisplay(adm_HRI);
	clickElement(adm_HRI);
	}
}