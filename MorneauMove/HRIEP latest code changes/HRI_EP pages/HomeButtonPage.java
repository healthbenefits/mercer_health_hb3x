package pages.HRI_EP;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class HomeButtonPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//a[text()='Home']")
	public WebElement homeButton;
	
	public HomeButtonPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}
	
public void homePage() throws InterruptedException {
		
		clickElement(homeButton);
		
	}

}