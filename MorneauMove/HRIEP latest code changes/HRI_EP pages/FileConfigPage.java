package pages.HRI_EP;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import verify.SoftAssertions;

public class FileConfigPage {
	
	ExtentTest test =null;
	
	@FindBy(xpath = "//h2[@class='mul-hdr-rule groups-hdr-icons mul-pop-out-menu-action mul-hdr-icons']")
	public WebElement flCon_Title;
	
	@FindBy(xpath = "//span[@data-bind='text: StreamConfigId']")
	public WebElement strm_ConID;
	
	@FindBy(xpath = "//a[text()='Event Triggers']")
	public WebElement con_ET;
	
	@FindBy(xpath = "//a[@title='Promote']")
	public WebElement file_PromoteIcon;
		
	@FindBy(xpath = "//i[@data-bind='click: navigateToDataMappingPage, css: listItemsButtonsCssClass()']")
	public WebElement datamap_edit;
	
	@FindBy(xpath = "//a[@title='Publish']")
	public WebElement publishIcon;
	
	@FindBy(xpath = "//h6[text()='Confirmation']")
	 public WebElement init_confirmation;
	
	@FindBy(xpath = "//h6[text()='Confirmation']")
	 public WebElement comp_confirmation;
	
	public FileConfigPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
}

	public void et_Tab() throws InterruptedException {
		
		clickElement(con_ET);
		
	}
	
	public void promote_File() throws InterruptedException {
		
		clickElement(file_PromoteIcon);
		
	}
	
	public void publish_edit() throws InterruptedException {
		
		clickElement(datamap_edit);
		
	}
	
	public void publish_file() throws InterruptedException {
		
		clickElement(publishIcon);
		
	}
}