package smoke.HRI_EP.tests;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.HRI_EP.Adm_BEPage;
import pages.HRI_EP.Adm_HRIPage;
import pages.HRI_EP.ClientHomePage;
import pages.HRI_EP.Client_Page;
import pages.HRI_EP.Con_BEPage;
import pages.HRI_EP.Con_HRIPage;
import pages.HRI_EP.DtMPeditPage;
import pages.HRI_EP.FileConfigPage;
import pages.HRI_EP.HomeButtonPage;
import pages.HRI_EP.MB_SubmitPage;
import pages.HRI_EP.ManualBatchPage;
import pages.HRI_EP.OnHoldLEPage;
import pages.HRI_EP.Promote_FileConfig;
import pages.HRI_EP.Promote_TEBPage;
import utilities.InitTests;
import verify.SoftAssertions;



public class Publish_Verification extends InitTests {

	Driver driverFact=new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public  Publish_Verification(String appname) {
		super(appname);
	}
	
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		Publish_Verification Tc01 = new Publish_Verification("HRI");
		
	}
	@Parameters({"Execute","BASEURL","Client","File","Verify1", "Verify2"})
	@Test(priority = 1, enabled = true)
	public void HRI_EP(String Execute, String BASEURL, String Client,String File, String Verify1, String Verify2) throws Exception {
		try {
			
			test = reports.createTest("HRI/EP--verify Publish--" + BROWSER_TYPE);
			test.assignCategory("regression");
			if (Execute.contentEquals("Y")) {
			webdriver=driverFact.initWebDriver(BASEURL,BROWSER_TYPE,EXECUTION_ENV,"");
			driver=driverFact.getEventDriver(webdriver,test);
			System.out.println("BaseURL is: " + BASEURL);
			
		
			Client_Page clientSelect = new Client_Page(driver); 
			clientSelect.clientClick(Client);
			
			ClientHomePage clientHome= new ClientHomePage(driver);
			waitForElementToDisplay(clientHome.client_HomePage_Verify);
			SoftAssertions.verifyElementTextContains(clientHome.client_HomePage_Verify,Client,test);
			clientHome.con_HRI_Click();
			
			Con_HRIPage filecon = new Con_HRIPage(driver);
			waitForElementToDisplay(filecon.hri_Title);
			filecon.searchFLConfig(File);
			
			FileConfigPage dmEdit= new FileConfigPage(driver);
			waitForElementToDisplay(dmEdit.flCon_Title);
			dmEdit.publish_edit();
			
			DtMPeditPage changeCB = new DtMPeditPage(driver);
			waitForElementToDisplay(changeCB.editCB);
			changeCB.dmedit(driver);
						
			FileConfigPage publish = new FileConfigPage(driver);
			waitForElementToDisplay(publish.publishIcon);
			publish.publish_file();
			waitForElementToDisplay(publish.init_confirmation);
			verifyElementTextContains(publish.init_confirmation, Verify1, test);
			Thread.sleep(70000);
			refreshpage();
			waitForElementToDisplay(publish.comp_confirmation);
			verifyElementTextContains(publish.init_confirmation, Verify2, test);
						
			dmEdit.publish_edit();
			
			
			waitForElementToDisplay(changeCB.editCB);
			changeCB.dmedit(driver);
			
			
			waitForElementToDisplay(publish.publishIcon);
			publish.publish_file();
			
			
			}
		} catch (Exception e) {
			e.printStackTrace();

			fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		finally {
			reports.flush();
			driver.quit();
		}
	}
}
