----schema: QA2_1a_GeoFramework -----------------

-----Login managementing CSP ----
-----Popup enabled or not: (no longer used) ----
select * from dbo.csp_Config_Setting nolock
where property_id in (select property_id from  csp_property nolock
where propertyname like '%IsLoginMgmtPopupEnabled%')

select * from dbo.csp_Config_Setting where Property_Id=2294; 

-----encryption enabled or not: ----
select * from dbo.csp_Config_Setting nolock
where property_id in (select property_id from  csp_property nolock
where propertyname like '%IsLoginMgmtEncryptEnabled%')

select * from dbo.csp_Config_Setting where Property_Id=2296;

Update csp_Config_Setting
set PropertyValue = 'true' 
where Property_Id=2294 and ControlId='PBSPHL' ; 

--Update csp_Config_Setting
set PropertyValue = 'false'
where Property_Id=2296 and ControlId='QAWORK'; 
 
 --Insert into csp_Config_Setting (property_id,SetPath_Id,ControlId,ServerName,Environment,PropertyValue)
values ('2294', '179','PBSPHL','_default', '_default', 'true') 


------MAS ENABLED------------------------
select * from dbo.csp_Config_Setting nolock
where property_id in (select property_id from  csp_property nolock
where propertyname like '%IsTransitionedToMAS%')



select * from csp_Config_Setting where Property_Id=2239 and ControlId='QAWORK'

Update csp_Config_Setting
set PropertyValue = 'false'
where Property_Id=2239 and ControlId='QAWORK'


--Insert into csp_Config_Setting (property_id,SetPath_Id,ControlId,ServerName,Environment,PropertyValue)
values ('2239', '179','TEST1','_default', '_default', 'true')

-----SETUP OF MAS URL---------------------------------------------

select * from csp_config_setting nolock where property_id in (select Property_Id from csp_property nolock
where propertyname like '%RedirectRootUrl%')
and settingid=29795


--Update csp_config_setting
set PropertyValue ='https://auth-qa.mercer.com/IBCG2'
where property_id = 2240 and settingid = '29824'

--Insert into csp_Config_Setting (property_id,SetPath_Id,ControlId,ServerName,Environment,PropertyValue)
values ('2240', '179','TEST1','_default', '_default', 'https://auth-qa.mercer.com/IBCG2')

-----FDNH-----

select * from csp_property nolock
where propertyname = 'AccountCreationToken'
 
select * from csp_config_setting  nolock
where property_id = 1123 