--======= SCHEMA: CLTQA2_1_MAS_UAS =======--

 --------------TESTDATA: RESETTING A USER FOR MAS REGISTRATION------------
 
  Delete from PasswordHistory
  --select * from PasswordHistory
  where memberId in (Select memberid from Member where userId in ('kishan.kashyap@mercer.com','nishant.kolte@mercer.com'))
  
   Delete from MemberAttributes
  --select * from MemberAttributes
	where memberId in (Select memberid from Member where userId in ('kishan.kashyap@mercer.com','nishant.kolte@mercer.com'))
  
  Delete from Member
  --select * from Member
  where memberId in (Select memberid from Member where userId in ('kishan.kashyap@mercer.com','nishant.kolte@mercer.com'));

Select * from Member where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
select * from MemberAttributes where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
select * from PasswordHistory where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
  
  --200000221
  --LNAME0221
  --1/1/1950
  --02062
  
  --username: nishant.kolte@Mercer.com
  --password: Test@001
  
----------------------TESTDATA: RESETTING A MIGRATED USER--------------------------------------

update member
set hashAlgorithm = 2, passwordHash=CONVERT(VARBINARY(255),'0x0200244308291945B7B50430549464E1824581A80AE69841FA9EF4BDB73A6CBBFD35D8AF5E2A50731557EFE7E0E7368691CE6FBB1CD1ADE524AF5E57DBE60DC9679FCC7235F9', 1), userId='auto_slsdm0215',updateUserId=1,emails='["nishant.kolte@mercer.com"]',phones='[]',updatePassword=0,updateContactInfo=0, completeLogin=0
where memberid = 475 ;

update PasswordHistory
set hashAlgorithm = 2, passwordhash=CONVERT(VARBINARY(255),'0x0200244308291945B7B50430549464E1824581A80AE69841FA9EF4BDB73A6CBBFD35D8AF5E2A50731557EFE7E0E7368691CE6FBB1CD1ADE524AF5E57DBE60DC9679FCC7235F9', 1)
where memberid = 475 and passwordHistoryId=530;
 
delete from PasswordHistory
where memberId=475 and passwordHistoryId!=530;

Select * from Member where memberId=475
select * from MemberAttributes where memberId =475
select * from PasswordHistory where memberId =475 

--URL: https://auth-cit.mercer.com/SLSDM/login
--Initial Username: auto_slsdm0215
--Initial Password: test0001
--200000215
--LNAME0215
--1/1/1950
--02062

--update Username to: auto_slsdm0215@mas.com
--update contact methods to: nishant.kolte@mercer.com|+918788748033
--update Password to: Test@001

----------------------------TESTDATA: RESETTING A RETURNING USER--------------------------------------------------------

DECLARE @PW VARBINARY(255);
SET @PW = (select Passwordhash from PasswordHistory where passwordHistoryId=3206);

insert into PasswordHistory (memberId,passwordHash,date,modifiedBy,hashAlgorithm)
values (315,@PW,GETDATE(),315,1)

Update Member
set userId='auto_slsdm1201@mas.com', emails='["nishant.kolte@mercer.com"]', phones='["918788748033"]', passwordHash=@PW, mfa='disabled'
where memberId=315

Select * from Member where memberId=315
select * from MemberAttributes where memberId =315
select * from PasswordHistory where memberId =315 

--URL: https://auth-cit.mercer.com/SLSDM/login
--auto_slsdm1201@mas.com
--SLSDMpass1

--1201
--LNAME1201
--1/4/1947
--02062


--TESTDATA: MBCTRA RETURNING USER
--URL: https://auth-cit.mercer.com/MBCTRA/login
--nishant.kolte1@mercer.com
--test@003

