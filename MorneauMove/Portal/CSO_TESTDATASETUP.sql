------------------------- schema: CSO_MAS_UAS --------

 --------------TESTDATA: RESETTING A USER FOR MAS REGISTRATION------------
 
  Delete from PasswordHistory
  --select * from PasswordHistory
  where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
  
   Delete from MemberAttributes
  --select * from MemberAttributes
	where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
  
  Delete from Member
  --select * from Member
  where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')

Select * from Member where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
select * from MemberAttributes where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
select * from PasswordHistory where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
  
  --url: https://auth-cso.mercer.com/SLSDM/login
  --200001309
  --LNAME1309
  --01/04/1947
  --77450
  
  --username: nishant.kolte@mercer.com
  --password: Test@001
  
  -----------------for MBC user:
  --url: https://auth-cso.mercer.com/MBCTRA/login
  --909004430
  --LNAME4430
  --08/19/1956
  --151082409
  
  --username: nishant.kolte@mbc.com
  --password: Test@001
  
----------------------TESTDATA: RESETTING A MIGRATED USER--------------------------------------

update member
set hashAlgorithm = 2, passwordHash=CONVERT(VARBINARY(255),'0x0200244308291945B7B50430549464E1824581A80AE69841FA9EF4BDB73A6CBBFD35D8AF5E2A50731557EFE7E0E7368691CE6FBB1CD1ADE524AF5E57DBE60DC9679FCC7235F9', 1), userId='testqa0204',updateUserId=1,emails='["nishant.kolte@mercer.com"]',phones='["918788748033"]',updatePassword=0,updateContactInfo=0, completeLogin=0
where memberid = 46511 ;

update PasswordHistory
set hashAlgorithm = 2, passwordhash=CONVERT(VARBINARY(255),'0x0200244308291945B7B50430549464E1824581A80AE69841FA9EF4BDB73A6CBBFD35D8AF5E2A50731557EFE7E0E7368691CE6FBB1CD1ADE524AF5E57DBE60DC9679FCC7235F9', 1)
where memberid = 46511 and passwordHistoryId=46600;
 
delete from PasswordHistory
where memberId=46511 and passwordHistoryId!=46600;

Select * from Member where memberId=46511
select * from MemberAttributes where memberId =46511
select * from PasswordHistory where memberId =46511   
 
--url: https://auth-cso.mercer.com/SLSDM/login
-- username: testqa0204
-- password: test0001


--0204
--LNAME0204
--01/01/1950
--02062 
----------------------------TESTDATA: RESETTING A RETURNING USER--------------------------------------------------------

DECLARE @PW VARBINARY(255);
SET @PW = (select Passwordhash from PasswordHistory where passwordHistoryId=815);

insert into PasswordHistory (memberId,passwordHash,date,modifiedBy,hashAlgorithm)
values (51,@PW,GETDATE(),51,1)

Update Member
set userId='auto_slsdm1001@mas.com', emails='["nishant.kolte@mercer.com"]', phones='["918788748033"]', passwordHash=@PW, mfa='disabled'
where memberId=51

Select * from Member where memberId=51
select * from MemberAttributes where memberId =51
select * from PasswordHistory where memberId =51  

--url: https://auth-cso.mercer.com/SLSDM/login
--username: auto_slsdm1001@mas.com
--password: SLSDMpass1

---------------------------------------------------------------------------