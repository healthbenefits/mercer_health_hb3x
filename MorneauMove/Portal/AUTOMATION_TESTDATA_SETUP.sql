--======= SCHEMA: QA2_1a_MAS_UAS =======--

  ---------AUTOMATION TESTDATA SETUP----------
  
--@RegistrationFlow-1:
  
Delete from PasswordHistory
where memberId in 
(select memberId from Member where userId='registrationtest1@qawork.com');

Delete from MemberAttributes
where memberId in 
(select memberId from Member where userId='registrationtest1@qawork.com');

Delete from Member
where memberId in 
(select memberId from Member where userId='registrationtest1@qawork.com');


--@RegistrationFlow-2:
  
Delete from PasswordHistory
where memberId in 
(select memberId from Member where userId='nishant.kolte@mercer.com');
Delete from MemberAttributes
where memberId in 
(select memberId from Member where userId='nishant.kolte@mercer.com');
Delete from Member
where memberId in 
(select memberId from Member where userId='nishant.kolte@mercer.com');

--@ForgotPasswordFlow-1:
Update Member
Set passwordHash=CONVERT(VARBINARY(255),'0x24326124313024594A59796B63394C49675879687A517549564767702E5372307A4D554979726F5A70734773694576384B345338617A6E594834476D', 1)
where userId='AUTO_ForgotFlowtest@Qawork.com';
Delete from PasswordHistory
where memberId=183;
insert into PasswordHistory
(memberid,passwordHash,date,modifiedBy,hashAlgorithm)
values ('183',CONVERT(VARBINARY(255),'0x24326124313024594A59796B63394C49675879687A517549564767702E5372307A4D554979726F5A70734773694576384B345338617A6E594834476D', 1),CURRENT_TIMESTAMP,'183','1')

--@ForgotPasswordFlow-2:
Update Member
Set passwordHash=CONVERT(VARBINARY(255),'0x24326124313024594A59796B63394C49675879687A517549564767702E5372307A4D554979726F5A70734773694576384B345338617A6E594834476D', 1)
where userId='AUTO_Migrateduserforgotflowtest';

Delete from PasswordHistory
where memberId=165;	
insert into PasswordHistory
(memberid,passwordHash,date,modifiedBy,hashAlgorithm)
values ('165',CONVERT(VARBINARY(255),'0x24326124313024594A59796B63394C49675879687A517549564767702E5372307A4D554979726F5A70734773694576384B345338617A6E594834476D', 1),CURRENT_TIMESTAMP,'165','1')

--@Login_Management_Regression:
Update Member
Set passwordHash=CONVERT(VARBINARY(255),'0x24326124313024594A59796B63394C49675879687A517549564767702E5372307A4D554979726F5A70734773694576384B345338617A6E594834476D', 1),lockeduntildate=NULL, invalidLoginAttempts=0
where userId='AUTO_LoginManagement@Qawork.Test';

Delete from PasswordHistory
where memberId=256;

insert into PasswordHistory
(memberid,passwordHash,date,modifiedBy,hashAlgorithm)
values ('256',CONVERT(VARBINARY(255),'0x24326124313024594A59796B63394C49675879687A517549564767702E5372307A4D554979726F5A70734773694576384B345338617A6E594834476D', 1),CURRENT_TIMESTAMP,'256','1')

--@Accountlockout
Update member
set lockeduntildate=NULL, invalidLoginAttempts=0
where userid='AUTO_Accountlocked@mas.com'

--@Registered_user_PW_update_required
Update Member
Set updatePassword=1, passwordHash=CONVERT(VARBINARY(255),'0x24326124313024594A59796B63394C49675879687A517549564767702E5372307A4D554979726F5A70734773694576384B345338617A6E594834476D', 1)
where userId='AUTO_PasswordUpdatetest@qawork.com';

Delete from PasswordHistory
where memberId=257;

insert into PasswordHistory
(memberid,passwordHash,date,modifiedBy,hashAlgorithm)
values ('257',CONVERT(VARBINARY(255),'0x24326124313024594A59796B63394C49675879687A517549564767702E5372307A4D554979726F5A70734773694576384B345338617A6E594834476D', 1),CURRENT_TIMESTAMP,'257','1')

--@Registered_user_ContactMethod_update_required
Update Member
Set updateContactInfo=1, phones='[]'
where userId='AUTO_ContactinfoUpdatetest@qawork.com';

--@Migrated_user_Login_UN_Contact_PW_update
--Update Member
--Set passwordHash=CONVERT(VARBINARY(255),'0x01002FED9D43E4F62EAF1BA28D8224B4A27D9627C0CCE62A9832', 1),hashAlgorithm=3,completeLogin=0,updateUserId=1,updatePassword=0,emails='["nishant.kolte@mercer.com"]',userId='AUTO_migrateduserlogintest1',phones='[]'
--where memberId=235

--Delete from PasswordHistory
--where memberId=235 and hashAlgorithm=1;
update member
set hashAlgorithm = 2, passwordHash=CONVERT(VARBINARY(255),'0x0200244308291945B7B50430549464E1824581A80AE69841FA9EF4BDB73A6CBBFD35D8AF5E2A50731557EFE7E0E7368691CE6FBB1CD1ADE524AF5E57DBE60DC9679FCC7235F9', 1), userId='AUTO_migrateduserlogintest1',updateUserId=1,emails='["nishant.kolte@mercer.com"]',phones='[]',updatePassword=0,updateContactInfo=0, completeLogin=0
where memberid = 235 ;

update PasswordHistory
set hashAlgorithm = 2, passwordhash=CONVERT(VARBINARY(255),'0x0200244308291945B7B50430549464E1824581A80AE69841FA9EF4BDB73A6CBBFD35D8AF5E2A50731557EFE7E0E7368691CE6FBB1CD1ADE524AF5E57DBE60DC9679FCC7235F9', 1)
where memberid = 235 and passwordHistoryId=316;
 
delete from PasswordHistory
where memberId=235 and passwordHistoryId!=316;


--@Migrated_user_Login_PW_update
Update Member
Set passwordHash=CONVERT(VARBINARY(255),'0x0200244308291945B7B50430549464E1824581A80AE69841FA9EF4BDB73A6CBBFD35D8AF5E2A50731557EFE7E0E7368691CE6FBB1CD1ADE524AF5E57DBE60DC9679FCC7235F9', 1),hashAlgorithm=2,completeLogin=0,updateUserId=1,updatePassword=0,emails='[]',userId='AUTO_migrateduserlogintest2',phones='[]'
where memberId=1300;

Delete from PasswordHistory
where memberId=1300 and hashAlgorithm=1;


Delete from PasswordHistory
where memberId=1300 and hashAlgorithm=1;

--@Migrated_user_Login_UN_Contact_update
Update Member
Set updateUserId=1,userId='AUTO_migrateduserlogintest3',completeLogin=0
where memberId=1283



--@altlogin-accountlockedmessage
Update member
set lockeduntildate=NULL, invalidLoginAttempts=0
where memberid=817

--@accountlockedmessage for login flow
update member
set userId='AUTO_accountlocked3@mas.com',lockeduntildate=NULL, invalidLoginAttempts=0
where memberid=76 

--@auto-migration scenarios

Delete from PasswordHistory
  --select * from PasswordHistory
  where memberId in 
  (select memberId from Member where userId in ('test@tmail.com','aa147@gmail.com','invalid10@invalid.com'));
  
   Delete from MemberAttributes
  --select * from MemberAttributes
   where memberId in 
  (select memberId from Member where userId in ('invalid10@invalid.com','aa147@gmail.com','test@tmail.com'));
  
  Delete from Member
  --select * from Member
   where memberId in 
  (select memberId from Member where userId in ('invalid10@invalid.com','aa147@gmail.com','test@tmail.com'));

--@invalid salt exception case
DECLARE @PW VARBINARY(255);
SET @PW = (select Passwordhash from PasswordHistory where passwordHistoryId=201);

Update Member
set userId='test0009@mas.com',passwordHash=@PW , hashAlgorithm=2
where memberId=120

delete from PasswordHistory
where memberId=120 and passwordHistoryId!=201;

