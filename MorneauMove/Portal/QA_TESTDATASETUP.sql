--======= SCHEMA: QA2_1a_MAS_UAS =======--

 --------------TESTDATA: RESETTING A USER FOR MAS REGISTRATION------------
 
  Delete from PasswordHistory
  --select * from PasswordHistory
  where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
  
   Delete from MemberAttributes
  --select * from MemberAttributes
	where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
  
  Delete from Member
  --select * from Member
  where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')

Select * from Member where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
select * from MemberAttributes where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
select * from PasswordHistory where memberId in (Select memberid from Member where userId='nishant.kolte@mercer.com')
  
  --1491
  --Allen
  --02/23/1963
  --78006
  
  --username: nishant.kolte@Mercer.com
  --password: Test@001
  
----------------------TESTDATA: RESETTING A MIGRATED USER--------------------------------------

update member
set hashAlgorithm = 2, passwordHash=CONVERT(VARBINARY(255),'0x0200244308291945B7B50430549464E1824581A80AE69841FA9EF4BDB73A6CBBFD35D8AF5E2A50731557EFE7E0E7368691CE6FBB1CD1ADE524AF5E57DBE60DC9679FCC7235F9', 1), userId='auto_test0271',updateUserId=1,emails='["nishant.kolte@mercer.com","narayan.namballa@mercer.com"]',phones='["918788748033"]',updatePassword=0,updateContactInfo=0, completeLogin=0
where memberid = 391 ;

update PasswordHistory
set hashAlgorithm = 2, passwordhash=CONVERT(VARBINARY(255),'0x0200244308291945B7B50430549464E1824581A80AE69841FA9EF4BDB73A6CBBFD35D8AF5E2A50731557EFE7E0E7368691CE6FBB1CD1ADE524AF5E57DBE60DC9679FCC7235F9', 1)
where memberid = 391 and passwordHistoryId=472;
 
delete from PasswordHistory
where memberId=391 and passwordHistoryId!=472;

Select * from Member where memberId=391
select * from MemberAttributes where memberId =391
select * from PasswordHistory where memberId =391   

-- url: auth-qa.mercer.com/qawork/login
-- username: auto_test0271
-- password: test0001

--Migrated user forgot username flow:
--0204
--LNAME0204
--01/01/1950
--02062 
----------------------------TESTDATA: RESETTING A RETURNING USER--------------------------------------------------------

DECLARE @PW VARBINARY(255);
SET @PW = (select Passwordhash from PasswordHistory where passwordHistoryId=162);

insert into PasswordHistory (memberId,passwordHash,date,modifiedBy,hashAlgorithm)
values (87,@PW,GETDATE(),87,1)

Update Member
set userId='AUTO_QAWORK4988@mas.com', emails='["nishant.kolte@mercer.com","benila.selvaraj@mercer.com","narayan.namballa@mercer.com","radhika.tambe@mercer.com","kishan.kashyap@mercer.com"]', phones='["918788748033"]', passwordHash=@PW , lockedUntilDate=NULL ,invalidLoginAttempts=0
where memberId=87

Select * from Member where memberId=87
select * from MemberAttributes where memberId =87
select * from PasswordHistory where memberId =87  

-- url: auth-qa.mercer.com/qawork/login
--username: AUTO_QAWORK4988@mas.com
--password: test@001

--forgot flows:
--6485
--Cousins
--08/04/1952
--80233

--userid:nishantkolte@gmail.com1
-----------------------------TESTDATA: RESETTING A AUTO_MIGRATION USER----------------------------------------------


Delete from PasswordHistory
  --select * from PasswordHistory
  where memberId in 
  (select memberId from Member where userId in ('test@tmail.com','aa147@gmail.com','invalid10@invalid.com'));
  
   Delete from MemberAttributes
  --select * from MemberAttributes
   where memberId in 
  (select memberId from Member where userId in ('invalid10@invalid.com','aa147@gmail.com','test@tmail.com'));
  
  Delete from Member
  --select * from Member
   where memberId in 
  (select memberId from Member where userId in ('invalid10@invalid.com','aa147@gmail.com','test@tmail.com'));

Select * from Member where userId = 'aa147@gmail.com'
select * from MemberAttributes where memberId = (Select memberId from Member where userId = 'aa147@gmail.com')
select * from PasswordHistory where memberId =(Select memberId from Member where userId = 'aa147@gmail.com')

Select * from Member where userId = 'test@tmail.com'
select * from MemberAttributes where memberId = (Select memberId from Member where userId = 'test@tmail.com')
select * from PasswordHistory where memberId =(Select memberId from Member where userId = 'test@tmail.com')
  
  --https://auth-qa.mercer.com/MMX365Plus/Member/login
  -- invalid10@invalid.com
  -- Test@001
  
  --test@tmail.com
  --Test@001
  
  ---IBCG2-MFA-Disabled--test1234@gmail.com/test@001 
  
  
  -----
  
DECLARE @PW VARBINARY(255);
SET @PW = (select Passwordhash from PasswordHistory where passwordHistoryId=162);

insert into PasswordHistory (memberId,passwordHash,date,modifiedBy,hashAlgorithm)
values (57,@PW,GETDATE(),87,1)

Update Member
set userId='AUTO_QAWORK0004@mas.com', emails='["nishant.kolte@mercer.com","benila.selvaraj@mercer.com","narayan.namballa@mercer.com","radhika.tambe@mercer.com","kishan.kashyap@mercer.com"]', phones='["918788748033"]', passwordHash=@PW , lockedUntilDate=NULL ,invalidLoginAttempts=0
where memberId=57

Select * from Member where memberId=57
Select * from Member where userid='AUTO_QAWORK0004@mas.com'
select * from MemberAttributes where memberId =57
select * from PasswordHistory where memberId =57  

-- url: auth-qa.mercer.com/qawork/login
--username: AUTO_QAWORK0004@mas.com
--password: test@001