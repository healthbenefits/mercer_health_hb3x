--======= SCHEMA: QA2_1a_MAS_UAS =======--

Select * from Member where userId like 'MBCKG11415@mercer.com%' --aa147@gmail.com  test@tmail.com
Select * from Member where memberId=3416;
select * from PasswordHistory where memberId=3092;
select * from MemberAttributes where memberId=2374;

select min(memberid) from member where memberid in
(select top 3 memberid from Member
order by memberId desc)
SELECT Count(DISTINCT(memberid)) total FROM member

SELECT top 10 applicationId, count(applicationUniqueUserId) FROM MemberAttributes 
GROUP BY applicationId
having applicationId = 'QAWORK|IBCG2'; 

--------RESETTING A MIGRATED USER---------------
delete from PasswordHistory
where memberId=391 and passwordHistoryId!=472;
update member
set hashAlgorithm = 2, passwordhash = (select passwordhash from passwordhistory ph where ph.passwordhistoryid = 472), userId='test0271',updateUserId=1,emails='[]',phones='[]'
where memberid = 391 ;

---------View Data------------------------------------
             
  ---search by SSN----           
  SELECT *
  FROM member a JOIN MemberAttributes b 
  ON a.memberId = b.memberId
  WHERE 
  --b.applicationId like '%BC_PTP_WEB' and
  b.applicationUniqueUserId like '908110040%'
  
    ---search by memberid----           
  SELECT *
  FROM member a JOIN MemberAttributes b 
  ON a.memberId = b.memberId
  WHERE 
  b.applicationId like 'QAWORK%' and
  a.memberId='163'
    
  --migrated user completed login process---
  SELECT *
  FROM member a JOIN MemberAttributes b 
  ON a.memberId = b.memberId
  WHERE 
  b.applicationId like 'QAWORK%' and
  a.memberId=186
  --a.completeLogin=0
  b.applicationUniqueUserId='523759452'
  --b.claims='{"uniqueclientid":["RETIREE"]}'
  --mfa='disabled'
 a.userId not like '%@%'
  select * from PasswordHistory where memberId=145;
  
  ---migrated user before login process--  
  SELECT *
  FROM member a JOIN MemberAttributes b 
  ON a.memberId = b.memberId
  WHERE 
  b.applicationId like 'MBC%' and
  a.completeLogin = '0' 

---update member---
Update member
--set emails='["nishant.kolte@mercer.com"]',phones='["918788748033"]'
--set userId='AUTO_migrateduserlogintest1' --, updateUserId=0
--set lockeduntildate=NULL, invalidLoginAttempts=0
set mfa='disabled' -- status='locked', lockedReason='lockedByMercer'--, invalidLoginAttempts=0, lockedUntilDate=NULL
--set emails='["ankita.goyal@mercer.com"]'--,phones='[]', completeLogin=0,updateuserid=1,updatePassword=1--userId='AUTO_Migrateduserforgotflowtest3', ,phones='[]'--mfa='default', status='active',userId='AUTO_QAtest_HBuser@MBCQA5.com', emails='["#mercermbcteamdevqa@mercer.com","nishant.kolte@mercer.com"]', phones='["918788748033","918884533631"]'
--set updateContactInfo=1--completeLogin=1,updateContactInfo=1,updatePassword=0--phones='["918788748033"]'--emails='[]',-- updatePassword=1--updateContactInfo=1--updateUserId=0,, completeLogin=1 --,,userid='test'
where memberid=2374
--where userid='test9948'


  
  --------------DELETE MAS records------------
 
  Delete from PasswordHistory
  --select * from PasswordHistory
  where memberId=3182;
  
   Delete from MemberAttributes
  --select * from MemberAttributes
  where memberId=3182;
  
  Delete from Member
  --select * from Member
  where memberId=3182;
   
------------------------Delete based on userid-------------------------
Delete from PasswordHistory
  --select * from PasswordHistory
  where memberId in 
  (select memberId from Member where userId in ('Last18679Name45@gmaililo.com'));
  
   Delete from MemberAttributes
  --select * from MemberAttributes
   where memberId in 
  (select memberId from Member where userId in ('Last18679Name45@gmaililo.com'));
  
  Delete from Member
  --select * from Member
   where memberId in 
  (select memberId from Member where userId in ('Last18679Name45@gmaililo.com'));
  
-----------------------------------------------------------------------------------

